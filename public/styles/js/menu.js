$(document).ready(function(){
    // Generate link menu when input name menu
    $('#name').keyup(function(){
        var $link = $('#name').val();
        $link = $link.replace(/ /g,'_');
        var $link_lower = $link.toLowerCase();
        $('#link').val($link_lower);
    });
});