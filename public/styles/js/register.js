$(document).ready(function(){

	$('#form-register').validate({
		showErrors: function(errorMap, errorList) { 
          	// Clean up any tooltips for valid elements
          	$.each(this.validElements(), function (index, element) {
              	var $element = $(element); 
              	$element.data("title", "") // Clear the title - there is no error associated anymore
                  	.removeClass("error")
                  	.tooltip("destroy");
          	});
 
          	// Create new tooltips for invalid elements
          	$.each(errorList, function (index, error) {
              	var $element = $(error.element);
 
              	$element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
					.data("title", error.message)
					.addClass("error")
					.tooltip({
						placement: "left"
					}); // Create a new tooltip based on the error messsage we just set in the title
          	});
      	},
		rules:{
			firstname:{
				required: true,				
			},
			lastname:{
				required: true,
			},
			username:{
				required: true,
				minlength: 4,
			},
			password:{
				required: true,
				minlength: 5,
			},
			password_confirmation: {
				equalTo: '#password',
			},
			email: {
				required: true,
				email: true,
			},
			recaptcha_response_field: {
				required: true,
			}
		},
		messages: {
			firstname:{
				required: 'Chưa nhập firstname',
			},
			lastname:{
				required: 'Chưa nhập lastname',
			},
			username: {
				required: 'Tên đăng nhập không được để trống',
				minlength: 'Tên đăng nhập phải có ít nhất 4 ký tự',
			},
			password: {
				required: 'Mật khẩu không được để trống',
				minlength: 'Mật khẩu phải có ít nhất 5 ký tự',
			},
			password_confirmation: {
				equalTo: 'Mật khẩu không khớp',
			},
			email: {
				required: 'Email không được để trống',
				email: 'Email không hợp lệ'
			}, 
			recaptcha_response_field: {
				required: 'Captcha không được để trống',
			}
		}
	});
});