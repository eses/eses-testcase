/**
 * Created by Nadare on 1/29/15.
 */
var esesDirective =  angular.module('esesDirective',[]);

esesDirective.factory('UniqueValidatorService',function($http){
        return {
            verify : function(value,name){
                var info = {'value': value,'name' : name};
                return $http({
                    method: 'POST',
                    url: pathUrl + 'backend/api/users/verify',
                    headers: { "Content-Type" : 'application/x-www-form-urlencoded' },
                    data: $.param(info)

                });
            }
        }
});

esesDirective.directive('esesUnique', function(UniqueValidatorService) {
    return {
        restrict: 'A', // only activate on element attribute
        require: 'ngModel',

        link: function(scope, element, attrs, ngModel) {
            scope.isUnique = true;
            element.bind('blur', function (e) {
                if(!ngModel || !element.val() ) return; // do nothing if no ng-model
                var dataInput = scope.$eval(attrs.esesUnique);
                UniqueValidatorService.verify(dataInput.value, dataInput.name)
                    .success(function(data){
                        if(data){
                            console.log('du lieu chua ton tai');
                            ngModel.$setValidity('unique', true);
                            //   ngModel.$invalid = true;
                        }else{
                            ngModel.$setValidity('unique', false);
                            // ngModel.$invalid = false;
                        }

                    })
                    .error(function(data){
                        ngModel.$setValidity('unique', false);
                    });

            });
        }
    }
});

esesDirective.directive('esesBinding', function() {
    return {
        restrict: 'A', // only activate on element attribute
        require: 'ngModel',

        link: function(scope, element, attrs, ngModel) {
            var te = element.text();
            scope.userProfile.name = element.text();

            scope.$watch(function(){
                console.log($('#username').text());
               return  $('#username').text();
            },function(newval, oldval){
                if(newval !== oldval) {
                console.log(newval);
                alert('before change');
                if (scope.userProfile.name !== $('#username').text()) {
                    alert('change');
                    scope.userProfile.name = $('#username').text();

                }
                }

            });


        }
    }
});