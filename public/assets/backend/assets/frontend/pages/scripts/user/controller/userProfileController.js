angular.module("UserProfile").controller('UserProfileController', function ($scope, $routeParams, $compile,
                                                                 UserProfileService,$http) {



    $scope.user = {};

    UserProfileService.get($scope.user)
        .success(function(result){
            var data = result['data'];
            console.log('userprofile');
            console.log(data);

            $.each(data, function(i, val){
                if(val['value_column']){
                    var id = '#' + val['name_column'];
                    if( !(typeof($(id).val()) === 'undefined')){
                        $scope.user[val['name_column']] = val['value_column'];
                        $(id).val(val['value_column']);
                    }
                }
            });
        })
        .error(function(data){
            toastr['error']('Cannot get profile, username=' + $scope.username);
        });
    $scope.isLoad=true;

$scope.saveProfile = function(){
    console.log($scope.user);
    UserProfileService.update($scope.user)
        .success(function(result){
            if( result['error'] !== 1){
                toastr['success']('Update user profile successfully!');
                alert('Update user profile successfully!');
            }else{
                toastr['error']('Update user profile failed!');
                alert('Update user profile failed!');
            }
        })
        .error(function(data){
            toastr['error']('System error when updating profile!');
        });
}

function disableForm(){
        $("#first_name").attr("disabled",true);
        $("#last_name").attr("disabled",true);
        $("#sex").attr("disabled",true);
        $("#email").attr("disabled",true);
        $("#phone").attr("disabled",true);
        $("#address").attr("disabled",true);
        $("#occupation").attr("disabled",true);
        $("#btnSave").hide();

    }





    //for new function

});
   