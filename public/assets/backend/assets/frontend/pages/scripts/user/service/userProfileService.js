/**
 * Created by Nadare on 12/20/14.
 */
var userProfileService = angular.module('UserProfile',['ngRoute','ngAnimate']);

userProfileService.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'UserProfileController',
            templateUrl: pathPublic + 'assets/backend/assets/frontend/pages/scripts/user/partial/userProfile.html'
        })
       /* .when('/create',{
            controller: 'UserController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/users/partials/addUser.html'
        }).when('/update/:id',{
            controller: 'UserController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/users/partials/editUser.html'
        })*/;
    /* .otherwise({
     redirectTo: '/'
     });*/


}]);


userProfileService.factory('UserProfileService', function($http){
       return {
           getUserProfile : function(){
               return $http.get( pathUrl + 'mypage/api/profile');
           },
           updateUserProfile: function(userProfile){
               return $http({
                   method: 'POST',
                   url: pathUrl + 'mypage/api/profile/update',
                   headers:{ "Content-Type" : 'application/json' },
                   data: userProfile
               });
           },
               get : function(username){
                   return $http.get( pathUrl + '/mypage/api/profile');
               },

               update : function(user){
                   return $http({
                       method: 'POST',
                       url: pathUrl + '/mypage/api/profile/update',
                       headers:{ "Content-Type" : 'application/json' },
                       data: user
                   });
               }




           }
    });


