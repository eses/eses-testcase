/**
 * Created by Nadare on 12/20/14.
 */
var registerService = angular.module('RegisterService',['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr', 'vcRecaptcha']);

registerService.factory('Register', function($http){
    return {
        register: function(user){
            return $http({
                method:'POST',
                url: pathUrl + '/api/register',
                headers:{  "Content-Type" : 'application/json' },
                data: user
            });
        }
    }
});

registerService.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'RegisterController',
            templateUrl: pathUrl + '/assets/backend/assets/frontend/pages/scripts/register/partial/register.html'
        });
}]);

