angular.module('NewsService').controller('CommentController', function($scope, $http, Comment, CommentFormService) {
        // object to hold all the data for the new comment form
        $scope.default = pathUrl + '/upload/avatars/default-avatar.png';
        $scope.commentData = {};
        $scope.childCommentData = {};
        // loading variable to show the spinning loading icon
        $scope.loading = true;
        $scope.newCommentData = {};

        $scope.newCommentData.parent = 0;
        $scope.newCommentData.postId = laravelIdNews;
        $scope.idNews = laravelIdNews;

        $scope.initForm = function(){
            if($("#isLogin").val().localeCompare("1") === 0){
               // alert($("#username").val());
                $("#author").val($("#username").val());
                $("#author-email").val($("#email").val());
                $("#author-email").prop('disabled', true);
                $("#author").prop('disabled', true);
                $scope.newCommentData.email = $("#email").val();
                $scope.newCommentData.name = $("#username").val();

            }
        }


        Comment.get($scope.idNews)
        .success(function(getData) {
            $scope.comments = getData;
            $scope.count = $scope.comments.length;
            $scope.loading = false;
            $('.comment-main-form').show('fast');
                $scope.initForm();
        });


        $scope.getComments = function(id){
            $scope.idNews = id;
            Comment.get(id)
                .success(function(data) {
                    console.log(data);
                    $scope.comments = data;

                    //$scope.loading = false;
                });
        }

        // function to handle submitting the form
        // SAVE A COMMENT ======================================================
        $scope.submitComment = function() {
            console.log('save comment');
            console.log($scope.newCommentData);
            // save the comment. pass in comment data from the form
            // use the function we created in our service
            Comment.save($scope.newCommentData)

                .success(function(data) {

                    // if successful, we'll need to refresh the comment list
                    Comment.get($scope.idNews)
                        .success(function(getData) {
                            console.log('success');
                            Comment.get($scope.idNews)
                                .success(function(data) {
                                    console.log(data);
                                    $scope.comments = data;

                                    //$scope.loading = false;
                                });
                            $scope.commentData={};
                            //if($scope.newCommentData.parent !=-1){
                                $scope.resetComposeComment();
                            //}
                        });

                })
                .error(function(data) {
                    console.log(data);
                });
        };


        $scope.closeComposeComment = function(id){
            $scope.childCommentData = {};
            $('.post-comment-' + id).hide();
            $('.comment-main-form').show('fast');
        }

        $scope.showComposeComment = function(id){
            //$scope.template = CommentFormService();
            var commentform = $('#commentform');
            $('#comment-' + id).append(commentform);
            $scope.newCommentData = {};
            $scope.newCommentData.parent = id;
            $scope.newCommentData.postId = $scope.idNews;
            $scope.initForm();
        };

        $scope.resetComposeComment = function(){
            var commentform = $('#commentform');
            $("#bottom-comment-placeholder").append(commentform);
            $scope.newCommentData = {};
            $scope.newCommentData.parent = 0;
            $scope.newCommentData.postId = $scope.idNews;
            $scope.initForm();
        }
        $scope.onCancel = function(){
            $scope.resetComposeComment();
        }

        $scope.passParentCommentID = function(parent, postId){
            $scope.childCommentData.parent = id;
            $scope.childCommentData.postId = postId;
        }

        // function to handle deleting a comment
        // DELETE A COMMENT ====================================================
        $scope.deleteComment = function(id) {
            $scope.loading = true;

            // use the function we created in our service
            Comment.destroy(id)
                .success(function(data) {
                    // if successful, we'll need to refresh the comment list
                    Comment.get()
                        .success(function(getData) {
                            $scope.comments = getData;
                            $scope.loading = false;
                        });

                });
        };

    });
