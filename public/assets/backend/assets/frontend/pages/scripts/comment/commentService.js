/**
 * Created by Nadare on 12/20/14.
 */

'use strict';
var newsService = angular.module('NewsService',['ngRoute','ngAnimate']);
newsService.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/',{
            controller: 'CommentController',
            templateUrl: pathUrl + '/assets/backend/assets/frontend/pages/scripts/comment/partial/metronic_comment.html'

        });



}]);



newsService.factory('Comment', function($http){
       return {
           get : function(id){

               return $http.get(pathUrl + '/api/comments/'+id);
           },

           save: function(commentData){
               return $http({
                   method:'POST',
                   url: pathUrl + '/api/comments',
                   headers:{ "Content-Type" : 'application/x-www-form-urlencoded' },
                   data: $.param(commentData)
               });
           },

           destroy : function(id){
               return $http.delete(pathUrl + '/api/comments/'+id);
           }
       }
    });
newsService.factory('CommentFormService', function(){
    return function(){
        return '<div><p ng-repeat="item in items">{{testCounter}}</p></div>';
    }

});

newsService.directive('formComment', function($compile, $timeout){
    return {
        scope: true,
        link: function ( scope, element, attrs ) {
            var el;
            attrs.$observe('template', function ( tpl ) {

                if ( angular.isDefined( tpl ) ) {
                    // compile the provided template against the current scope
                    alert(scope.testCounter);
                    el = $compile( tpl )( scope );
                    console.log(el);

                    // stupid way of emptying the element
                   // element.html("");
                    // add the template content
                    element.append( el );
                }
            });
        }
    };
});
