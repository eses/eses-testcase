/**
 * Created by Nadare on 1/10/15.
 */
angular.module('UserManagement').controller('UserController', function($scope, $compile, $routeParams, $http, DTOptionsBuilder, DTColumnBuilder, UserDAOService){


    $scope.lang = JSON.parse(lang);


    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/users/partials/tools.html';

    $scope.optStatus = [
        { label: $scope.lang.active, value: 1 },
        { label: $scope.lang.deactive, value: 0 }
    ];
    $scope.arrUsers = [];
    $scope.arrRoles = [];
    $scope.arrUsernames = [];
    UserDAOService.getUserRoles()
        .success(function(data) {
            $scope.optRoles = data;
            console.log(data);
     });


    /**
     * Create USER table
     */
    $scope.dtOptions_user = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'api/users',
            type: 'GET',
            dataSrc: function (data) {
                $scope.arrUsers = [];
                $scope.$parent.newUser = {};
                $scope.$parent.newUser.status = 1;
                $scope.$parent.arrUsers = [];
                $scope.$parent.arrUsernames = [];
                $.each(data.data, function (key, value) {
                    $scope.arrUsers[value['username']]= value;
                    $scope.$parent.arrUsers[value['username']]= value;
                });
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
          },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {

                var index = $scope.arrUsernames.indexOf(data.username)
                if (index == -1){
                    $scope.arrUsernames.push(data.username);
                    $scope.$apply();
                    $(row).addClass('selected');
                } else {
                    $scope.arrUsernames.splice(index, 1);
                    $scope.$apply();
                    $(row).removeClass('selected');
                }
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });
// Custom data user table
    $scope.dtColumns_user = [

        DTColumnBuilder.newColumn(null).withTitle($scope.lang.avatar).renderWith(function(data){
            return '<img alt="" src="' + pathUrl + data.avatar  +'"' + ' width="30" height="30" class="img-circle">'
            }


        ),//.withClass('idWidth'),
        DTColumnBuilder.newColumn('username').withTitle($scope.lang.user_name),
        DTColumnBuilder.newColumn('email').withTitle($scope.lang.email),
        DTColumnBuilder.newColumn('first_name').withTitle($scope.lang.first_name),
        DTColumnBuilder.newColumn('last_name').withTitle($scope.lang.last_name),
        DTColumnBuilder.newColumn('role').withTitle($scope.lang.role)
            .renderWith(function(role){
                return $scope.$parent.optRoles[role-1].name;
            }),
        DTColumnBuilder.newColumn('phone').withTitle($scope.lang.phone),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.status)
            .renderWith(function (data) {
                if (data.status === 1) {
                    return '<span class="label bg-green-meadow">Active</span>';
                } else if (data.status === 0) {
                    return '<span class="label label-warning">Inactive</span>';
                }
            }),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.action).notSortable()
            .renderWith(function (data) {
               // if(data.role !== 1)
                return '<a class="btn btn-xs btn-warning"  href="#/update/'+ data.id + '"'+
                    'id="update_' + data.id + '">' +
                    '<i class="fa fa-edit"></i>' +
                    '</a>&nbsp;'
                //return "";
            })
            //.withClass('tools')
    ];

    /**
     * Create USER_ROLE table
     */
    $scope.dtOptions_role = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'api/users/roles/datatable',
            type: 'GET',
            dataSrc: function (data) {

                $.each(data.data, function (key, value) {
                    $scope.$parent.arrRoles[value['name']]= value;
                });
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        /*rowCallback: function(row, data){
            $('td', row).on('click', function(event) {

                var index = $scope.arrUsernames.indexOf(data.username)
                if (index == -1){
                    $scope.arrUsernames.push(data.username);
                    $scope.$apply();
                    $(row).addClass('selected');
                } else {
                    $scope.arrUsernames.splice(index, 1);
                    $scope.$apply();
                    $(row).removeClass('selected');
                }
            });
        },*/
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            this;
        }
    });

    $scope.dtColumns_role = [

        DTColumnBuilder.newColumn('id').withTitle($scope.lang.no),
        DTColumnBuilder.newColumn('name').withTitle($scope.lang.name),
        DTColumnBuilder.newColumn('description').withTitle($scope.lang.description),
        //DTColumnBuilder.newColumn(null).withTitle('Role'),
        DTColumnBuilder.newColumn('updated_at').withTitle($scope.lang.update_date),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.action).notSortable()
            .renderWith(function (data) {
                return '<button class="btn default"  data-toggle="modal" href="#responsive"' +
                    'ng-click="getRoleInfo(' + "'" + data.name + "'" + ')" id="update_' + data.name + '">' +
                    '   <i class="fa fa-edit"></i>' +
                    '</button>&nbsp;'
            })
        //.withClass('tools')
    ];

    //END USER_ROLE TABLE

    $scope.getRoleInfo = function(roleName){
        var roleData = $scope.arrRoles[roleName];
        console.log("role");
        console.log($scope.arrRoles);
        //Do somthing here
    };

    $scope.getUserInfo = function(){
        console.log('user id:');
        console.log($routeParams.id);
        $scope.updatedUser= {};
        $http.post('api/users/getUserById', {'id':$routeParams.id})
            .success(function(data){
                console.log('success');
                $scope.updatedUser = data;
                var userData = $scope.updatedUser;

               /* if(userData.status == 1 ){
                    $("#cbStatus").parent().parent().removeClass("bootstrap-switch-off").addClass("bootstrap-switch-on");
                }else{
                    $("#cbStatus").parent().parent().removeClass("bootstrap-switch-on").addClass("bootstrap-switch-off");
                }*/

                $scope.role = $scope.optRoles[userData.role-1];

                var avatar = pathUrl + 'upload/avatars/default.jpg';
                userData['avatar'] = avatar;
            })
            .error(function(){
                toastr['error']($scope.lang.update_user_profile_failed);
            });


        //$scope.$parent.role = userData['roleObject'] = $scope.optRoles[userData.role-1];
        //$scope.$parent.userInfo = userData;


    };

    $scope.resetPassword = function(){
        $scope.updatedUser.password = "12345?a";
    };

    $scope.changeStatus = function(){

        if($scope.updatedUser.status == 1){
            $scope.updatedUser.status = 0;
        }else{
            $scope.updatedUser.status = 1;
        }
    }

    $scope.updateUser = function(){
        UserDAOService.upsertUser($scope.updatedUser)
            .success(function(data){
                if( data['error'] !== 1){
                    toastr['success']($scope.lang.update_user_profile_successfully);
                }else{
                    toastr['error']($scope.lang.update_user_profile_failed);
                }
                $scope.arrUsers[$scope.updatedUser.username] = angular.copy($scope.updatedUser);
                $scope.userInfo = {};
                $scope.dtOptions_user.reloadData();
            })
            .error(function(data){
                $scope.userInfo = {};
                    toastr['error']($scope.lang.update_user_profile_failed);
            })
    };

    $scope.addUser = function(){

        $userData = angular.copy($scope.newUser);

        $scope.newUser.status = 1;
        UserDAOService.addUser($userData)
            .success(function(data){
                $scope.newUser = {};
                $scope.newUser.status=1;
                $scope.newUser.role=1;
                $scope.formAddUser.$setPristine();
                if(data['error'] === 1) {
                    toastr['error'](data['message']);
                }else{
                    toastr['success']($scope.lang.adding_user_successfully);
                   // window.location.reload();
                }
            })
            .error(function(data){
                $scope.newUser.password = '';
                $scope.newUser.repassword = '';
                $scope.formAddUser[data['name']].$setValidity('unique', false);
                toastr['error']['']

            })
    };
    //upser user's information

    $scope.tabClick = function(activeId, deactiveId){

        $("#"+activeId).addClass('active');
        $("#a_"+activeId).parent().addClass('active');

        $("#"+deactiveId).removeClass('active');
        $("#a_"+deactiveId).parent().removeClass('active');

    }

});