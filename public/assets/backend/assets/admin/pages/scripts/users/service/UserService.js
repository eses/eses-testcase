
/**
 * Created by Nadare on 1/10/15.
 */
'use strict';
var UserManagement = angular.module('UserManagement',['ngRoute','ngAnimate','ngResource','ngCookies','chieffancypants.loadingBar','blockUI','toastr','datatables', 'esesDirective']);
UserManagement.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'UserController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/users/partials/index.html'
        })
        .when('/create',{
            controller: 'UserController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/users/partials/addUser.html'
        }).when('/update/:id',{
            controller: 'UserController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/users/partials/editUser.html'
        });
       /* .otherwise({
            redirectTo: '/'
        });*/


}]);



UserManagement.factory('UserDAOService', function($http){
    return {
        getUser : function(username){
            return $http({
                method: 'POST',
                url: pathUrl + '/api/users',
                headers:{ "Content-Type" : 'application/x-www-form-urlencoded' },
                data: $.param(username)
            });
        },

        upsertUser : function(userData){
            console.log(userData);
            return $http({
                method: 'PUT',
                url: pathUrl + 'backend/api/users/upsert',
                headers:{ "Content-Type" : 'application/json' },
                data: userData
            });
        },
        addUser : function(userData){
            console.log(userData);
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/api/users/add',
                headers:{ "Content-Type" : 'application/json' },
                data: userData
            });
        },
        getUserRoles : function(){

            return $http.get(pathUrl + '/backend/api/users/roles');
        }

         }
});

/*
UserManagement.directive('esesUnique', function(UserDAOService) {
    return {
        restrict: 'A', // only activate on element attribute
        require: 'ngModel',

        link: function(scope, element, attrs, ngModel) {
            scope.isUnique = true;
            element.bind('blur', function (e) {
                if(!ngModel || !element.val() ) return; // do nothing if no ng-model
                var dataInput = scope.$eval(attrs.esesUnique);
                UserDAOService.verify(dataInput.value, dataInput.name)
                    .success(function(data){
                        if(data){
                            console.log('du lieu chua ton tai');
                            ngModel.$setValidity('unique', true);
                         //   ngModel.$invalid = true;
                        }else{
                            ngModel.$setValidity('unique', false);
                           // ngModel.$invalid = false;
                        }

                    })
                    .error(function(data){
                        ngModel.$setValidity('unique', false);
                    });

            });
        }
    }
});
    */