var TestCase = angular.module('TestCase',['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr','datatables']);
TestCase.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/add',{
            controller: 'TestCaseController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/addtestlink.html'
        })
        .when('/update/:id',{
            controller: 'TestCaseController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/updatetestlink.html'
        })
        .when('/testcase/getall/:id',{
            controller: 'TestCaseController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/tbcase.html'
        })
        .when('/testcase/add/:id',{
            controller: 'TestCaseController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/addtestcase.html'
        })
        .when('/testcase/update/:id',{
            controller: 'TestCaseController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/updatetestcase.html'
        })
        .otherwise({
            controller: 'TestCaseController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/tblink.html'
        });
}]);


TestCase.factory('TestCaseDAO', function($http){
    return {
        insertTestLink : function(testlink){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/add',
                headers:{ "Content-Type" : 'application/x-www-form-urlencoded' },
                data: $.param(testlink)
            });
        },

        updateTestLink : function(testlink){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/update',
                headers:{ "Content-Type" : 'application/json' },
                data: testlink
            });
        },

        deleteTestLink : function(id){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/delete',
                headers:{ "Content-Type" : 'application/json' },
                data: id
            });
        },
        getTestLink : function(id){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/get',
                headers:{ "Content-Type" : 'application/json' },
                data: {'id':id}
            });
        },
        getTestCaseByTestLink : function(id){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/testcase/getall',
                headers:{ "Content-Type" : 'application/json' },
                data: {'id':id}
            });
        },
        getTestCase : function(id){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/testcase/get',
                headers:{ "Content-Type" : 'application/json' },
                data: {'id':id}
            });
        },

        insertTestCase : function(testcase){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/testcase/add',
                headers:{ "Content-Type" : 'application/x-www-form-urlencoded' },
                data: $.param(testcase)
            });
        },

        updateTestCase : function(testcase){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/testlink/testcase/update',
                headers:{ "Content-Type" : 'application/json' },
                data: testcase
            });
        },

        deleteTestCase : function(id){
            return $http({
                method: 'POST',
                url: pathUrl +  'backend/testlink/testcase/delete',
                headers:{ "Content-Type" : 'application/json' },
                data: id
            });
        }
    }
});