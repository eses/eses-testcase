/**
 * Created by Nadare on 4/17/15.
 */
angular.module('TestCase').controller('TestCaseController', function($scope, $compile, $routeParams, $http, DTOptionsBuilder, DTColumnBuilder,  TestCaseDAO){

    //for tool
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/tools.html';
    $scope.tools_testcase = pathPublic + 'assets/backend/assets/admin/pages/scripts/testcase/partial/tools_testcase.html';

    //for loading Table Data TESTLINK
    $scope.dtOptions_testlink = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'testlink/getall',
            type: 'GET',
            dataSrc: function (data) {
                console.log(data);
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    //for testlink Table RENDER
    $scope.dtColumns_testlink = [
        DTColumnBuilder.newColumn('id').withTitle("ID"),
        DTColumnBuilder.newColumn('name').withTitle("Name"),
        DTColumnBuilder.newColumn('link').withTitle("Link"),
        DTColumnBuilder.newColumn('config').withTitle("Configuration"),
        DTColumnBuilder.newColumn('updated_at').withTitle("Updated At"),
        DTColumnBuilder.newColumn(null).withTitle("Action").notSortable()
            .renderWith(function (data) {
                // if(data.role !== 1)
                return '<a class="btn btn-sm btn-warning"  href="#/update/'+ data.id + '">Edit<i class="fa fa-edit"></i></a>&nbsp;'
                     +  '<a class="btn btn-sm btn-default" href="#/testcase/getall/'+data.id+'">View</a>';
            })
    ];



    //add test link
    $scope.addTestLink = function(){
        console.log("testlink=");
        console.log($scope.newTestLink);
        TestCaseDAO.insertTestLink($scope.newTestLink)
            .success(function(response){
                console.log("response=");
                console.log(response);
                if(response['error'] === 1) {
                    toastr['error'](response['message']);
                }else{
                    toastr['success'](response['message']);
                    $scope.newTestLink = {};
                }
            })
            .error(function(response){
                toastr['error'](response['message']);
            });
    };

    $scope.getTestLink = function(){
        console.log("test link id="+ $routeParams.id);
        $scope.updatedTestLink = {};
        TestCaseDAO.getTestLink($routeParams.id)
            .success(function(response){
                $scope.updatedTestLink = response;
            })
            .error(function(response){
                toastr['error'](response['message']);
            });
    }

    $scope.updateTestLink = function(){
        console.log("update teslink id=" + $scope.updatedTestLink.id);
        TestCaseDAO.updateTestLink($scope.updatedTestLink)
            .success(function(response){
                console.log("resposne=" + response );
                if(response['error'] === 1) {
                    toastr['error'](response['message']);
                }else{
                    toastr['success'](response['message']);
                }
            })
            .error(function(response){
                toastr['error'](response['message']);
            })
    }
    ///////////TEST CASE
    //for loading Table Data TESTLINK
    $scope.dtOptions_testcase = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'testlink/testcase/getall',
            data: {id: $routeParams.id},
            type: 'GET',
            dataSrc: function (data) {
               // if(data.data.length > 0){
                    //$scope.getTestLink(data.data[0].link_ref_id);
                console.log("id parent = " + data.idTestLink);
                    $scope.getTestLink(data.idTestLink);
                //}
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    //for TESTCASE Table RENDER
    $scope.dtColumns_testcase = [
        DTColumnBuilder.newColumn('id').withTitle("ID"),
        DTColumnBuilder.newColumn('name').withTitle("Name"),
        DTColumnBuilder.newColumn('testcase_id').withTitle("TestCase ID"),
        DTColumnBuilder.newColumn('action_no').withTitle("Action No"),
        DTColumnBuilder.newColumn('updated_at').withTitle("Updated At"),
        DTColumnBuilder.newColumn(null).withTitle("Action").notSortable()
            .renderWith(function (data) {
                // if(data.role !== 1)
                return '<a class="btn btn-sm btn-warning"  href="#/testcase/update/'+ data.id + '">Edit<i class="fa fa-edit"></i></a>';
            })
    ];


    $scope.getTestCaseByTestLink = function(){
        console.log("test link id="+ $routeParams.id);
        $scope.updatedTestCase = {};
        TestCaseDAO.getTestCaseByTestLink($routeParams.id)
            .success(function(response){
                $scope.updatedTestCase = response;
            })
            .error(function(response){
                toastr['error'](response['message']);
            });
    }

    $scope.getTestCase = function(){
        console.log("test case id="+ $routeParams.id);
        $scope.updatedTestCase = {};
        TestCaseDAO.getTestCase($routeParams.id)
            .success(function(response){
                console.log(response)
                $scope.updatedTestCase = response;
            })
            .error(function(response){
                toastr['error'](response['message']);
            });
    }

    $scope.addTestCase = function(){
        $scope.newTestCase.link_ref_id = $scope.updatedTestLink.id;
        TestCaseDAO.insertTestCase($scope.newTestCase)
            .success(function(response){
                toastr['success'](response['message']);
                $scope.newTestCase = {};
            })
            .error(function(response){
                toastr['error'](response['message']);
            });
    }

    $scope.updateTestCase = function(){
        console.log("update tescase id=" + $scope.updatedTestCase.id);
        TestCaseDAO.updateTestCase($scope.updatedTestCase)
            .success(function(response){
                console.log("resposne=" + response );
                if(response['error'] === 1) {
                    toastr['error'](response['message']);
                }else{
                    toastr['success'](response['message']);
                }
            })
            .error(function(response){
                toastr['error'](response['message']);
            })
    }
});
