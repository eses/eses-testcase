angular.module("appSettingSiteInfo").controller('settingPageInfoController', function ($scope, $routeParams, $compile, $http) {

    //Init variable
    $scope.siteInfo = [];
    $scope.lang = JSON.parse(lang);
    // Save update
    $scope.updateSiteInfo = function () {
        $http.post('siteinfo/api/updateSiteInfo', $scope.siteInfo)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']('Save site infomation fail');
            });
        $scope.arrStaticPageId = [];
    };


    // Get update
    $scope.getSiteInfoUpdate = function(){
        $http.post('siteinfo/api/getSiteInfo')
            .success(function(data){
                $scope.siteInfo = data;
            })
            .error(function(){
                toastr['error']('Load site infomation error');
            });
    };
});

