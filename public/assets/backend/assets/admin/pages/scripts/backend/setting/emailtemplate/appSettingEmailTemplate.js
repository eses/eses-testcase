var appSettingEmailTemplate = angular.module("appSettingEmailTemplate", ['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr','ui.codemirror']);

appSettingEmailTemplate.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'settingEmailTemplateController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/backend/setting/emailtemplate/partials/index.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
