angular.module("appSettingEmailTemplate").controller('settingEmailTemplateController', function ($scope, $routeParams, $compile, $http) {

    //Init variable
    $scope.emailTemplate = [];
    $scope.lang = JSON.parse(lang);
    // Save update
    $scope.updateEmailTemplate = function () {
        $http.post('emailtemplate/api/updateEmailTemplate', $scope.emailTemplate)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']('Save site infomation fail');
            });
    };


    // Get update
    $scope.getEmailTemplateUpdate = function(){
        $http.post('emailtemplate/api/getEmailTemplate')
            .success(function(data){
                $scope.emailTemplate = data;
            })
            .error(function(){
                toastr['error']('Load site infomation error');
            });
    };

    //Init ui
    $scope.editorOptions = {
        theme: "eclipse",
        lineWrapping : true,
        lineNumbers: true,
        mode: "text/html"
    };
});

