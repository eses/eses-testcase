var appSettingSiteInfo = angular.module("appSettingSiteInfo", ['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr']);

appSettingSiteInfo.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'settingPageInfoController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/backend/setting/siteinfo/partials/index.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
