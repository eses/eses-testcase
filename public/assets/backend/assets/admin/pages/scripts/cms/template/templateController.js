angular.module("appTemplate").controller('templateController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {
    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/template/partials/main-tools.html';
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/template/partials/tools.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrTemplateId = [];
    // Create paging for data table template
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'template/api/getAllTemplate',
            type: 'GET',
            dataSrc: function (data) {
                $scope.arrTemplateId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        filter: false,
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrTemplateId.indexOf(data.id)
                if (index == -1){
                    $scope.arrTemplateId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrTemplateId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrTemplateId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table template
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn('template_code').withTitle($scope.lang.code),
        DTColumnBuilder.newColumn('name').withTitle($scope.lang.name),
        DTColumnBuilder.newColumn('description').withTitle($scope.lang.description),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.default)
            .renderWith(function (data) {
                if (data.default == '1') {
                    return '<label class="label label-sm label-success">'+scope.lang.yes+'</label>';
                } else if (data.default != '1') {
                    return '<label class="label label-sm label-danger">'+scope.lang.no+'</label>';
                }
            }),
        DTColumnBuilder.newColumn(null).withTitle('Status')
            .renderWith(function (data) {
                if (data.status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.publish+'</label>';
                } else if (data.categories_status == '0') {
                    return '<label class="label label-sm label-warning">'+$scope.lang.unpublish +'</label>';
                } else {
                    return '<label class="label label-sm label-danger">'+$scope.lang.draft+'</label>';
                }
            })
            .withClass('colStatus'),
        DTColumnBuilder.newColumn(null).withTitle('Tools').notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<a class="btn btn-xs btn-warning"  href="#/update/' + data.id + '" id="update_' + data.id + '">' +
                    '   <i class="fa fa-edit"></i>' +
                    '</a>&nbsp;' +
                    '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                    '   <i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
            .withClass('colTools')
    ];

    // Save categories
    $scope.save = function () {
        $http.post('template/api/saveTemplate', $scope.newTemplate)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_template_fail);
            });
        $scope.arrTemplateId = [];
    };

    // Get update categories
    $scope.getUpdate = function(){
        $http.post('categories/api/getCategoriesById', {'id':$routeParams.id})
            .success(function(data){
                $scope.categories = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_categories_update_false);
            });
    };

    // Update categories
    $scope.update = function () {
        $http.post('categories/api/updateCategories', $scope.categories)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function () {
                toastr['error']($scope.lang.get_categories_update_false);
            });
        $scope.arrCategoriesId = [];
    };

    // Delete template
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('template/api/deleteTemplate', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrCategoriesId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_template_fail);
                    })
            }
        });
    };

    // Delete multi categories
    $scope.deleteMultiCategories = function () {
        bootbox.confirm("Are you sure want to delete?", function(result) {
            if(result){
                console.log($scope.arrCategoriesId);
                $http.post('categories/api/deleteMultiCategories', {'ids': $scope.arrCategoriesId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrCategoriesId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_template_fail);
                    })
            }
        });
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrCategoriesId = [];
        $('#categories tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrCategoriesId.push(id);
        })
        console.log($scope.arrCategoriesId);
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#categories tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrCategoriesId = [];
        });
        console.log($scope.arrCategoriesId);
    };

    // Clear form add categories
    $scope.reset = function () {
        $scope.newCategories.name = "";
        $scope.newCategories.alias = "";
        $scope.newCategories.status = "1";
        $scope.newCategories.parent = "0";
        $('#name').focus();
    };

    // Load categories
    $scope.loadCategories =  function(){
        $http.post('categories/api/getAllCategories')
            .success(function(data){
                $scope.optionParents = data;
                //handleCategories.init();
            })
            .error(function(){
                toastr['error']($scope.lang.load_categories_error);
            });
    };

    // Load categories without id
    $scope.loadCategoriesWithoutId =  function(){
        $http.post('categories/api/getAllCategoriesWithoutId',{'id':$routeParams.id})
            .success(function(data){
                var arr = new Array({'id':0,'parent_id':0,'categories_name':$scope.lang.root});
                $scope.optionParents = arr.concat(data);
                //handleCategories.init();
            })
            .error(function(){
                toastr['error']($scope.lang.load_categories_error);
            });
    };

    // Load menu type create
    $scope.loadMenuType = function(){
        $http.post('static/api/getMenuType')
            .success(function(data){
                $scope.optionMenuTypes = data;
                $.each(data, function(key,value){
                    if(value.static_default == 1){
                        $scope.newCategories.menuType = value.static_key;
                    }
                });
            })
            .error(function(){
                toastr['error']($scope.lang.load_menu_type_error);
            });
    };

    // Load menu type update
    $scope.loadMenuTypeUpdate = function(){
        $http.post('static/api/getMenuType')
            .success(function(data){
                $scope.optionMenuTypes = data;
            })
            .error(function(){
                toastr['error']($scope.lang.load_menu_type_error);
            });
    };

    //Load display config categories nestable
    $scope.getDisplayCategoriesConfig =  function(){
        $http.post('categories/api/displayConfigCategories')
            .success(function(data){
                $('#displayCategories').append(data);
                handleCategories.initConfig();
            })
            .error(function(){
                toastr['error']($scope.lang.load_categories_error);
            });
    };

    //Save configuration categories
    $scope.saveConfig = function(){
        var dataConfig = window.JSON.stringify($('#displayCategories').nestable('serialize'));
        $http.post('categories/api/saveConfigCategories', {'config':dataConfig})
            .success(function(data){
                $.each(data, function (key, value) {
                    toastr[key](value);
                });
            })
            .error(function(){
                toastr['error']($scope.lang.save_configuration_categories_error);
            });
    };
});
