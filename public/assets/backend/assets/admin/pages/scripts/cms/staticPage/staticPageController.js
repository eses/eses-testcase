angular.module("appStaticPage").controller('staticPageController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/staticPage/partials/tools.html';
    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/staticPage/partials/main-tools.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrStaticPageId = [];
    // Create paging for data table news
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'staticpage/api/getStaticPage',
            type: 'GET',
            dataSrc: function (data) {
                $scope.arrStaticPageId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        filter: false,
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrStaticPageId.indexOf(data.id);
                if (index == -1){
                    $scope.arrStaticPageId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrStaticPageId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrStaticPageId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table categories
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
     //   DTColumnBuilder.newColumn('post_link').withTitle($scope.lang.page_id),
        DTColumnBuilder.newColumn('post_title').withTitle($scope.lang.name),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.status)
            .renderWith(function (data) {
                if (data.post_status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.publish+'</label>';
                } else if (data.post_status == '0') {
                    return '<label class="label label-sm label-warning">'+$scope.lang.unpublish +'</label>';
                } else {
                    return '<label class="label label-sm label-danger">'+$scope.lang.draft+'</label>';
                }
            })
            .withClass('colStatus'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<a class="btn btn-xs btn-warning"  href="#/update/' + data.id + '" id="update_' + data.id + '">' +
                    '   <i class="fa fa-edit"></i>' +
                    '</a>&nbsp;' +
                  // '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                 //   '   <i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
            .withClass('colTools')
    ];

    // Save static page
    $scope.save = function () {
        $http.post('staticpage/api/saveStaticPage', $scope.newStaticPage)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_static_page_fail);
            })
        $scope.arrStaticPageId = [];
    };

    // Save update static page
    $scope.saveUpdate = function () {
        $http.post('staticpage/api/updateStaticPage', $scope.staticPage)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_static_page_fail);
            })
        $scope.arrStaticPageId = [];
    };


    // Get update static page by id
    $scope.getStaticPageUpdate = function(){
        $http.post('staticpage/api/getStaticPageById', {'id':$routeParams.id})
            .success(function(data){
                $scope.staticPage = data;
            })
            .error(function(){
                toastr['error']($scope.lang.load_static_page_update_error);
            });
    };

    // Delete news
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('staticpage/api/deleteStaticPage', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrStaticPageId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_static_page_fail);
                    })
            }
        });
    };

    // Delete multi static page
    $scope.deleteMultiStaticPage = function () {
        bootbox.confirm("Are you sure want to delete?", function(result) {
            if(result){
                $http.post('staticpage/api/deleteMultiStaticPage', {'ids': $scope.arrStaticPageId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrStaticPageId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_static_page_fail);
                    })
            }
        });
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrStaticPageId = [];
        $('#news tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrStaticPageId.push(id);
        })
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#news tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrStaticPageId = [];
        });
    };

    // Clear form add static page
    $scope.reset = function () {
        $scope.newStaticPage.title = "";
        $scope.newStaticPage.url = "";
        $scope.newStaticPage.status = "1";
        $scope.newStaticPage.post = "";
        $('#title').focus();
    };

    $scope.initCreate = function(){
        FormCreate.init();
        $scope.$apply();
    }

    $scope.tinymceOptions = {
        relative_urls: true,
        document_base_url: pathPublic,
        height: 500,
        plugins : ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor codemirror gmap"],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | code | gmap",
        image_advtab: true,
        file_browser_callback : fileBrowser,
        codemirror: {
            indentOnInit: true, // Whether or not to indent code on init.
            path: 'codemirror', // Path to CodeMirror distribution
            config: {           // CodeMirror config object
                mode: 'application/x-httpd-php',
                lineNumbers: false
            },
            jsFiles: [          // Additional JS files to load
                'mode/clike/clike.js',
                'mode/php/php.js'
            ]
        }
    };
});

