angular.module("appMedia").controller('mediaController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/media/partials/main-tools.html';
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/media/partials/tools.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrMediaId = [];
    // Create paging for data table media
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'media/api/getMedia',
            type: 'GET',
            dataSrc: function (data) {
                $scope.arrMediaId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        filter: false,
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrMediaId.indexOf(data.id);
                if (index == -1){
                    $scope.arrMediaId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrMediaId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrMediaId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table categories
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.image)
            .renderWith(function(data){
                if(data.media_mime_type.indexOf('image') > -1){
                    return '<img src="' + '../../' + data.media_url + '" style="width:40px;height:30px;">';
                } else {
                    return '<i class="fa fa-file"></i>';
                }
            }),
        DTColumnBuilder.newColumn('media_name').withTitle($scope.lang.name),
        DTColumnBuilder.newColumn('media_mime_type').withTitle($scope.lang.mine_type),
        DTColumnBuilder.newColumn('media_size').withTitle($scope.lang.size),
        DTColumnBuilder.newColumn('media_extension').withTitle($scope.lang.extension),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                    '   <i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
            .withClass('colTools')
    ];

    // Delete media
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('media/api/deleteMedia', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrMediaId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_media_fail);
                    })
            }
        });
    };

    // Delete multi media
    $scope.deleteMultiMedia = function () {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                console.log($scope.arrMediaId);
                $http.post('media/api/deleteMultiMedia', {'ids': $scope.arrMediaId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrMediaId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_media_fail);
                    })
            }
        });
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrMediaId = [];
        $('#media tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrMediaId.push(id);
        });
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#media tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrMediaId = [];
        });
        console.log($scope.arrMediaId);
    };
});
