angular.module("appSlide").controller('slideController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/main-tools.html';
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/tools.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrSlideId = [];
    // Create paging for data table categories
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'slide/api/getSlide',
            type: 'GET',
            dataSrc: function (data) {
                $scope.arrSlideId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        filter: false,
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrSlideId.indexOf(data.id)
                if (index == -1){
                    $scope.arrSlideId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrSlideId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrSlideId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table categories
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn('slide_name').withTitle($scope.lang.name),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.status)
            .renderWith(function (data) {
                if (data.slide_status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.publish+'</label>';
                } else if (data.slide_status == '0') {
                    return '<label class="label label-sm label-warning">'+$scope.lang.unpublish +'</label>';
                } else {
                    return '<label class="label label-sm label-danger">'+$scope.lang.draft+'</label>';
                }
            })
            .withClass('colStatus'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<a class="btn btn-xs btn-warning" href="#detail/' + data.id + '" id="detail_' + data.id + '">' +
                    '   <i class="fa fa-gear"></i>' +
                    '</a>&nbsp;' +
                  //  '<a class="btn btn-xs btn-warning" href="#update/' + data.id + '" id="update_' + data.id + '">' +
                   // '   <i class="fa fa-edit"></i>' +
                   // '</a>&nbsp;' +
                    '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                    '   <i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
            .withClass('colTools')
    ];

    // Save slide
    $scope.save = function () {
        $http.post('slide/api/saveSlide', $scope.newSlide)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_slide_fail);
            })
        $scope.arrSlideId = [];
    };

    // Get update slide
    $scope.getUpdate = function(){
        $http.post('slide/api/getSlideById', {'id':$routeParams.id})
            .success(function(data){
                $scope.slide = data;
            })
            .error(function(){
                toastr['error']('Get slide update false');
            });
    };

    // Update slide
    $scope.updateSlide = function () {
        $http.post('slide/api/updateSlide', $scope.slide)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function () {
                toastr['error']($scope.lang.update_slide_failed);
            })
       $scope.arrSlideId = [];
    };

    //Active slide
    $scope.activeMainSlide = function () {
        $http.post('slide/api/activeMainSlide',  {'ids': $scope.arrSlideId})
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function () {
                toastr['error']($scope.lang.update_slide_failed);
            })
        $scope.dtOptions.reloadData();
        $scope.arrSlideId = [];
    };

    // Delete slide
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('slide/api/deleteSlide', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrSlideId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_slide_fail);
                    })
            }
        });
    };

    // Delete multi slide
    $scope.deleteMultiCategories = function () {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                console.log($scope.arrSlideId);
                $http.post('categories/api/deleteMultiSlide', {'ids': $scope.arrSlideId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrCategoriesId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_slide_fail);
                    })
            }
        });
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrSlideId = [];
        $('#slide tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrSlideId.push(id);
        })
        console.log($scope.arrSlideId);
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#slide tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrSlideId = [];
        });
        console.log($scope.arrSlideId);
    };

    // Clear form add slide
    $scope.reset = function () {
        $scope.newSlide.name = "";
        $scope.newSlide.alias = "";
        $scope.newSlide.status = "1";
        $('#name').focus();
    };
});
