angular.module("appNews").controller('groupController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    // Init tools
    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/main-tools.html';
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/group/toolConfig.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrGroupId = [];
    // Create paging for data table group
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'news/api/getNewsGroup',
            data: {'id': $routeParams.id},
            type: 'POST',
            dataSrc: function (data) {
                $scope.arrGroupId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrGroupId.indexOf(data.id);
                if (index == -1){
                    $scope.arrGroupId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrGroupId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrGroupId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table news group
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn('group_name').withTitle($scope.lang.group_name),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.status)
            .renderWith(function (data) {
                if (data.status !== undefined && data.status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.publish+'</label>';
                } else if (data !== null && data.status !== undefined && data.status == '0') {
                    return '<label class="label label-sm label-warning">'+$scope.lang.unpublish +'</label>';
                } else {
                    return '<label class="label label-sm label-danger">'+$scope.lang.draft+'</label>';
                }
            })
            .withClass('colStatus'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<a class="btn btn-xs btn-warning" href="#group/update/'
                    + data.id + '" id="update_' + data.id + '">' + '   <i class="fa fa-edit"></i>' +
                    '</a>&nbsp;' +
                    /*
                    '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                    '   <i class="fa fa-trash-o"></i>' +*/
                    '</button>';
            })
            .withClass('colStatus')
    ];

    // Save news group
    $scope.save = function () {
        $http.post('news/api/saveNewsGroup', $scope.newGroup)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_news_group_fail);
            });
        $scope.arrSlideId = [];
    };

    // Get update news group
    $scope.getUpdate = function(){
        $http.post('news/api/getNewsGroupById', {'id':$routeParams.id})
            .success(function(data){
                $scope.group = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_news_in_group_fail);
            });
    };

    // Update news group
    $scope.update = function () {
        $http.post('news/api/updateNewsGroup', $scope.group)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function () {
                toastr['error']($scope.lang.update_news_group_fail);
            })
        $scope.arrGroupId = [];
    };
/*
    // Delete news group
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('news/api/deleteNewsGroup', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrGroupId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_news_group_fail);
                    })
            }
        });
    };

    // Delete multi news group
    $scope.deleteMultiNewsGroup = function () {
        bootbox.confirm("Are you sure want to delete?", function(result) {
            if(result){
                console.log($scope.arrGroupId);
                $http.post('news/api/deleteMultiNewsGroup', {'ids': $scope.arrGroupId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrGroupId = [];
                    })
                    .error(function () {
                        toastr['error']('Delete multi news group fail');
                    })
            }
        });
    };
*/
    // Select all
    $scope.selectedAll = function(){
        $scope.arrGroupId = [];
        $('#group tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrGroupId.push(id);
        })
        console.log($scope.arrGroupId);
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#group tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrGroupId = [];
        });
        console.log($scope.arrGroupId);
    };

    // Clear form add slide
    $scope.reset = function () {
        $scope.newGroup.name = "";
        $scope.newGroup.status = "1";
        $('#text').focus();
    };

    $scope.getNewsGroup = function(){
        $http.post('news/api/getAllNewsGroup')
            .success(function(data){
                $scope.group = data;
            })
            .error(function(){
                toastr['error']('get news group fail');
            });
    };

});
