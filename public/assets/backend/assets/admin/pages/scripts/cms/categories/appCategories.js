
var appCategories = angular.module("appCategories", ['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr','datatables']);

appCategories.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'categoriesController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/categories/partials/index.html'
        })
        .when('/create',{
            controller: 'categoriesController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/categories/partials/create.html'
        })
        .when('/update/:id',{
            controller: 'categoriesController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/categories/partials/update.html'
        })
        .when('/config',{
            controller: 'categoriesController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/categories/partials/config.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
