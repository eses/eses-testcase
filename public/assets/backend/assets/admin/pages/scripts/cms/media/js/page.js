/**
 * Created by phans_000 on 20/1/2015.
 */

var handleCategories = function(){
    var handleSelect = function(){
        $('.select2').select2();
    }

    return {
        init: function(){
            handleSelect();
        },
        initConfig: function(){
            $('#displayCategories').nestable({
                group: 1,
                listNodeName: 'ul'
            });
        }
    }
}();
