var appSlide = angular.module("appSlide", ['ui.tinymce','ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr','datatables','ui.bootstrap']);

appSlide.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'slideController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/index.html'
        })
       // .when('/create',{
         //   controller: 'slideController',
           // templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/create.html'
        //})
      //  .when('/update/:id',{
        //    controller: 'slideController',
          //  templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/update.html'
        //})
        .when('/detail/:id',{
            controller: 'detailController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/detail/index.html'
        })
        .when('/detail/:id/create',{
            controller: 'detailController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/detail/create.html'
        })
        .when('/detail/:id/update/:idDetail',{
            controller: 'detailController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/detail/update.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
