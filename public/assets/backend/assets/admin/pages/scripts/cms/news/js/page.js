
var fileBrowser = function (field_name, url, type, win) {
    tinymce.activeEditor.windowManager.open({
        file: '../../backend/cms/media/management',// use an absolute path!
        title: 'Upload & Browser file',
        width: 900,
        height: 600,
        resizable: 'yes'
    },{
        setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
        }
    });
    return false;
};