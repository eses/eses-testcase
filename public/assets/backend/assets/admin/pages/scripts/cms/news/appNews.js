var appNews = angular.module("appNews", ['rt.select2','ngRoute','ngAnimate','ui.tinymce','chieffancypants.loadingBar','blockUI','toastr','datatables','frapontillo.bootstrap-switch']);

appNews.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'newsController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/index.html'
        })
        .when('/category',{
            controller: 'categoriesController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/categories/index.html'
        })
        .when('/category/:id',{
            controller: 'categoriesController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/categories/detail.html'
        })
        .when('/create',{
            controller: 'newsController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/create.html'
        })
        .when('/update/:id',{
            controller: 'newsController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/update.html'
        })
        .when('/config',{
            controller: 'newsController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/config.html'
        })
        .when('/group',{
            controller: 'groupController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/group/index.html'
        })
        .when('/group/create',{
            controller: 'groupController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/group/create.html'
        })
        .when('/group/update/:id',{
            controller: 'groupController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/group/update.html'
        })
        .when('/group/config',{
            controller: 'configController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/group/config.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
