angular.module("appNews").controller('categoriesController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/main-tools.html';
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/categories/tools.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrNewsId = [];
    // Create paging for data table news
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'news/api/getNewsByCategories',
            data: {id: $routeParams.id},
            type: 'POST',
            dataSrc: function (data) {
                $scope.arrNewsId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        filter: false,
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrNewsId.indexOf(data.id);
                if (index == -1){
                    $scope.arrNewsId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrNewsId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrNewsId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table categories
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.detail)
            .renderWith(function(data){
                return '<i> /' + data.post_link + '</i>'
                    + '<h5>' + data.post_title +'</h5>';
            }),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.comment)
            .renderWith(function(data){
                if (data.comment_status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.yes+'</label>';
                } else {
                    return '<label class="label label-sm label-warning">'+$scope.lang.no + '</label>';
                }
            }),
        DTColumnBuilder.newColumn('post_sort').withTitle($scope.lang.sort),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.status)
            .renderWith(function (data) {
                if (data.post_status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.publish+'</label>';
                } else if (data.post_status == '0') {
                    return '<label class="label label-sm label-warning">'+$scope.lang.unPublish +'</label>';
                } else {
                    return '<label class="label label-sm label-danger">'+$scope.lang.draft+'</label>';
                }
            })
            .withClass('colStatus'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<a class="btn btn-xs btn-warning"  href="#/update/' + data.id + '" id="update_' + data.id + '">' +
                    '   <i class="fa fa-edit"></i>' +
                    '</a>&nbsp;' +
                    '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                    '   <i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
            .withClass('colTools')
    ];

    // Save news
    $scope.save = function () {
        $http.post('news/api/saveNews', $scope.newNews)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']('Save news fail');
            })
        $scope.arrNewsId = [];
    };

    // Save update news
    $scope.saveUpdate = function () {
        $http.post('news/api/updateNews', $scope.news)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_news_fail);
            })
        $scope.arrNewsId = [];
    };


    // Get update news by id
    $scope.getNewsUpdate = function(){
        $http.post('news/api/getNewsById', {'id':$routeParams.id})
            .success(function(data){
                $scope.news = data;
            })
            .error(function(){
                toastr['error']($scope.lang.load_news_update_error);
            });
    };

    // Delete news
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('news/api/deleteNews', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrNewsId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_news_fail);
                    })
            }
        });
    };

    // Delete multi categories
    $scope.deleteMultiNews = function () {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                console.log($scope.arrCategoriesId);
                $http.post('news/api/deleteMultiNews', {'ids': $scope.arrNewsId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrNewsId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_news_fail);
                    })
            }
        });
    };

    // Get all categories
    $scope.getCategories = function(){
        $http.post('categories/api/getAllCategories')
            .success(function(data){
                $scope.categories = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_categories_fail);
            })
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrNewsId = [];
        $('#news tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrNewsId.push(id);
        })
        console.log($scope.arrNewsId);
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#news tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrNewsId = [];
        });
        console.log($scope.arrNewsId);
    };

    // Clear form add categories
    $scope.reset = function () {
        $scope.newNews.title = "";
        $scope.newNews.url = "";
        $scope.newNews.status = "1";
        $scope.newNews.commentStatus = "1";
        $scope.newNews.post = "";
        $('#title').focus();
    };

    $scope.initCreate = function(){
        FormCreate.init();
        $scope.$apply();
    }

    $scope.tinymceOptions = {
        height: 500,
        plugins : ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
        image_advtab: true,
        file_browser_callback : fileBrowser
    };
});

