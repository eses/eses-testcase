var appTemplate = angular.module("appTemplate", ['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr','datatables']);

appTemplate.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'templateController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/template/partials/index.html'
        })
        .when('/create',{
            controller: 'templateController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/template/partials/create.html'
        })
        .when('/update/:id',{
            controller: 'templateController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/template/partials/update.html'
        })
        .when('/config',{
            controller: 'templateController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/template/partials/config.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
