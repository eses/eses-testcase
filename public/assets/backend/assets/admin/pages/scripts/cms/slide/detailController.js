angular.module("appSlide").controller('detailController', function ($scope, $modal, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    // Init tools
    $scope.tools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/detail/tools.html';
    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/slide/partials/detail/main-tools.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrSlideDetailId = [];
    $scope.valueProperty = [];

    // Create paging for data table slide detail
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'slide/api/getSlideDetailBySlideId',
            data: {'id': $routeParams.id},
            type: 'POST',
            dataSrc: function (data) {
                $scope.arrSlideDetailId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrSlideDetailId.indexOf(data.id);
                if (index == -1){
                    $scope.arrSlideDetailId.push(data.id);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrSlideDetailId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrSlideDetailId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table slide detail
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn('name').withTitle($scope.lang.name),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.status)
            .renderWith(function (data) {
                if (data.status !== undefined && data.status == '1') {
                    return '<label class="label label-sm label-success">'+$scope.lang.publish+'</label>';
                } else if (data !== null && data.status !== undefined && data.status == '0') {
                    return '<label class="label label-sm label-warning">'+$scope.lang.unpublish +'</label>';
                } else {
                    return '<label class="label label-sm label-danger">'+$scope.lang.draft+'</label>';
                }
            })
            .withClass('colStatus'),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<a class="btn btn-xs btn-warning" href="#detail/' + data.slideId + '/update/'
                    + data.id + '" id="update_' + data.id + '">' + '   <i class="fa fa-edit"></i>' +
                    '</a>&nbsp;' +
                    '<button class="btn btn-xs btn-success" ng-click="showAndHiddenSlide(' + data.id + ')" id="showAndHiddenSlide_' + data.id + '">' +
                    '   <i class="fa fa-eye"></i>' +
                    '</button>&nbsp;' +
                    '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.id + ')" id="delete_' + data.id + '">' +
                    '   <i class="fa fa-trash-o"></i>' +
                    '</button>&nbsp;' +
                    '<button class="btn btn-xs btn green" ng-click="copy(' + data.id + ')" id="copy_' + data.id + '">' +
                    '   <i class="fa fa-copy"></i>' +
                    '</button>';
            })
            .withClass('colTools')
    ];

    // Save slide
    $scope.save = function () {
        $scope.newSlideDetail.slideId = $routeParams.id;
        $http.post('slide/api/saveSlideDetail', $scope.newSlideDetail)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function (data) {
                toastr['error']($scope.lang.save_slide_detail_fail);
            });
        $scope.arrSlideId = [];
    };

    // Get update slide
    $scope.getUpdateDetail = function(){
        $http.post('slide/api/getSlideDetailById', {'id':$routeParams.idDetail})
            .success(function(data){
                $scope.slide = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_slide_detail_update_false);
            });
    };

    // Update slide
    $scope.updateSlideDetail = function () {
        $http.post('slide/api/updateSlideDetail', $scope.slide)
            .success(function (data) {
                $.each(data, function (key, value) {
                    toastr[key](value);
                })
            })
            .error(function () {
                toastr['error']($scope.lang.get_slide_detail_update_false);
            });
        $scope.arrSlideId = [];
    };

    // Delete slide detail
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('slide/api/deleteSlideDetail', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrSlideDetailId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_slide_detail_fail);
                    })
            }
        });
    };


    // Delete multi slide
    $scope.deleteMultiSlideDetail = function () {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                console.log($scope.arrSlideDetailId);
                $http.post('slide/api/deleteMultiSlideDetail', {'ids': $scope.arrSlideDetailId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrSlideDetailId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_slide_detail_fail);
                    })
            }
        });
    };

    // Show / hidden slide
    $scope.showAndHiddenSlide = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_change_status_slide, function(result) {
            if(result){
                $http.post('slide/api/showAndHiddenSlide', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrSlideDetailId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.get_slide_detail_update_false);
                    })
            }
        });
    };

    // Get update Detail Properties
    $scope.getUpdateDetailProperties = function(){
        $http.post('slide/api/getSlideDetailPropertiesBySlideDetailId', {'id':$routeParams.idDetail})
            .success(function(data){
                $scope.properties = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_slide_detail_properties_false);
            });
    };

    // Update detail properties
    $scope.updateDetailProperties = function(){
        $http.post('slide/api/updateSlideDetailProperties', {'id':$routeParams.idDetail,'ids':$routeParams.id , 'valueProperty':$scope.valueProperty})
            .success(function(data){
                //$scope.getUpdateDetailProperties();
                $.each(data, function (key, value) {
                    toastr[key](value);
                });
            })
            .error(function(){
                toastr['error']($scope.lang.update_slide_detail_properties_fail);
            });
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrSlideDetailId = [];
        $('#slide tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrSlideDetailId.push(id);
        });
        console.log($scope.arrSlideDetailId);
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#slide tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrSlideDetailId = [];
        });
        console.log($scope.arrSlideDetailId);
    };

    // Clear form add slide
    $scope.reset = function () {
        $scope.newSlideDetail.name = "";
        $scope.newSlideDetail.image = "";
        $scope.newSlideDetail.video = "";
        $scope.newSlideDetail.text = "";
        $scope.newSlideDetail.class = "";
        $scope.newSlideDetail.status = "1";
        $('#text').focus();
    };

    $scope.getSlideId = function(){
        $scope.slideId = $routeParams.id;
    };

    $scope.tinymceOptions = {
        height: 200,
        plugins : ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage",
        image_advtab: true,
        file_browser_callback : fileBrowser
    };

    var idTextImage;
    $scope.chooseImage = function($event){
        idTextImage = $event.target.id;
        $('#idText').val('image'+idTextImage);
        window.open("../../backend/cms/media/management","_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=100, width=900, height=600");
    };

    $scope.applyImage = function(){
        $scope.valueProperty[idTextImage] = $('#image' + idTextImage).val();
    };


    //copy slide detail
    // Delete slide detail
    $scope.copy = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_copy, function(result) {
            if(result){
                $http.post('slide/api/copySlideDetail', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrSlideDetailId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.copy_slide_detail_fail);
                    })
            }
        });
    };
});


