angular.module("appNews").controller('configController', function ($scope, $routeParams, $compile, $http, DTOptionsBuilder, DTColumnBuilder) {

    // Init tools
    $scope.mainTools = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/main-tools.html';
    $scope.toolConfig = pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/news/partials/group/toolConfig.html';
    $scope.lang = JSON.parse(lang);
    $scope.arrGroupId = [];
    $scope.inputGroup = function(){
        return $('#inputGroup').val();
    };

    // Create paging for data table group
    $scope.dtOptions = ({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'news/api/getConfigNewsInGroup',
            data: {'id': $scope.inputGroup},
            type: 'POST',
            dataSrc: function (data) {
                $scope.arrGroupId = [];
                return data.data;
            }
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        order: [[1, "desc"]],
        pagingType: "full_numbers",
        createdRow: function (row) {
            $compile(angular.element(row).contents())($scope);
        },
        rowCallback: function(row, data){
            $('td', row).on('click', function(event) {
                var index = $scope.arrGroupId.indexOf(data.idGroup);
                if (index == -1){
                    $scope.arrGroupId.push(data.idGroup);
                    $scope.$apply();
                    $(row).addClass('colSelected');
                } else {
                    $scope.arrGroupId.splice(index,1);
                    $scope.$apply();
                    $(row).removeClass('colSelected');
                }
                console.log($scope.arrGroupId);
            });
        },
        fnDrawCallback: function(){
            Metronic.init();
        },
        reloadData: function () {
            this.reload = true;
            return this;
        }
    });

    // Custom data table news group
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle($scope.lang.id).withClass('colId'),
        DTColumnBuilder.newColumn('title').withTitle($scope.lang.title),
        DTColumnBuilder.newColumn(null).withTitle($scope.lang.tools).notSortable()
            .renderWith(function (data, type, full, meta) {
                return '<button class="btn btn-xs btn-danger" ng-click="delete(' + data.idGroup + ')" id="delete_' + data.idGroup + '">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
            .withClass('colStatus')
    ];

    // Get config news in group
    $scope.viewNewInGroup = function(){
        $scope.dtOptions.reloadData();
        $scope.arrGroupId = [];
    };

    // Add config news in group
    $scope.add = function(){
        $http.post('news/api/addConfigNewsInGroup', $scope.config)
            .success(function(data){
                $.each(data, function (key, value) {
                    toastr[key](value);
                });
                $scope.dtOptions.reloadData();
                $scope.arrGroupId = [];
            })
            .error(function(){
                toastr['error']($scope.lang.get_news_in_group_fail);
            });
        $scope.arrGroupId = [];
    };

    // Get update news group
    $scope.getAllNews = function(){
        $http.post('news/api/getAllNews')
            .success(function(data){
                $scope.news = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_new_fail);
            });
    };

    // Delete news group
    $scope.delete = function (id) {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                $http.post('news/api/deleteConfigNewsInGroup', {'id': id})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrGroupId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_news_group_fail);
                    })
            }
        });
    };

    // Delete multi news group
    $scope.deleteMultiConfigNewsInGroup = function () {
        bootbox.confirm($scope.lang.are_you_sure_want_to_delete, function(result) {
            if(result){
                console.log($scope.arrGroupId);
                $http.post('news/api/deleteMultiConfigNewsInGroup', {'ids': $scope.arrGroupId})
                    .success(function (data) {
                        $.each(data, function (key, value) {
                            toastr[key](value);
                        });
                        $scope.dtOptions.reloadData();
                        $scope.arrGroupId = [];
                    })
                    .error(function () {
                        toastr['error']($scope.lang.delete_news_group_fail);
                    })
            }
        });
    };

    // Select all
    $scope.selectedAll = function(){
        $scope.arrGroupId = [];
        $('#groupNews tbody').find('tr').each(function(index, element){
            $(element).addClass('colSelected');
            var trs = $(element).children();
            var id = parseInt($(trs[0]).text());
            $scope.arrGroupId.push(id);
        })
        console.log($scope.arrGroupId);
    };

    // Deselect all
    $scope.deselectedAll = function(){
        $('#groupNews tbody').find('tr').each(function(index, element){
            $(element).removeClass('colSelected');
            $scope.arrGroupId = [];
        });
        console.log($scope.arrGroupId);
    };

    // Clear form add slide
    $scope.reset = function () {
        $scope.newGroup.name = "";
        $scope.newGroup.status = "1";
        $('#text').focus();
    };

    $scope.getNewsGroup = function(){
        $http.post('news/api/getAllNewsGroup')
            .success(function(data){
                $scope.group = data;
            })
            .error(function(){
                toastr['error']($scope.lang.get_news_in_group_fail);
            });
    };

});
