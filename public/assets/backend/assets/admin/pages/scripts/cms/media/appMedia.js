var appMedia = angular.module("appMedia", ['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr','datatables']);

appMedia.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {
    $routeProvider
        .when('/',{
            controller: 'mediaController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/media/partials/index.html'
        })
        .when('/config',{
            controller: 'mediaController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/media/partials/config.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
