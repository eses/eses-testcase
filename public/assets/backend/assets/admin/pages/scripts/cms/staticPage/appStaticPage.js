var appStaticPage = angular.module("appStaticPage", ['rt.select2','ngRoute','ngAnimate','ui.tinymce','chieffancypants.loadingBar','blockUI','toastr','datatables','frapontillo.bootstrap-switch']);

appStaticPage.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'staticPageController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/staticPage/partials/index.html'
        })
        .when('/create',{
            controller: 'staticPageController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/staticPage/partials/create.html'
        })
        .when('/update/:id',{
            controller: 'staticPageController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/staticPage/partials/update.html'
        })
        .when('/config',{
            controller: 'staticPageController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/cms/staticPage/partials/config.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
