var UIAlertDialogApi = function () {

    var handleDialogs = function() {

        $('#demo_1').click(function(){
                bootbox.alert("Hello world!");    
            });
            //end #demo_1

            $('#demo_2').click(function(){
                bootbox.alert("Hello world!", function() {
                    alert("Hello world callback");
                });  
            });
            //end #demo_2
        
            $('#demo_3').click(function(){
                bootbox.confirm("Are you sure?", function(result) {
                   alert("Confirm result: "+result);
                }); 
            });
            //end #demo_3

            $('#demo_4').click(function(){
                bootbox.prompt("What is your name?", function(result) {
                    if (result === null) {
                        alert("Prompt dismissed");
                    } else {
                        alert("Hi <b>"+result+"</b>");
                    }
                });
            });
            //end #demo_6

            $('#demo_5').click(function(){
                var divHtml = document.createElement('div');
                $(divHtml).append($("<iframe />").attr('src','http://localhost/bitbucket_eses/public/preview/news-magazine'))
                //var dialogView = $('#dialog-preview');

                bootbox.dialog({
                    message: divHtml,
                    title: "Custom title",
                    buttons: {
                      success: {
                        label: "Success!",
                        className: "green",
                        callback: function() {
                          alert("great success");
                        }
                      },
                      danger: {
                        label: "Danger!",
                        className: "red",
                        callback: function() {
                          alert("uh oh, look out!");
                        }
                      },
                      main: {
                        label: "Click ME!",
                        className: "blue",
                        callback: function() {
                            $(divHtml).remove();
                        }
                      }
                    }
                });
            });

        $('#btn-preview').click(function(){
            var divHtml = document.createElement('div');
            $(divHtml).append($("<iframe onload='onIFrameLoader()' />").attr('src','http://localhost/bitbucket_eses/public/preview/news-magazine'))
            //var dialogView = $('#dialog-preview');
            var iframeHeight = $(document).height()*0.6;
            var iframeWidth = $(document).width()*0.8;
            $(divHtml).find('iframe').height(iframeHeight);
            $(divHtml).find('iframe').width(iframeWidth);

            bootbox.dialog({
               // width: 'auto', // overcomes width:'auto' and maxWidth bug
                //maxWidth: 1000,
                //height:1000,
                message: divHtml,
                title: "Preview Page",
                buttons: {

                    main: {
                        label: "Close!",
                        className: "blue",
                        callback: function() {
                            $(divHtml).remove();
                        }
                    }
                }
            });
        });
            //end #demo_7
        $('#show-dialog').click(function(){
            bootbox.dialog({
                message: "Do you want fill your data into template",
                title: "Fill Data Function",
                buttons: {
                    yes: {
                        label: "Yes!",
                        className: "green",
                        callback: function() {
                            alert("Yes option is selected");
                        }
                    },
                    no: {
                        label: "No!",
                        className: "red",
                        callback: function() {
                            alert("uh oh, look out!");
                        }
                    }

                }
            });
        });

    }

    var handleAlerts = function() {
        
        $('#alert_show').click(function(){

            Metronic.alert({
                container: $('#alert_container').val(), // alerts parent container(by default placed after the page breadcrumbs)
                place: $('#alert_place').val(), // append or prepent in container 
                type: $('#alert_type').val(),  // alert's type
                message: $('#alert_message').val(),  // alert's message
                close: $('#alert_close').is(":checked"), // make alert closable
                reset: $('#alert_reset').is(":checked"), // close all previouse alerts first
                focus: $('#alert_focus').is(":checked"), // auto scroll to the alert after shown
                closeInSeconds: $('#alert_close_in_seconds').val(), // auto close after defined seconds
                icon: $('#alert_icon').val() // put icon before the message
            });

        });

    }

    return {

        //main function to initiate the module
        init: function () {
            handleDialogs();
            handleAlerts();
        }
    };

}();

function onIFrameLoader(){
    var img = "imgurl";
    var title = "this is test title";
    var shortContent = "this is short content";
    var url= "this test url";

    var holder = $('iframe').contents().find('.news-holder');
    var listNews = $(holder).find('news-item').clone();
    $(holder).empty();
    $(listNews).each(function(){
        var item = $(this).clone();
        $(item).find('.news-item-image').prop('src', img);
        $(item).find('.news-item-url').prop('href', url);
        $(item).find('.news-item-title').text(title);
        $(item).find('.news-item-short-content').text(shortContent);
        $(holder).append(item);
        return;
    });

    $(holder).append(listNews);


}