var Post = function () {
    var createURL = function () {
        $('#title').keyup(function () {
            $('#url').val($('#title').val().replace(/ /g, '-'));
        });
    }

    var initSummerNote = function(){
        $(document).ready(function(){
            $('#post').summernote({height: 300});
        })
    }

    return {
        init: function () {
            createURL();
            initSummerNote();
        }
    }
}();