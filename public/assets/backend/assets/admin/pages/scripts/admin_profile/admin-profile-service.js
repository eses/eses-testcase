var AdminProfile = angular.module('AdminProfile',['ngRoute','ngAnimate','chieffancypants.loadingBar','blockUI','toastr']);
AdminProfile.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .otherwise({
            controller: 'AdminProfileController',
            templateUrl: pathPublic + 'assets/backend/assets/admin/pages/scripts/admin_profile/partial/profile.html'
        });
}]);


AdminProfile.factory('AdminProfileDAO', function($http){
    return {
        get : function(username){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/api/admin/profile/getUser',
                headers:{ "Content-Type" : 'application/x-www-form-urlencoded' },
                data: $.param(username)
            });
        },

        update : function(user){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/api/admin/profile/updateUser',
                headers:{ "Content-Type" : 'application/json' },
                data: user
            });
        },

        submitAvatar : function(img){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/api/admin/profile/updateAvatar',
                headers:{ "Content-Type" : 'application/json' },
                data: img
            });
        },
        changePassword : function(user){
            return $http({
                method: 'POST',
                url: pathUrl + 'backend/api/admin/profile/updatePassword',
                headers:{ "Content-Type" : 'application/json' },
                data: user
            });
        }

    }
});