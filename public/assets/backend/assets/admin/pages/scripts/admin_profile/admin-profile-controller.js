/**
 * Created by Nadare on 4/17/15.
 */
angular.module('AdminProfile').controller('AdminProfileController', function($scope, $compile, $routeParams, $http, AdminProfileDAO){

    $scope.user = {};
    $scope.user.username = username;
    $scope.lang = JSON.parse(lang);
    $scope.isLoad = false;
    if(!$scope.isLoad){

        $('#cur-avatar-holder').attr('src',$('#hide-avatar-value').val());

        AdminProfileDAO.get($scope.user)
            .success(function(result){
                var data = result['data'];
                console.log('userprofile');
                console.log(data);

                $.each(data, function(i, val){
                    if(val['value_column']){
                        var id = '#' + val['name_column'];
                        if( !(typeof($(id).val()) === 'undefined')){
                            $scope.user[val['name_column']] = val['value_column'];
                            $(id).val(val['value_column']);
                        }
                    }
                });
            })
            .error(function(data){
                toastr['error']('Cannot get profile, username=' + $scope.username);
            });
        $scope.isLoad=true;
    }
    $scope.saveProfile = function(){
        console.log($scope.user);
        AdminProfileDAO.update($scope.user)
            .success(function(result){
                if( result['error'] !== 1){
                    toastr['success']('Update user profile successfully!');
                }else{
                    toastr['error']('Update user profile failed!');
                }
            })
            .error(function(data){
                toastr['error']('System error when updating profile!');
            });
    }

    $scope.submitAvatar = function(){
        $scope.user.avatar = $($('#avatar-holder').children()[0]).attr('src');

        AdminProfileDAO.submitAvatar($scope.user)
            .success(function(result){
                if( result['data'] === true){
                    toastr['success']('Update user avatar successfully!');
                    location.reload();
                }else{
                    toastr['error']('Update user avatar failed!');
                }
            })
            .error(function(data){
                toastr['error']('System error when updating avatar!');
            });
    }
    $scope.changePassword = function(){
        if($scope.user.newPassword.localeCompare($scope.user.retypePassword) !== 0){
            toastr['error']('New password comfirm does not match');
            return;
        }
        AdminProfileDAO.changePassword($scope.user)
            .success(function(result){
                if( result['result'] === true){
                    toastr['success'](result['message']);
                    location.reload();
                }else{
                    toastr['error'](result['message']);
                }
            })
            .error(function(result){
                toastr['error']('System error when updating password!');
            });
    }


});
