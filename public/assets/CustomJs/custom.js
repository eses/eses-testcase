var FillDataTemplate = function(){
    var buildMenu = function(data){
        this.menuData = data;
        this.parentMenu = $('#parent-menu').clone();
        $('#parent-menu').remove();
        this.mem_parentMenu = $('#mem-parent-menu').clone();
        $('#mem-parent-menu').remove();
        this.subMenu1 = $('#sub-menu-1').clone();
        $('#sub-menu-1').remove();
        this.subMenu2 = $('#sub-menu-2').clone();
        $('#sub-menu-2').remove();
        this.mem_subMenu1 = $('#mem-sub-menu-1').clone();
        $('#mem-sub-menu-1').remove();
        this.mem_subMenu2 = $('#mem-sub-menu-2').clone();
        $('#mem-sub-menu-2').remove();

        var htmlParentMenu = this.parentMenu;
        for(var pIdx = 0, length = this.menuData.length; pIdx < length; pIdx++){
            var mem_dataMenu = this.menuData[pIdx];
            if(mem_dataMenu['parent_id'] == 0){
                var htmlMemParentMenu = this.mem_parentMenu.clone();
                htmlMemParentMenu.children().prop('href','test link');
                htmlMemParentMenu.children().text(mem_dataMenu['name']);

                /**
                 * find sub menu
                 */
                var htmlSubMenu1 = this.subMenu1.clone();
                for(var sIdx1 = 0, length1 = this.menuData.length; sIdx1 < length1; sIdx1++ ){
                    var mem_dataSubMenu1 = this.menuData[sIdx1];
                    if(mem_dataSubMenu1['parent_id'] === mem_dataMenu['id']){
                        var htmlMemSubMenu1 = this.mem_subMenu1.clone();
                        htmlMemSubMenu1.children().prop('href', 'test link');
                        htmlMemSubMenu1.children().text(mem_dataSubMenu1['name']);
                        htmlSubMenu1.append(htmlMemSubMenu1);
                        /**
                         * find sub menu 2
                         */
                        var htmlSubMenu2 = this.subMenu2.clone();
                        for(var sIdx2 = 0, length2 = this.menuData.length; sIdx2 < length2; sIdx2++ ){
                            var mem_dataSubMenu2 = this.menuData[sIdx2];
                            if(mem_dataSubMenu2['parent_id'] === mem_dataSubMenu1['id']){
                                var htmlMemSubMenu2 = this.mem_subMenu2.clone();
                                htmlMemSubMenu2.children().prop('href', 'test link');
                                htmlMemSubMenu2.children().text(mem_dataSubMenu2['name']);
                                htmlSubMenu2.append(htmlMemSubMenu2);
                            }

                        }
                        htmlMemSubMenu1.append(htmlSubMenu2);
                    }
                }
                htmlMemParentMenu.append(htmlSubMenu1);
                htmlParentMenu.append(htmlMemParentMenu);
            }
        }
        $('#main-menu').append(htmlParentMenu);

    }
    var buildMainSlider = function(data){
        this.sliderData = data;
        this.mainSlider = $('#main-slider');
        this.sliderMem = $('#mem-main-slider').clone();
        $('#mem-main-slider').remove();
        this.mainSlider.empty();
        //insert data
        for(var idx = 0, length = this.sliderData.length; idx < length; idx++){
            var memData = this.sliderData[idx];
            var slider = this.sliderMem.clone();
            var title = slider.find('#mem-main-slider-title');
            var subtitle = slider.find('#mem-main-slider-subtitle');
            var content = slider.find('#mem-main-slider-content');
            var holderImage = slider.find('#mem-main-slider-holder-source-image');
            var holderVideo = slider.find('#mem-main-slider-holder-source-video');
            var link = slider.find('#mem-main-slider-link');
            var preLink = $('#pre-link').val();
            if(title.length > 0){
                title.text(memData['title']);
            }
            if(subtitle.length > 0){
                subtitle.text(memData['subtitle']);
            }
            if(content.length > 0){
                content.text(memData['content']);
            }
            if(link.length > 0){
                link.prop('href',memData['link']);
                link.text(memData['link_text'])
            }
            if(holderImage.length > 0 && memData['type'] === 'image'){
                var urlImage = preLink + memData['source'];
                var temp = holderImage.find('#mem-main-slider-source-image-css');
                if(temp.length > 0){
                    holderImage.find('#mem-main-slider-source-image-css').css('background-image', 'url(' + urlImage + ')');
                }else if(holderImage.find('#mem-main-slider-source-image').length > 0){
                    holderImage.find('#mem-main-slider-source-image').prop('src',urlImage);
                }
            }else{
                holderImage.remove();
            }
            if(holderVideo.length > 0 && memData['type'] === 'video'){
                holderVideo.find('#mem-main-slider-source-video').prop('src',memData['source']);
            }else{
                holderVideo.remove();
            }

            this.mainSlider.append(slider);

        }
    }
    var buildFooter = function(data){
        this.footerData = data;
        this.htmlContact = $("#footer-contact");
        this.htmlAboutUs = $("#footer-about-us");
        this.htmlSubscribe = $("#footer-subscribe");
        this.htmlSocial = $("#footer-social");
        var listSocial = new Array();
        for(var i = 0, size = data.length; i < size; i++){
            var child = data[i];
            if(child['title'] == 'contact'){
                this.htmlContact.html(data[i]['content']);
            }
            if(child['title'] == 'aboutus'){
                this.htmlAboutUs.html(data[i]['content']);
            }
            if(child['title'] == 'subscribe'){
                this.htmlSubscribe.html(data[i]['content']);
            }
            if(child['title'] == 'social'){
                id = "#footer-social-" + child['content'];
                item = $(id);
                url = item.find(id + "-url");
                url.prop('href', child['url']);
                listSocial.push(item);
            }
        }
        this.htmlSocial.empty();
        for(var i = 0, length = listSocial.length; i < length; i++){
            this.htmlSocial.append(listSocial[i]);
        }
}
    var buildProductPage = function(data){
        var arrProductHtmlItem = $(".product-item");
        var countHtmlItem = arrProductHtmlItem.length;
        var countDataItem = data.length;
        var size = countDataItem;
        if(countDataItem >= countHtmlItem){
            size = countHtmlItem;
        }
        var idx = 0;
        $('.product-item').each(function(){
            var itemHtml = $(this);
            var itemData = data[idx];
            itemHtml.find('.product-item-image').prop('src', itemData['image']);
            itemHtml.find('.product-item-url').prop('href', itemData['url']);
            itemHtml.find('.product-item-price').text(itemData['price']);
            itemHtml.find('.product-item-quantity').text(itemData['quantity']);
            itemHtml.find('.product-item-name').text(itemData['name']);
            itemHtml.find('.product-item-description').text(itemData['description']);
            if(idx >= size){
                return;
            }
        });
    }
    var buildSideMenu = function(data, idMenu){
        var numLevel = data.length;
        var mapMenuItems = new Array();
        for(var idx = 0 ; idx < numLevel ; idx++){
            var menuItem = $('#' + idMenu + '-menu-item-level-' + idx).clone();
            var arrMenuHtmlItems = new Array();

            var currentLvlMenuData = data['level ' + idx];
            for(var idxMenuItem = 0, lengthItem = currentLvlMenuData.length; idxMenuItem < lengthItem ; idxMenuItem++){
                var cloneMenuItem = createMenuItem(menuItem.clone(),currentLvlMenuData[idxMenuItem]);
                var idMenuItem = currentLvlMenuData[idxMenuItem]['id'];
                arrMenuHtmlItems[idMenuItem] = cloneMenuItem;
            }
            mapMenuItems[idx] = arrMenuHtmlItems;
        }
        // combine menu
        for(var level = numLevel - 1 ; level > 0 ; level--){
            var preLevel = level - 1;
            var curMenuData = data[level];

            var preMenuHtmlItems  = mapMenuItems[preLevel];
            var curMenuHtmlItems  = mapMenuItems[level];

            //get add child menu to its parent menu
            var idItem;
            var idxMenuItem = 0;
            for (idItem in curMenuHtmlItems) {
                if (arrayHasOwnIndex(curMenuHtmlItems, idItem)) {
                    var parentId = curMenuData[idxMenuItem]['parent'];
                    //add child to parent
                    preMenuHtmlItems[parentId].find('#menu-holder').append(curMenuHtmlItems[idItem]);
                }
                idxMenuItem++;
            }
        }
        var rootHolder = $('#'+ idMenu + '-root');
        rootHolder.empty();
        var idItem;
        var arrMenuLevel1 = mapMenuItems[0];
        for(idItem in arrMenuLevel1){
            if (arrayHasOwnIndex(arrMenuLevel1, idItem)) {
                rootHolder.append(arrMenuLevel1[idItem]);
            }
        }


    }
    var buildNewsPage = function(data){
        var arrItems = $(".news-item");
        var countHtmlItems = $(arrItems).length;
        var countDataItem = data.length;
        var size = countHtmlItems;
        if(countDataItem < countHtmlItems){
            size = countDataItem;
        }
        var idx = 0;
        $(".news-item").each(function(){
            var itemHtml = $(this);
            var itemData = data[idx];
            var image = itemHtml.find('.news-item-image');
            if(image.length >0){
                image.prop('src', itemData['image']);
            }

            var title = itemHtml.find('.news-item-title');
            if(title.length >0){
                title.text(itemData['title']);
            }
            var shortContent = itemHtml.find('.news-item-short-content');
            if(shortContent.length >0){
                shortContent.empty();
                shortContent.append(itemData['short_content']);
            }

            var fullContent = itemHtml.find('.news-item-full-content');
            if(fullContent.length >0){
                fullContent.empty();
                fullContent.append(itemData['full_content']);
            }
            idx++;
            if(idx >= size){
                return;
            }
        });

    }
    function createMenuItem(menuItem,dataItem){
        var cloneMenuItem = menuItem;
        cloneMenuItem.text(dataItem['name'])
        return cloneMenuItem;

    }
    function arrayHasOwnIndex(array, prop) {
        return array.hasOwnProperty(prop) && /^0$|^[1-9]\d*$/.test(prop) && prop <= 4294967294; // 2^32 - 2
    }

    return{
        init: function(mapData, typePage){

            buildMenu(mapData['mainMenu']);
            buildFooter(mapData['footer']);
            buildMainSlider(mapData['slider']);

            if(typePage === 'product'){
                buildProductPage(mapData['product']);
            }
            if(typePage === 'news'){
                buildNewsPage(mapData['news']);
            }
        }
    }
}


