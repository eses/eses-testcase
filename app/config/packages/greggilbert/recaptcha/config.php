<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6Lc8EgETAAAAAF-8emBvqlwrdwOTv7--60hsOK_X',
	'private_key'	=> '6Lc8EgETAAAAAB1hG5_QAo77qpo8gfSwr41-esFr',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	
);