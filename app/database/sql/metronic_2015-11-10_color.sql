CREATE DATABASE  IF NOT EXISTS `metronic` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `metronic`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: metronic
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `site_template_class_group`
--

DROP TABLE IF EXISTS `site_template_class_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_template_class_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_id` bigint(20) DEFAULT NULL,
  `constructor_id` bigint(20) DEFAULT NULL,
  `group_key` varchar(200) DEFAULT NULL,
  `group_name` varchar(200) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `default` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_template_class_group`
--

LOCK TABLES `site_template_class_group` WRITE;
/*!40000 ALTER TABLE `site_template_class_group` DISABLE KEYS */;
INSERT INTO `site_template_class_group` VALUES (2,1,NULL,'color','Color',1,1,'2014-10-09 02:37:47','2014-10-09 02:37:47');
/*!40000 ALTER TABLE `site_template_class_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_template_constructor`
--

DROP TABLE IF EXISTS `site_template_constructor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_template_constructor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `constructor_block_id` bigint(20) NOT NULL,
  `file` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `default` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_template_constructor`
--

LOCK TABLES `site_template_constructor` WRITE;
/*!40000 ALTER TABLE `site_template_constructor` DISABLE KEYS */;
INSERT INTO `site_template_constructor` VALUES (1,1,'core-plugins','Core plugins',NULL,1,1,'2014-10-08 16:49:28','2014-10-15 03:05:32'),(2,2,'main-header','Main header',NULL,1,1,'2014-10-08 16:49:28','2014-10-15 03:05:32'),(3,3,'head','Head',NULL,1,1,'2014-10-08 16:49:28','2014-10-15 03:05:32'),(4,4,'footer','Footer',NULL,1,1,'2014-10-08 16:49:28','2014-10-15 03:05:32'),(5,5,'header','Header',NULL,1,1,'2014-10-08 16:49:28','2014-10-15 03:05:32'),(23,6,'metronic','metronic',NULL,1,1,'2015-10-24 17:35:35','2015-10-24 18:12:34');
/*!40000 ALTER TABLE `site_template_constructor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_template_config`
--

DROP TABLE IF EXISTS `site_template_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_template_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `master_page` bigint(20) NOT NULL,
  `constructor_block` bigint(20) NOT NULL,
  `default` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_template_config`
--

LOCK TABLES `site_template_config` WRITE;
/*!40000 ALTER TABLE `site_template_config` DISABLE KEYS */;
INSERT INTO `site_template_config` VALUES (1,1,1,1,1,'2014-10-08 17:20:23','2014-10-13 07:35:25'),(2,1,2,1,1,'2014-10-08 17:20:23','2014-10-08 17:20:23'),(3,1,3,1,1,'2015-11-09 18:30:43','2015-11-09 18:30:43'),(4,1,4,1,1,'2015-11-09 18:30:43','2015-11-09 18:30:43'),(5,1,5,1,1,'2015-11-09 18:30:43','2015-11-09 18:30:43'),(6,1,6,1,1,'2014-10-08 17:20:23','2014-10-08 17:20:23');
/*!40000 ALTER TABLE `site_template_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_template_constructor_block`
--

DROP TABLE IF EXISTS `site_template_constructor_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_template_constructor_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_id` bigint(20) NOT NULL,
  `block` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_show` tinyint(4) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_template_constructor_block`
--

LOCK TABLES `site_template_constructor_block` WRITE;
/*!40000 ALTER TABLE `site_template_constructor_block` DISABLE KEYS */;
INSERT INTO `site_template_constructor_block` VALUES (1,1,'core-plugins','Core plugins',1,'2014-10-08 17:13:12',0,'2014-10-08 17:13:12'),(2,1,'main-header','Main header',1,'2014-10-08 17:13:12',0,'2014-10-08 17:13:12'),(3,1,'head','Head',1,'2014-10-08 17:13:12',0,'2014-10-08 17:13:12'),(4,1,'footer','Footer',1,'2014-10-08 17:13:12',0,'2014-10-08 17:13:12'),(5,1,'header','Header',1,'2014-10-08 17:13:12',0,'2014-10-08 17:13:12'),(6,1,'style','Style',1,'2014-10-08 17:13:12',1,'2014-10-08 17:13:12');
/*!40000 ALTER TABLE `site_template_constructor_block` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-10  1:56:08
