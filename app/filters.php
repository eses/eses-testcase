<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    $subscriber = new UserEvent;
    Event::subscribe($subscriber);

	// Init theme
    /*if(!Session::has('Theme')){
        $theme = Array();
        $theme['Color'] = 'color-orange';
        $theme['Menu'] = 'h3';
        Session::put('Theme',$theme);
    }
    $them = Session::get('Theme');
    View::share('theme', $them);*/
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('AuthAdmin', function(){
    $role = Session::get(UserModel::USER_INFO);
    Log::info('authen admin:');
    if(is_null($role) || $role->role != UserModel::USER_ADMIN_ROLE){
        return View::make('backend.login.index')->with('captcha', false);
    }
});

Route::filter('AuthUser', function(){
    $role = Session::get(UserModel::USER_INFO);
    Log::info('authen user');
    Log::info($role);
    if(is_null($role) || $role->role != UserModel::USER_USER_ROLE && $role->role != UserModel::USER_ADMIN_ROLE){
        Log::error("ko vao dc view nay");
        return View::make('frontend.login.index')->with('captcha', false);
    }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('configTemplate', function(){
    if(!Session::has('Template')){
        return Redirect::to('/backend/management_site_template_management');
    }
});
