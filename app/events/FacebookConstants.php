<?php
/**
 * Created by PhpStorm.
 * User: Nadare
 * Date: 3/7/15
 * Time: 10:25 AM
 */

class FacebookConstants {

    const USERNAME = 'username';
    const EMAIL = 'email';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const PHONE = 'phone';
} 