<?php
/**
 * Created by PhpStorm.
 * User: Nadare
 * Date: 3/7/15
 * Time: 9:08 AM
 */

class UserEvent {

    public function saveToSession($username){

    }

    public function onLoginFacebook($event){
        // dd($event);
        $first_name = $event['first_name'];
        $last_name = $event['last_name'];
        $username = $event['email'];

        $company = 'unknown';
        $job = 'unknown';
        $gender = 'unknown';
        try{
            $gender = $event['gender'];
        }catch(Exception $ex){
            $gender = 'unknown';
        }
        if(sizeof($event['work']) > 0){
            try{
                $company = $event['work'][0]['employer']['name'];
            }catch(Exception $ex){
                $company = 'unknown';
            }
            try{
                $job = $event['work'][0]['position']['name'];
            }catch(Exception $ex){
                $job = 'unknown';
            }
        }

        //save user
        $rs = User::isAlreadyInfo($username, UserModel::USER_ID);
        if(empty($rs)){
            $email = 'unknown';
            $phone = 'unknown';
            $username = $event[FacebookConstants::USERNAME];
            try{
                $email = $event[FacebookConstants::EMAIL];
            }catch(Exception $ex){
                $phone = $event[FacebookConstants::PHONE];
            }
            //save new user
            $data = array(UserModel::USER_ID =>  $username,
                UserModel::USER_EMAIL => $email,
                UserModel::USER_PHONE => $phone,
                UserModel::USER_FIRST_NAME => $first_name,
                UserModel::USER_LAST_NAME => $last_name,
                UserModel::USER_PASSWORD => "",
                UserModel::USER_ADDRESS => "",
                UserModel::USER_ROLE => UserModel::USER_USER_ROLE,
                UserModel::USER_STATUS => 1,
                UserModel::USER_GENDER => "male");
           // User::createUser($data);
            //User::initUserProfile($username, $first_name, $last_name);
        }

        //update user profile


        //save to session
        $user_info = new UserModel();
        $user_info->username = $username;
        $user_info->role  =  UserModel::USER_USER_ROLE;
        Session::put(UserModel::USER_INFO, $user_info);
        Session::put('email', $email);
        Session::put(UserModel::USER_ID, $username);
        Session::put(UserModel::USER_ROLE, UserModel::USER_USER_ROLE);
        Session::put(UserModel::IS_LOGIN, true);
        Session::put(UserModel::IS_LOGIN_FB, true);
        Session::put(UserModel::IS_EDIT, false);
    }

    public function onLoginGoogle($event){
//        dd($event);
        //   dd($event);

        $user = new UserModel();
        $user->first_name = $event['family_name'];
        $user->last_name = $event['given_name'];
        $user->username = $event['name'];
        $user->email = $event['email'];
        //$user->save();

      //  User::initUserProfile($user->username,  $user->first_name, $user->last_name);

        //update user profile


        //save to session
        Session::put(UserModel::USER_ID, $user->username);
        $user_info = new UserModel();
        $user_info->username = $user->username;
        $user_info->role  =  UserModel::USER_USER_ROLE;
        Session::put(UserModel::USER_INFO, $user_info);
        Session::put('email', $user->email);
        Session::put(UserModel::USER_ROLE, UserModel::USER_USER_ROLE);
        Session::put(UserModel::IS_LOGIN, true);
        Session::put(UserModel::IS_LOGIN_GG, true);
        Session::put(UserModel::IS_EDIT, false);
    }


    public function removeToSession($username){
        Session::forget(UserModel::USER_ID);
        Session::put(UserModel::IS_LOGIN, false);

    }

    public function onLogout(){
        Session::put(UserModel::IS_LOGIN, false);
        Session::forget(UserModel::USER_ID);
        Session::forget(UserModel::USER_INFO);
        Session::forget(UserModel::IS_EDIT);
        Auth::logout();
    }

    public function subscribe($events){
        $events->listen('user.loginFacebook', 'UserEvent@onLoginFacebook');
        $events->listen('user.loginGoogle', 'UserEvent@onLoginGoogle');
        $events->listen('user.logout', 'UserEvent@onLogout');
    }
}
