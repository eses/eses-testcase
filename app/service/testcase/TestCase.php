<?php
/**
 * Created by PhpStorm.
 * User: nadare
 * Date: 7/13/2015
 * Time: 11:31 PM
 */

namespace service\testcase;

use DB;
use Illuminate\Support\Facades\Config;
use Log;
use TestLink;
use TestCaseModel;
use Exception;
use Hash;
use Paginator;
use Str;
use Auth;

class TestCase
{
    const TEST_LINK = "testlink";
    const TEST_CASE = "testcase";

    public function getTestLinkBy($keyword, $start, $length){
        Log::info("Start get TestLink");
        $data =
            TestLink::where('id','LIKE','%'.$keyword.'%')
            ->orWhere('name','LIKE','%'.$keyword.'%')
            ->orWhere('link','LIKE','%'.$keyword.'%')
            ->orWhere('config','LIKE','%'.$keyword.'%')
            ->skip($start)->take($length)->get()->toArray();
        Log::info("Paginate data");
        $paginationData = Paginator::make($data, TestLink::count(), $length)->toArray();
        Log::info("End get TestLink");
        return $paginationData;
    }

    public function getTestCaseBy($id, $start, $length){
        Log::info("Start get TestCase");
        $data =
            TestCaseModel::where('link_ref_id',$id)
                ->skip($start)->take($length)->get()->toArray();
        Log::info("Paginate data");
        $paginationData = Paginator::make($data, TestCaseModel::count(), $length)->toArray();
        Log::info("End get TestCase");
        return $paginationData;
    }


    public function issetField($arr, $key, $name){
        if(!isset($arr[$key])){
            throw new Exception("Please provide ".$name);
        }
    }
    public function insertTestLink($input){
        //input check
        $this->issetField($input, "name","Name");
        $this->issetField($input, "link","Link");
        $this->issetField($input, "config","Configuration");

        //check testlink
        if(DB::table(TestCase::TEST_LINK)
            ->where('name',$input["name"])->first() != null){
            throw new Exception("TestLink=".$input["name"]." is already in database");
        }

        //insert db
        $test_link = new TestLink();
        $test_link->name = $input["name"];
        $test_link->link = $input["link"];
        $test_link->config = $input["config"];
        if($test_link->save()){
            return true;
        }
        return false;
    }
    public function updateTestLink($input){
        $this->issetField($input, "id","ID");
        $this->issetField($input, "name","Name");
        $this->issetField($input, "link","Link");
        $this->issetField($input, "config","Configuration");
        //update
        DB::table(TestCase::TEST_LINK)
            ->where('id', $input["id"])
            ->update(array("name" => $input["name"],
                            "link"=> $input["link"],
                            "config"=> $input["config"]));
    }

    public function getTestLink($id){
        return DB::table(TestCase::TEST_LINK)
            ->where('id', $id)
            ->first();
    }

    public function deleteTestLink($input){
        $this->issetField($input, "id","ID");

        //delete db
        //delete test link
        DB::table(TestCase::TEST_LINK)
            ->where('id', $input['id'])
            ->delete();
        //delete test case
        DB::table(TestCase::TEST_CASE)
            ->where('link_ref_id',$input['id'])
            ->delete();

    }

    public function getTestCaseByTestLink($id){
        Log::info("Get Testcase by test link");
        return DB::table(TestCase::TEST_CASE)
            ->where('link_ref_id', $id)
            ->get();
    }

    public function getTestCase($id){
        return DB::table(TestCase::TEST_CASE)
            ->where('id', $id)
            ->first();
    }
    public function insertTestCase($input){
        //input check
        $this->issetField($input, "link_ref_id","TestLinK");
        $this->issetField($input, "testcase_id","Test Case ID");
        $this->issetField($input, "action_no","Action No");
        $this->issetField($input, "name","Name");
        $this->issetField($input, "value","Value");

        //insert db
        $test_case = new TestCaseModel();
        $test_case->link_ref_id = $input["link_ref_id"];
        $test_case->testcase_id = $input["testcase_id"];
        $test_case->action_no = $input["action_no"];
        $test_case->name = $input["name"];
        $test_case->value = $input["value"];
        if($test_case->save()){
            return true;
        }
        return false;
    }

    public function updateTestCase($input){
        $this->issetField($input, "id","ID");
        $this->issetField($input, "link_ref_id","TestLinK");
        $this->issetField($input, "testcase_id","Test Case ID");
        $this->issetField($input, "action_no","Action No");
        $this->issetField($input, "name","Name");
        $this->issetField($input, "value","Value");

        //update
        DB::table(TestCase::TEST_CASE)
            ->where('id', $input["id"])
            ->update(array(
                "testcase_id"=> $input["testcase_id"],
                "action_no"=> $input["action_no"],
                "name"=> $input["name"],
                "value"=> $input["value"]));
    }

    public function deleteTestCase($input){
        $this->issetField($input, "id","ID");
        //delete test case
        DB::table(TestCase::TEST_CASE)
            ->where('id',$input['id'])
            ->delete();
    }

}