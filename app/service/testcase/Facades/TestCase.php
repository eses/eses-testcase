<?php
/**
 * Created by PhpStorm.
 * User: nadare
 * Date: 7/14/2015
 * Time: 12:00 AM
 */

namespace service\testcase\Facades;
use Illuminate\Support\Facades\Facade;

class TestCase extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'TestCase';
    }
} 