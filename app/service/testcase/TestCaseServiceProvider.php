<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 10:51 PM
 */

namespace service\testcase;

use Illuminate\Support\ServiceProvider;

class TestCaseServiceProvider extends ServiceProvider  {
    public function register()
    {
        $this->app->bind('TestCase', function()
        {
            return new TestCase;
        });
    }
}