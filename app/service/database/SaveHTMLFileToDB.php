<?php
/**
 * Created by PhpStorm.
 * User: Nadare
 * Date: 12/2/14
 * Time: 11:30 PM
 */

namespace service\database;
use Log;
use RecursiveDirectoryIterator;
use TemplatePageContent;
use SplFileInfo;
use Monolog;

class SaveHTMLFileToDB {
    const DIR_SLIDE_SHOW_PICTURE = "/images/image-slide-show/";
    const DIR_SLIDE_SHOW_AVATAR = "/images/avatar/";

    private function saveTemplateProduct($idTemplate, $rootDir){
        $dirSlide = $rootDir . self::DIR_SLIDE_SHOW_PICTURE;
        LOG::info("bat dau get list image slide");
        $slideImages = $this->getListFile($dirSlide);
        LOG::info("ket thuc get list image slide");

        LOG::info('bat dau update property');
        $templateProductPropertyDAO = new \TemplateProductPropertyModel();
        $templateProductPropertyDAO->upsert($idTemplate, $slideImages);
        LOG::info('ket thuc update property');

        $urlAvatar = $this->getAvatarUrl($rootDir);
        $templateProductDAO = new \TemplateProductModel();
        $templateProductDAO->upsert($idTemplate, "description example","SALE", $urlAvatar);
    }
    private function getListFile($dir){
        $iterator = new \RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir)
        );
        $files = Array();
        foreach($iterator as $fullFileName =>$fileSPLObject){
            if($fileSPLObject->isFile()){
                $info = new SplFileInfo($fullFileName);
                array_push($files, $info->getPathname());
            }
        }
        LOG::info($files);
       // dd('aaa');
        return $files;
    }

    public function getAvatarUrl($rootDir){
        $dirAvatar = $rootDir . self::DIR_SLIDE_SHOW_AVATAR;
        return $this->getListFile($dirAvatar)[0];
    }

    public function save($mediaModel){
        LOG::info('bat dau parse');
        LOG::info($mediaModel);
        //$idTemplate = $this->getFileNameFromPathFile($mediaModel->media_name);
        $idTemplate = $mediaModel['media_name'];
        LOG::info("ten template=" . $idTemplate);

        $filePath = $mediaModel['media_url'];
        LOG::info('url='.$filePath);
        //$rootDir = "../public".substr($filePath,0, (strlen($filePath) - (strlen($filePath) - strpos($filePath, ".")))).'/';
        $rootDir = "../public/upload/".$idTemplate."/";
        LOG::info('root='.$rootDir);
        //luu hinh anh cua template
        //$this->saveTemplateProduct($idTemplate, $rootDir);

        $pathHtmlFile= $rootDir.'index.html';

        $iterator = new \RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootDir)
        );
        $files = Array();
        $search = Array();
        $lstDir = Array();
        $replacement = Array();
        foreach($iterator as $fullFileName =>$fileSPLObject){
            if($fileSPLObject->isFile()){

                $info = new SplFileInfo($fullFileName);
                array_push($files, $info->getExtension());
            }else{
                $path = $fileSPLObject->getPath();
                //Log::info($path);
                $strTemp = substr($path, strpos($path, $idTemplate) + strlen($idTemplate) + 1);
                if($strTemp != ''){
                    array_push($lstDir, $strTemp);
                }
            }
        }
        // sort array

        usort($lstDir,function($a, $b)
        {

            if(strlen($a) < strlen($b)) return 1;
            if(strlen($a) > strlen($b)) return -1;
            return 0;
        });
       // Log::info(str_replace("layout/images/", "layoutimages"))
        //Log::info($replacement);
        $lstDir = array_unique($lstDir);
       //end sort array

        $files = array_unique($files);

        foreach($files as $file ){
            $memTarget = '.' . $file;
            $memReplacement = $memTarget . "')}}" ;
            array_push($search, $memTarget);
            array_push($replacement, $memReplacement);
        }

        //START TO GET LIST DIRECTORIES
        foreach($lstDir as $dir){
            $dir = str_replace("\\", "/", $dir);
            $memTarget1 =  'src="'. $dir . "/";
            $memTarget2 =  'href="'. $dir . "/";

            $memReplacement1 = 'src="'."{{Asset('" . substr($rootDir, 10) . "/". $dir . "/";
            $memReplacement2 = 'href="'."{{Asset('" . substr($rootDir, 10) . "/". $dir . "/";
           // Log::info($memReplacement);
            array_push($search, $memTarget1);
            array_push($search, $memTarget2);
            array_push($replacement, $memReplacement1);
            array_push($replacement, $memReplacement2);
        }
        //END GET LIST DIRECTORIES

        $fileContent = file_get_contents($pathHtmlFile);
        LOG::info("search");
        LOG::info($search);
        LOG::info("replace");
        LOG::info($replacement);
        $result = str_replace($search, $replacement,  $fileContent);

        //id template is the name of upload zip file

        $pageObject = TemplatePageContent::where('idTemplate','=',$idTemplate)->first();
        if(!is_null($pageObject)){
            Log::info("update template=".$idTemplate);
            $pageObject->content = $result;
            $pageObject->idTemplate = $idTemplate;//substr($mediaModel->media_name,0, (strlen($mediaModel->media_name) - (strlen($mediaModel->media_name) - strrpos($mediaModel->media_name, "."))));;
        }else{

            Log::info('insert template='.$idTemplate);
            $pageObject = new TemplatePageContent();
            $pageObject->page='index';
           // $pageObject->id = $mediaModel->id;
            $pageObject->content = $result;
            $pageObject->idTemplate = $idTemplate;//substr($mediaModel->media_name,0, (strlen($mediaModel->media_name) - (strlen($mediaModel->media_name) - strrpos($mediaModel->media_name, "."))));

        }
        $pageObject->save();
        return $idTemplate;
    }

    static function myCmp($a, $b)
    {

        if($a < $b) return 1;
        if($a > $b) return -1;
        return 0;
    }
    private function getFileNameFromPathFile($pathFile){
        $length = strlen($pathFile);
        $dotPos = $length - strpos($pathFile, ".");

        return substr($pathFile,0, ($length - $dotPos));
    }
} 