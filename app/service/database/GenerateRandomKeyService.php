<?php


namespace service\database;

class GenerateRandomKeyService{
    public static function GenerateKey($length = 16) {
       // dd('enter');
        $key = '';

        for($i = 0; $i < $length; $i ++) {
            $key .= chr(mt_rand(33, 126));
        }
        return $key;
    }
}