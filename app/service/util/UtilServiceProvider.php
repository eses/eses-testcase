<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 10:51 PM
 */

namespace service\util;

use Illuminate\Support\ServiceProvider;

class UtilServiceProvider extends ServiceProvider  {
    public function register()
    {
        $this->app->bind('Util', function()
        {
            return new Util;
        });
    }
}