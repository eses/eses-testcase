<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 21/1/2015
 * Time: 4:10 PM
 */

namespace service\util\Facades;

use Illuminate\Support\Facades\Facade;

class Util extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'Util';
    }
}