<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 9:56 PM
 */

namespace service\upload;

use Illuminate\Support\Facades\DB;
use NewsMediaModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload
{

    /**
     * Constructor file upload
     */
    function __construct()
    {
    }

    /**
     * Validation file upload
     * @param UploadedFile $file
     * @return bool
     */
    function FileValidationAdmin(UploadedFile $file)
    {
        return true;
    }

    /**
     * Upload file using for role admin
     * @param $author
     * @param UploadedFile $file
     * @return NewsMediaModel|null
     */
    function adminUpload($author, UploadedFile $file)
    {
        $pathUpload = '/upload/media/';
        if(!$this->FileValidationAdmin($file)){
            return null;
        }
        DB::beginTransaction();
        $mediaModel = new NewsMediaModel();
        $mediaModel->media_author = $author;
        $mediaModel->media_mime_type = $file->getClientMimeType();
        $mediaModel->media_size = $file->getClientSize();
        $mediaModel->media_extension = $file->getClientOriginalExtension();
        if ($mediaModel->save()) {
            $destinationPath = public_path() . $pathUpload;
            $id = $mediaModel->id;
            $filename = $id . '_' . $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $mediaModel->media_name = $filename;
            $mediaModel->media_url = $pathUpload . $filename;
            if ($mediaModel->save()) {
                DB::commit();
                return $mediaModel;
            }
        }
        DB::rollback();
        return null;
    }

    /**
     * Validation file upload for role designer
     * @param UploadedFile $file
     * @return bool
     */
    function FileValidationDesigner(UploadedFile $file)
    {
        return true;
    }

    /**
     * Upload file using for role designer
     * @param $author
     * @param UploadedFile $file
     * @return NewsMediaModel|null
     */
    function designerUpload($author, UploadedFile $file)
    {
        $pathUpload = '/upload/'.md5($author).'/';
        Log::info('path upload:');
        Log::info($pathUpload);
        if(!$this->FileValidationDesigner($file)){
            Log::info('Designer Upload: Failed to validate');
            return null;
        }
        DB::beginTransaction();
        $mediaModel = new NewsMediaModel();
        $mediaModel->media_author = $author;
        $mediaModel->media_mime_type = $file->getClientMimeType();
        $mediaModel->media_size = $file->getClientSize();
        $mediaModel->media_extension = $file->getClientOriginalExtension();
        Log::info('Media info:');
        Log::info($mediaModel);
        if ($mediaModel->save()) {
            $destinationPath = public_path() . $pathUpload;
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $mediaModel->media_name = $filename;
            $mediaModel->media_url = $pathUpload . $filename;
            if ($mediaModel->save()) {
                DB::commit();
                $realPath = $destinationPath . $filename;
                $zip = new \ZipArchive();
                if($zip->open($realPath) === TRUE){
                    $zip->extractTo($destinationPath);
                    $zip->close();
                }
                return $mediaModel;
            }
        }
        DB::rollback();
        return null;
    }

    public static function uploadBase64Image($username , $src)
    {
        $pathUpload = '/upload/'.md5($username).'/';
        Log::info('path upload:');
        Log::info($pathUpload);
        $type='';
        list($type, $src) = explode(';', $src);
        Log::info('type='.$type);

        list(, $data)      = explode(',', $src);
        $data = base64_decode($data);

        file_put_contents($pathUpload.'.png', $data);

    }

} 