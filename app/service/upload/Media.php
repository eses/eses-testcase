<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 22/1/2015
 * Time: 2:01 PM
 */

namespace service\upload;

use NewsMediaModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Media {

    /**
     * Media construct
     */
    function __construct(){
    }

    /**
     * Delete media file and database
     * @param $id
     * @return bool
     */
    function deleteMedia($id){
        $media = NewsMediaModel::find($id);
        if(!isset($media)){
            return false;
        }
        $pathFile = public_path().$media->media_url;
        DB::beginTransaction();
        if(File::delete($pathFile) || !File::exists($pathFile)){
            if($media->delete()){
                DB::commit();
                return true;
            }
        } else {
            DB::rollback();
        }
        return false;
    }

    /**
     * Delete multi media
     * @param array $ids
     * @return bool
     */
    function deleteMultiMedia($ids = array()){
        $result = true;
        try{
            DB::beginTransaction();
            foreach($ids as $id){
                $media = NewsMediaModel::find($id);
                if(!isset($media)){
                    $result = false;
                }
                $pathFile = public_path().$media->media_url;
                if(File::delete($pathFile) || !File::exists($pathFile)){
                    if(!$media->delete()){
                        $result = false;
                    }
                } else {
                    $result = false;
                }
            }
            if($result){
                DB::commit();
                return true;
            }
        } catch(\PDOException $ex){
            Log::error($ex);
        }
        DB::rollback();
        return false;
    }
}