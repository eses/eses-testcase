<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/13/14
 * Time: 9:51 PM
 */

namespace service\upload;


use Illuminate\Support\ServiceProvider;

class UploadServiceProvider extends ServiceProvider {
    public function register()
    {
        $this->app->bind('Upload', function() {
            return new Upload;
        });

        $this->app->bind('Media', function(){
            return new Media;
        });
    }
} 