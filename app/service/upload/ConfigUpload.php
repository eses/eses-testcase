<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 9:56 PM
 */

namespace service\upload;

use NewsConfigModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ConfigUpload
{
    /**
     * Constructor file upload
     */
    function __construct()
    {
    }

    /**
     * Insert config Upload Media
     * @param string $role
     * @param array $configs
     * @return bool
     */
    function createConfig($role, $configs = array())
    {
        DB::beginTransaction();
        try {
            foreach ($configs as $key => $value) {
                NewsConfigModel::insert(
                    array(
                        'name' => $role,
                        'config_key' => $key,
                        'config_value' => $value
                    )
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
        return true;
    }

    /**
     * Update config upload media
     * @param $role
     * @param $configKey
     * @param $configValue
     * @return bool
     */
    function updateConfig($role, $configKey, $configValue){
        $configModel = NewsConfigModel::where('name',$role)->where('config_key', $configKey)->firstOrFail();
        if(!$configModel){
            return false;
        }
        $configModel->config_value = $configValue;
        if($configModel->save()){
            return true;
        }
        return false;
    }

    function getConfig($role, $configKey){
        $configModel = NewsConfigModel::where('name',$role)->where('config_key', $configKey)->firstOrFail();
        if(!$configModel){
            return null;
        }
        return $configModel;
    }

} 