<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 6/1/2015
 * Time: 4:10 PM
 */

namespace service\cms;

use DB;
use Log;

class StaticPage {

    /**
     * Constructor News class
     */
    function __constructor(){
    }

    /**
     * Delete multi news
     * @param array $ids
     * @return bool
     */
    function deleteMultiStaticPage($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                DB::table('news_posts')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * Get static page by link
     * @param $link
     * @return array|static[]
     */
    function getStaticPageByLink($link){
        $staticPages = DB::table('news_posts as p')
            ->where('p.post_link',$link)
            ->where('p.post_status','1')
            ->take(1)
            ->get(array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content as content',
                'p.created_at as created'
            ));
        foreach($staticPages as $staticPage){
            return $staticPage;
        }
        return array();
    }

    /**
     * Get content in static page by link
     * @param $link
     * @return string
     */
    function getContentByLink($link){
        $page = $this->getStaticPageByLink($link);
        if(isset($page->content)){
            return $page->content;
        }
        return '';
    }
}