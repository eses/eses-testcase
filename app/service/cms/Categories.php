<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 6/1/2015
 * Time: 4:10 PM
 */

namespace service\cms;

use DB;
use Log;

class Categories {

    /**
     * Constructor Categories class
     */
    function __constructor(){
    }

    /**
     * Delete multi categories
     * @param array $ids
     * @return bool
     */
    function deleteMultiCategories($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                $parent =DB::table('news_categories')->where('parent_id',$id)->first();
                if(isset($parent->id)){
                    return false;
                }
            }
            foreach($ids as $id){
                DB::table('news_categories')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * Get categories by alias
     * @param $alias
     * @return mixed
     */
    function getCategoriesByAlias($alias){
        return DB::table('news_categories')->where('categories_alias',$alias)->first();
    }

    /**
     * Get categories by id
     * @param $id
     * @return mixed|static
     */
    function getById($id){
        return DB::table('news_categories')->where('id',$id)->first();
    }

    /**
     * Generate categories config
     * @param $parent
     * @param $level
     * @return string
     */
    function generateCategoriesConfig($parent = 0, $level = 1){
        $resultQuery = DB::select("SELECT c.id, c.categories_sort, c.categories_name, c.categories_alias, temp_categories.count_parent FROM news_categories c  LEFT OUTER JOIN (SELECT parent_id, COUNT(*) count_parent FROM news_categories GROUP BY parent_id) temp_categories ON c.id = temp_categories.parent_id WHERE c.parent_id=? ORDER BY c.categories_sort ASC", array($parent));
        if(isset($result)){
            $result .= '<ul class="dd-list">';
        } else {
            $result = '<ul class="dd-list">';
        }
        foreach ($resultQuery as $row) {
            if ($row->count_parent > 0) {
                $result .= '<li class="dd-item dd3-item" data-id="'.$row->id.'"><div class="dd-handle dd3-handle"></div><div class="dd3-content">';
                $result .= $row->categories_name;
                $result .= '</div>';
                $result .= $this->generateCategoriesConfig($row->id, $level + 1);
                $result .= '</li>';
            } else {
                $result .= '<li class="dd-item dd3-item" data-id="'.$row->id.'"><div class="dd-handle dd3-handle"></div><div class="dd3-content">';
                $result .= $row->categories_name;
                $result .= '</div></li>';
            }
        }
        $result .= "</ul>";
        return $result;
    }

    /**
     * Save configuration categories sort and parent id
     * @param array $arr
     * @return bool
     */
    function saveConfigCategories($arr = array()){
        $result = false;
        try{
            DB::beginTransaction();
            foreach($arr as $key=>$value){
                $categories = \NewsCategoriesModel::find($value['id']);
                $categories->parent_id = $value['parent'];
                $categories->categories_sort = $key+1;
                $categories->save();
            }
            DB::commit();
            $result = true;
        } catch (\PDOException $ex){
            Log::error($ex);
            DB::rollback();
        }
        return $result;
    }


    /**
     * Check categories is have children menu
     * @param $id
     * @return number children
     */
    function isHaveChildMenu($id){
        $resultQuery = DB::select("SELECT count(*) as sl FROM news_categories a inner join news_categories b on (a.id=b.parent_id) and a.id=?", array($id));
        foreach ($resultQuery as $row) {
           return $row->sl;
        }
        return 0;
    }

     /**
     * Get list categories active
     * @return categories list active
     */
    function getListCategoriesActive(){
        return DB::table('news_categories')->where('categories_status','1')->orderBy('categories_sort','asc')->get();
    }
}