<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 9:56 PM
 */

namespace service\cms;

use NewsPostModel;

class Cms {

    /**
     * Constructor cms
     */
    function __construct(){
    }

    /**
     * Resolve path assets in template
     * @param $template
     * @param $nameTheme
     * @return mixed
     */
    function resolvePathTemplate($template, $nameTheme){
        return str_replace('assets',asset('/').'themes/'.$nameTheme.'/assets', $template);
    }

    /**
     * Get page home head
     * Return NewsPostModel with post_type = news_page_home
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    function getPageHomeHead(){
        return NewsPostModel::where('post_type','news_page_home_head')->firstOrFail();
    }

    /**
     * Get page home body
     * Return NewsPostModel with post_type = news_page_home
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    function getPageHomeBody(){
        return NewsPostModel::where('post_type','news_page_home_body')->firstOrFail();
    }

    /**
     * Get NewsPostModel with post type
     * Return NewsPostModel[]
     * @param $postType
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getPostWithPostType($postType){
        return NewsPostModel::where('post_type', $postType)->get();
    }

    /**
     * Get Field in post with post type
     * Return NewsPostModel[]
     * @param $postType
     * @param array $arrayField
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getFieldInPostWithPostType($postType, $arrayField = array()){
        return NewsPostModel::where('post_type', $postType)->get($arrayField);
    }

    function getAllPartial(){
        $partials = NewsPostModel::where('post_type','partial')->get();
        $arrPartial = array();
        foreach($partials as $partial){
            $arrPartial[$partial->post_link] = $this->resolvePathTemplate($partial->post_content, 'onepage');
        }
        return $arrPartial;
    }
} 