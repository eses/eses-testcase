<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 6/1/2015
 * Time: 4:10 PM
 */

namespace service\cms;

use DB;
use Log;

class News {

    /**
     * Constructor News class
     */
    function __constructor(){
    }

    /**
     * Delete multi news
     * @param array $ids
     * @return bool
     */
    function deleteMultiNews($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                DB::table('news_posts')->where('post_type','<>','static_page')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * Delete multi news group
     * @param array $ids
     * @return bool
     */
    function deleteMultiNewsGroup($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                DB::table('news_group')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * Delete multi config news in group
     * @param array $ids
     * @return bool
     */
    function deleteMultiConfigNewsInGroup($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                DB::table('config_news_group')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * Get news in group config
     * @param $idGroup
     * @return array|static[]
     */
    function getNewsInGroupConfig($idGroup, $take){
        $topNews = DB::table('config_news_group as g')
            ->join('news_posts as p','p.id','=','g.news_id')
            ->join('news_group as nf','nf.id','=','g.group_id')
            ->where('g.group_id',$idGroup)
            ->where('p.post_status','1')
            ->where('nf.status','1')
            ->take($take)
            ->orderBy('g.id','desc')
            ->get(array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
        return $topNews;
    }

    /**
     * Get news by categories alias
     * @param $aliasCategories
     * @param $take
     * @return array|static[]
     */
    function getNewsByCategoriesAlias($aliasCategories, $take){
        $topNews = DB::table('news_posts as p')
            ->join('news_categories as c','c.id','=','p.categories_id')
            ->where('c.categories_alias',$aliasCategories)
            ->where('p.post_status','1')
            ->where('c.categories_status','1')
            ->orderBy('p.id','desc')
            ->take($take)
            ->get(array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
        return $topNews;
    }

    /**
     * Get news by categories alias
     * @param $aliasCategories
     * @param $take
     * @return array|static[]
     */
    function getNewsNotInCategories($take){
        $topNews = DB::table('news_posts as p')
            ->where('p.post_status','1')
            ->where('p.post_type','news_post')
            ->whereNull('p.categories_id')
            ->orderBy('p.created_at','desc')
            ->take($take)
            ->get(array(
                'p.id as id',
                'p.comment_status as status',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
        return $topNews;
    }

    /**
     * Get news by categories alias
     * @param $aliasCategories
     * @param $take
     * @return array|static[]
     */
    function getNewsByKeyword($keyword, $take){
        $topNews = DB::table('news_posts as p')
            ->leftJoin('news_categories as c','c.id','=','p.categories_id')
            ->where('p.post_status','1')
            ->where('p.post_type','news_post')
            ->where('p.post_title', 'LIKE', '%'.$keyword.'%')
            ->orWhere(function($query) use($keyword) {
                $query->where('p.post_content_short', 'LIKE', '%' . $keyword . '%')
                    ->orWhere(function ($query) use ($keyword) {
                        $query->where('c.categories_name', 'LIKE', '%' . $keyword . '%');
                    });
            })
            ->orderBy('p.created_at','desc')
            ->take($take)
            ->get(array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
      //  dd($topNews);
        return $topNews;
    }

    /**
     * Get news by categories return paginator
     * @param $aliasCategories
     * @param $take
     * @return \Illuminate\Pagination\Paginator
     */
    function getNewsByCategoriesAliasWithPaging($aliasCategories, $take){
        $topNews = DB::table('news_posts as p')
            ->join('news_categories as c','c.id','=','p.categories_id')
            ->where('c.categories_alias',$aliasCategories)
            ->where('p.post_status','1')
            ->where('c.categories_status','1')
            ->orderBy('p.created_at','desc')
            ->paginate($take, array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
        return $topNews;
    }

    /**
 * Get news by link
 * @param $link
 * @return array|static[]
 */
    function getNewsByLink($link){
        $topNews = DB::table('news_posts as p')
            ->where('p.post_link',$link)
            ->where('p.post_status','1')
            ->take(1)
            ->get(array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
        return $topNews;
    }

    /**
     * Get categories by link news
     * @param $link
     * @return mixed|static
     */
    function getCategoriesByLink($link){
       // dd($link);
        $categories = DB::table('news_posts as p')
            ->join('news_categories as c','p.categories_id','=','c.id')
            ->where('p.post_link',$link)
            ->where('p.post_status','1')
            ->first(array(
                'c.*'
            ));
      //  dd($categories);
        return $categories;
    }

    /**
     * Get single news by link
     * @param $link
     * @return array|static[]
     */
    function getSingleNewsByLink($link){
        $topNews = DB::table('news_posts as p')
            ->where('p.post_link',$link)
            ->where('p.post_status','1')
            ->take(1)
            ->first(array(
                'p.id as id',
                'p.post_link as link',
                'p.post_author as author',
                'p.post_title as title',
                'p.post_content_short as short',
                'p.post_content as content',
                'p.comment_count as commentCount',
                'p.created_at as created',
                'p.comment_status as status'
            ));
        return $topNews;
    }

    /**
     * Get max sort number
     * @return mixed
     */
    function getMaxSortNumber(){
        $max = DB::table('news_posts')
            ->max('post_sort');
        return $max + 10;
    }

    /**
     * Move data row with sort number
     * @param $source
     * @param $destination
     * @return bool
     */
    function moveDataRowWithSortNumber($source, $destination){
        $result = false;
        DB::connection()->beginTransaction();
        if($source <= $destination){
            $arrChange = DB::table('news_posts')
                ->where('post_sort','<=',$destination)
                ->where('post_sort','>=',$source)
                ->get(array('id','post_sort'));
        } else {
            $arrChange = DB::table('news_posts')
                ->where('post_sort','<=',$source)
                ->where('post_sort','>=',$destination)
                ->get(array('id','post_sort'));
        }
        if(isset($arrChange) && !empty($arrChange)){
            $length = sizeof($arrChange);
            $temp = $arrChange[$length-1]->post_sort;
            for($i=$length-1; $i>0; $i--){
                $arrChange[$i]->post_sort = $arrChange[$i-1]->post_sort;
            }
            $arrChange[0]->post_sort = $temp;
            foreach($arrChange as $item){
                $result = DB::table('news_posts')
                    ->where('id',$item->id)
                    ->update(array('post_sort'=>$item->post_sort));
            }

        }
        if($result){
            DB::connection()->commit();
            return true;
        }
        DB::connection()->rollBack();
        return false;
    }

    /**
     * Get src image in news
     * @param $news
     * @return mixed
     */
    function getSourceImageInNews($news){
        $doc = new \DOMDocument();
        $doc->loadHTML($news);
        $xpath = new \DOMXPath($doc);
        $src = $xpath->evaluate("string(//img/@src)"); # "/images/image.jpg"
        if(!isset($src) || empty($src)){
            return asset('upload/media/image_null.jpg');
        }
        return $src;
    }

    /**
     * Get multi image in news
     * @param $news
     * @return array
     */
    function getMultiSourceImageInNews($news){
        $result = array();
        $doc = new \DOMDocument();
        $doc->loadHTML($news);
        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $tag) {
            $result[] = $tag->getAttribute('src');
        }
        return $result;
    }
    
    /**
     * Get group name by id
     * @param $id
     * @return \Illuminate\Database\Query\static[]
     */
    function getGroupNameById($id){
    	return DB::table('news_group')->where('id',$id)->where('status','1')->first(array('group_name'));
    }

}