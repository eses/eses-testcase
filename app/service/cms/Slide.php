<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 6/1/2015
 * Time: 4:10 PM
 */

namespace service\cms;

use DB;
use Log;
use ConfigSlideDetailModel;
use OAuth\Common\Exception\Exception;

class Slide {

    /**
     * Constructor Slide class
     */
    function __constructor(){
    }

    /**
     * Delete multi slide
     * @param array $ids
     * @return bool
     */
    function deleteMultiSlide($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                DB::table('config_slide')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * Delete multi slide detail
     * @param array $ids
     * @return bool
     */
    function deleteMultiSlideDetail($ids = array()){
        try{
            DB::connection()->beginTransaction();
            foreach($ids as $id){
                DB::table('config_slide_detail')->delete($id);
            }
            DB::connection()->commit();
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }

    /**
     * General slide
     * @param $slideId
     * @return mixed
     */
    function generalSlide($slideId){
        $result = '';
        $slideDetails = \ConfigSlideDetailModel::where('slide_id',$slideId)->where('status',\Status::ACTIVE)->get();
        foreach($slideDetails as $value){
            $slideXML = simplexml_load_string($value->value_column);
            $slideProperties = \ConfigSlideDetailPropertiesModel::where('id_ref',$value->id)->get();
            foreach($slideProperties as $property){
                $text = trim($property->value_column);
                $str = '$slideXML'.$property->key_column.'="'.$text.'";';
                eval($str);
            }
            $result .= $slideXML->asXML();
        }
        //dd($result);
        return $result;
    }

    /**
     * Write slide default
     * @param $slideId
     * @return int
     */
    function writeSlideDefault($slideId){
        try{
            $template = \SiteTemplate::whereRaw('status = ? and `default` = ?', array(\Status::ACTIVE, \IsDefault::YES))->first();
            $path = app_path('/views/layouts/frontend/'.$template->template_code.'/slider/slider.blade.php');
            $slide = $this->generalSlide($slideId);
           // dd($this->generalSlide($slideId));
            $slide = str_replace('<?xml version="1.0" encoding="UTF-8"?>','',$slide);
            $config_slide = \ConfigSlideModel::where('id',$slideId)->where('slide_status',\Status::ACTIVE)->first();
            $content = $config_slide->slide_prefix.$slide.$config_slide->slide_suffix;
            file_put_contents($path,$content);
            return true;
        } catch (Exception $e){
            Log::error($e->getMessage());
            return false;
        }
    }
    /**
     * Copy slide
     * @param $slideId
     * @return boolean
     */
    function copySlide($id){
        try{
            $slide = ConfigSlideDetailModel::find($id);
            //  dd($slide);
            Log::info($slide);
            $nID = DB::table('config_slide_detail')->insertGetId(
                array('name_column'=> $slide->name_column.'_copy',
                        'value_column' => $slide->value_column,
                        'status' => $slide->status,
                        'slide_id' => $slide->slide_id));
            Log::info($nID);
            DB::statement('INSERT INTO config_slide_detail_properties (key_column,value_column,type_column,id_ref) SELECT key_column,value_column,type_column,' .$nID.' FROM config_slide_detail_properties where id_ref = '. $id);
            return true;
        }catch(Exception $ex){
            Log::error($ex->getMessage());
            return false;
        }
    }

    /**
     * Active main slide
     * @param array $ids
     * @return bool
     */
    function activeMainSlide($ids = array()){
        try{
            DB::connection()->beginTransaction();
            $slides = \ConfigSlideModel::whereRaw('slide_status>=?',array(\Status::INACTIVE))->get();
            $temp = 0;
            foreach($ids as $id){
                $temp=$id;
                break;
            }

            foreach ($slides as $slide)
            {
                if($temp!=$slide->id) {
                    DB::table('config_slide')
                        ->where('id', $slide->id)
                        ->update(['slide_status' => 0]);
                }else{
                    DB::table('config_slide')
                        ->where('id', $slide->id)
                        ->update(['slide_status' =>1]);
                }
            }
            DB::connection()->commit();
            $this->writeSlideDefault($temp);
            return true;
        } catch(\PDOException $ex){
            Log::error($ex);
            DB::connection()->rollback();
        }
        return false;
    }
}