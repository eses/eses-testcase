<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 10:51 PM
 */

namespace service\cms;

use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider  {
    public function register()
    {
        $this->app->bind('Cms', function(){
            return new Cms;
        });

        $this->app->bind('Categories', function(){
            return new Categories;
        });

        $this->app->bind('News', function(){
            return new News;
        });

        $this->app->bind('StaticPage', function(){
            return new StaticPage;
        });

        $this->app->bind('Slide', function(){
            return new Slide;
        });
    }
}