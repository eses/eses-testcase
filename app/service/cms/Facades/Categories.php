<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 6/1/2015
 * Time: 4:15 PM
 */

namespace service\cms\Facades;

use Illuminate\Support\Facades\Facade;

class Categories extends Facade{
    protected static function getFacadeAccessor(){
        return 'Categories';
    }
}