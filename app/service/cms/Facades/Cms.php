<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 10/16/14
 * Time: 2:31 PM
 */

namespace service\cms\Facades;

use Illuminate\Support\Facades\Facade;

class Cms extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'Cms';
    }
} 