<?php
/**
 * Created by PhpStorm.
 * User: nadare
 * Date: 7/14/2015
 * Time: 12:00 AM
 */

namespace service\user\Facades;
use Illuminate\Support\Facades\Facade;

class User extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'User';
    }
} 