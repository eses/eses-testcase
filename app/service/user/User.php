<?php
/**
 * Created by PhpStorm.
 * User: nadare
 * Date: 7/13/2015
 * Time: 11:31 PM
 */

namespace service\user;

use DB;
use Illuminate\Support\Facades\Config;
use Log;
use UserModel;
use Exception;
use Hash;
use Paginator;
use Str;
use Auth;

class User
{

        public function issetField($arr, $key, $name){
            if(!isset($arr[$key])){
                throw new Exception("Please provide ".$name);
            }
        }

        public function register($input)
        {

            Log::info("Register User");
            $this->issetField($input, "username","Username");
            $this->issetField($input, "email","Email");
            $this->issetField($input, "first_name","First Name");
            $this->issetField($input, "last_name","Last Name");

            $this->issetField($input, "password","password");
            if (strcmp($input['password'], $input['confirmPassword']) != 0) {
                throw new Exception("Confirm password is not match");
            }

            DB::beginTransaction();
            try {

                $userModel = new UserModel();

                $userModel['username'] = Str::lower($input['username']);
                $userModel['email'] = Str::lower($input['email']);

                $rs = $this->isAlreadyInfo('username', $userModel['username']);
                if (!empty($rs)) {
                    Log::info($rs);
                    throw new Exception($rs);
                }
                $rs = $this->isAlreadyInfo('email', $userModel['email']);

                if (!empty($rs)){
                    Log::info($rs);
                    throw new Exception($rs);
                }
                try {
                    $userModel['first_name'] = $input['first_name'];
                    $userModel['last_name'] = $input['last_name'];
                }catch(\OAuth\Common\Exception\Exception $ex1){

                }
                $userModel['status'] = UserModel::USER_STATUS_DEACTIVE;
                $userModel['role'] = UserModel::USER_USER_ROLE;
                $userModel['password'] = Hash::make($input['password']);
                $userModel->save();


                DB::commit();
                return true;
            } catch (Exception $e) {
                DB::rollback();
                return "Adding User Failed: " . $e->getMessage();
            }
        }
    public function adminAddUser($input)
    {

        Log::info("Register User");

        if (strcmp($input['password'], $input['confirmPassword']) != 0) {
            throw new Exception("Confirm password is not match");
        }

        DB::beginTransaction();
        try {

            $userModel = new UserModel();
            if(!array_key_exists('username',$input) || !array_key_exists('email',$input)
                || !array_key_exists('password',$input)){
                throw new Exception("Please Fill In Form");
            }

            $userModel['username'] = Str::lower($input['username']);
            $userModel['email'] = Str::lower($input['email']);

            $rs = $this->isAlreadyInfo('username', $userModel['username']);
            if (!empty($rs)) {
                Log::info($rs);
                throw new Exception($rs);
            }
            $rs = $this->isAlreadyInfo('email', $userModel['email']);

            if (!empty($rs)){
                Log::info($rs);
                throw new Exception($rs);
            }
            try {
                $userModel['first_name'] = array_key_exists('first_name', $input) ? $input['first_name'] : '';
                $userModel['last_name'] = array_key_exists('last_name', $input) ? $input['last_name'] : '';
            }catch(\OAuth\Common\Exception\Exception $ex1){

            }
            $userModel['status'] = $input['status'];
            $userModel['role'] = $input['role'];
            Log::info("password".$input['password']);
            $userModel['password'] = Hash::make($input['password']);
            $userModel->save();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            return "Adding User Failed: " . $e->getMessage();
        }
    }

    public function isAlreadyInfo($field, $data){
        $user = UserModel::where($field,"=", Str::lower($data))->get();
        Log::info("data email");
        Log::info($user);
        if(count($user)<=0){
            Log::info("username/Email Empty");
            return "";
        }else{
            Log::info("Your ".$field." is already in database");
            return "Your ".$field." is already in database";
        }
    }


    public static function getUserInfo($username){
        $userRole = UserModel::where(UserModel::USER_ID, '=', $username)->first();
        Log::info($userRole);
        return $userRole;
    }

    public function getUsers($start, $length){
        Log::info("Get User");
        $userData = UserModel::skip($start)->take($length)->get()->toArray();
        $paginationData = Paginator::make($userData, UserModel::count(), $length)->toArray();

        return $paginationData;
    }

    public function getUserBy($keyword, $start, $length){
        $acc = \AppSettingEnum::ESES_Account;
        $userData = UserModel::where('username','LIKE','%'.$keyword.'%')->where('username','<>',$acc)
            ->orWhere(function($query) use($keyword,$acc) {
                $query->where('first_name', 'LIKE', '%' . $keyword . '%')->where('username','<>',$acc)
                    ->orWhere(function ($query) use ($keyword,$acc) {
                        $query->where('last_name', 'LIKE', '%' . $keyword . '%')->where('username','<>',$acc);
                    });
            })
            ->skip($start)->take($length)->get()->toArray();
        foreach($userData as &$u){
            if($u['avatar'] == null){
                $u['avatar']='upload/avatars/default.jpg';
            }
        }
        $paginationData = Paginator::make($userData, UserModel::count(), $length)->toArray();

        return $paginationData;
    }

    public function updateEmail($username, $email){
        UserModel::where(UserModel::USER_ID, '=',Str::lower($username))->update(array('email'=>$email));
    }




    public function changePassword($user){
        Log::info('UserDAO changepassword');
        Log::info(Str::lower($user->username));

        $userInDb = UserModel::where(UserModel::USER_ID, '=',Str::lower($user->username))->first();

        if(is_null($userInDb)){
            Log::info('no user');
            return false;
        }
        if(Auth::attempt(array("username"=>$user->username, "password"=>$user->password))){
            $userInDb->password = Hash::make($user->newPassword);
            $userInDb->save();
            return true;
        }else{
            Log::info('password does not match');
            return false;
        }
    }

    public function getUserProfile($username){
        Log::info("username=".$username);
        $data = UserModel::where(UserModel::USER_ID, '=', Str::lower($username))->first();
        Log::info($data);
        $rs = array(
            array("name_column"=> "first_name", "value_column"=>$data->first_name),
            array("name_column"=> "last_name", "value_column"=>$data->last_name),
            array("name_column"=> "email", "value_column"=>$data->email),
            array("name_column"=> "gender", "value_column"=>$data->gender),
            array("name_column"=> "phone", "value_column"=>$data->phone),
            array("name_column"=> "address", "value_column"=>$data->address),
        );
        Log::info($rs);

        return $rs;
    }

    public function adminUpdateUser($user){
        try {
            UserModel::where('username', '=', $user->username)->update(
                array('status' => $user->status,
                    'role' => $user->role));
            if(array_key_exists('password',$user)){
                UserModel::where('username', '=', $user->username)->update(
                    array('password' => $user->password));
            }
            $data = array('error'=>0, 'data'=>'Update User Successfully!');

            return $data;
        }catch(Exception $ex){
            $data = array('error'=>1,'data'=>'Update User Failed:'.$ex->getMessage());
            return $data;
        }
    }




    public function updateUser($username, $name, $value){
        UserModel::where('username','Like',$username)
                    ->update([$name => $value]);
    }

    public function updateUserProfile($user_info){
        try{
            DB::beginTransaction();

            $ref_key = $user_info['username'];
            Log::info('in user property dao');
            Log::info($user_info);
            if(isset($user_info['email']) && !is_null($user_info['email'])){
                $this->updateUser($ref_key, 'email', $user_info['email']);
            }
            if(isset($user_info['gender']) && !is_null($user_info['gender'])){
                $this->updateUser($ref_key, 'gender', $user_info['gender']);
            }
            if(isset($user_info['first_name']) && !is_null($user_info['first_name'])){
                $this->updateUser($ref_key, 'first_name', $user_info['first_name']);
            }
            if(isset($user_info['last_name']) && !is_null($user_info['last_name'])){
                $this->updateUser($ref_key, 'last_name', $user_info['last_name']);
            }
            if(isset($user_info['phone']) && !is_null($user_info['phone'])){
                $this->updateUser($ref_key, 'phone', $user_info['phone']);
            }
            if(isset($user_info['address']) && !is_null($user_info['address'])){
                $this->updateUser($ref_key, 'address', $user_info['address']);
            }
            if(isset($user_info['password']) && !is_null($user_info['password'])){
                $this->updateUser($ref_key, 'password', $user_info['password']);
            }
            if(isset($user_info['role']) && !is_null($user_info['role'])){
                $this->updateUser($ref_key, 'role', $user_info['role']);
            }
            if(isset($user_info['status']) && !is_null($user_info['status'])){
                $this->updateUser($ref_key, 'status', $user_info['status']);
            }
            DB::commit();
            return true;
        }catch(Exception $ex){
            Log::error('UserProperty: Failed to update user profile, error='.$ex->getMessage(), $ex->getTrace());
            return false;
        }
    }
}