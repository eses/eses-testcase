<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 10:51 PM
 */

namespace service\user;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider  {
    public function register()
    {
        $this->app->bind('User', function()
        {
            return new User;
        });
    }
}