<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 10:51 PM
 */

namespace service\menu;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider  {
    public function register()
    {
        $this->app->bind('Menu', function()
        {
            return new Menu;
        });
    }
}