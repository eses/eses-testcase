<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 11/6/14
 * Time: 9:56 PM
 */

namespace service\menu;

use SiteMenuModel;
use SiteGroupMenuModel;

class Menu {

    /**
     * Constructor menu
     */
    function __construct(){
    }

    /**
     * Get all menu
     * @return $this
     */
    function getAllMenus(){
        return SiteMenuModel::where('active','1')->get();
    }

    /**
     * Get menus
     * @param $group
     * @return $this
     */
    function getMenus($group){
        return SiteMenuModel::where('group',$group)->where('active','1')->get();
    }

    /**
     * Get menu
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getMenu($id){
        return SiteMenuModel::where('id',$id)->first();
    }

    /**
     * Get menus root
     * @return $this
     */
    function getMenuRoots(){
        return SiteMenuModel::where('parent_id',0)->where('parent_id','0')->where('active','1')->get();
    }

    /**
     * Create menu
     * @param $group
     * @param $name
     * @param $url
     * @param int $parentId
     * @param int $sort
     * @param int $active
     * @return bool
     */
    function createMenu($group,$name,$url,$parentId = 0,$sort = 0,$active = 1){
        $menu = new SiteMenuModel();
        $menu->group = $group;
        $menu->name = $name;
        $menu->url = $url;
        $menu->parent_id = $parentId;
        $menu->sort = $sort;
        $menu->active = $active;
        if($menu->save()){
            return true;
        }
        return false;
    }

    /**
     * Get menu group
     * @return mixed
     */
    function getGroups(){
        return SiteGroupMenuModel::distinct()->get();
    }

    function getGroup($id){
        return SiteGroupMenuModel::where('id',$id)->first();
    }
} 