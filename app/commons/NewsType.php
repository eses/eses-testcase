<?php
/**
 * Created by PhpStorm.
 * User: phans_000
 * Date: 10/9/14
 * Time: 10:15 AM
 */

class NewsType {
    const NEWS_POST = 'news_post';
    const NEWS_PAGE = 'news_page';
    const STATIC_PAGE = 'static_page';
}