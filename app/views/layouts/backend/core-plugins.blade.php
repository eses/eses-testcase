<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{asset('assets/backend/assets/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('assets/backend/assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('assets/backend/assets/global/plugins/jquery-1.11.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/jquery-migrate-1.2.1.min.js')}}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{asset('assets/backend/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular/angular.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular/angular-route.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular/angular-resource.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular/angular-animate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular/loading-bar/loading-bar.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular-block-ui/angular-block-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular-toastr/angular-toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular-datatables/angular-datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular/angular-cookies.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/backend/assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/admin/pages/scripts/ui-toastr.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/backend/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/backend/assets/global/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('assets/backend/assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/angular-ui/angular-select2.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript" ></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init();    // init metronic core componets
        Layout.init();      // init layout
        QuickSidebar.init() // init quick sidebar
        UIToastr.init();    // init toastr message
        @if($errors->has())
            @foreach($errors->all() as $error)
                toastr['error']('{{$error}}');
            @endforeach
        @endif

        @if(isset($messages))
            @foreach($messages as $key => $msg)
                toastr['{{$key}}']('{{$msg}}');
            @endforeach
        @endif

        @if(!is_null($messages = Session::has('messages')?Session::get('messages'):null))
            @foreach($messages as $key => $msg)
                toastr['{{$key}}']('{{$msg}}');
            @endforeach
        @endif
    });
</script>
<script>
    var pathUrl = '{{asset("/")}}';
</script>