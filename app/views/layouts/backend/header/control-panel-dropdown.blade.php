<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
		data-close-others="true">
		<i class="fa fa-desktop font-green-meadow"></i><i class="fa fa-angle-down font-green-meadow"></i>
	</a>
	<ul class="dropdown-menu">		
		<li>
			<a href="{{url('/backend/users')}}">
				<i class="icon-user font-green-meadow"></i> Users
			</a>
		</li>
		<li class="divider"></li>
		<li>
			<a href="{{url('/backend/setting')}}">
				<i class="icon-wrench font-green-meadow"></i> Setting
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-copy font-green-meadow"></i> Page CMS
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-sitemap font-green-meadow"></i> Menu Management
			</a>
		</li>
	</ul>
</li>