
<li class="dropdown dropdown-user"><a href="#" class="dropdown-toggle"
	data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
		<img alt="" class="img-circle" style="height:30px;width:30px"
		src="{{asset(Session::get('avatar'))}}" />
		<span class="username"> {{Session::get('username')}} </span> <i class="fa fa-angle-down"></i>
</a>
	<ul class="dropdown-menu">
		<li><a href="{{URL::to('backend/admin/profile')}}"> <i class="icon-user"></i> {{Lang::get('messages.my_profile')}}
		</a></li>
		<li><a href="{{URL::to('admin/logout')}}"> <i class="icon-key"></i> {{Lang::get('messages.log_out')}}
		</a></li>
	</ul></li>