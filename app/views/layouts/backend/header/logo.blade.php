<div class="page-logo">
	<a href="{{url('backend/dashboard')}}"> <img
		src="{{asset('assets/backend/assets/admin/layout/img/logo.png')}}"
		alt="logo" class="logo-default" />
	</a>

	<div class="menu-toggler sidebar-toggler">
		<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
	</div>
</div>