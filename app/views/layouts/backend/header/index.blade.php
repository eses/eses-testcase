<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		@include('layouts.backend.header.logo')
		<!-- END LOGO -->
		<!-- BEGIN HEADER SEARCH BOX -->
	<!--	<form class="search-form search-form-expanded" action="#" method="GET">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Search..." name="query">
				<span class="input-group-btn">
				<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
				</span>
			</div>
		</form> -->
		<!-- END HEADER SEARCH BOX -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler"
			data-toggle="collapse" data-target=".navbar-collapse"> </a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				{{--@include('layouts.backend.header.notification-dropdown')--}}
				<!-- END NOTIFICATION DROPDOWN -->
				<!-- BEGIN INBOX DROPDOWN -->
				{{--@include('layouts.backend.header.inbox-dropdown')--}}
				<!-- END INBOX DROPDOWN -->
				<!-- BEGIN TODO DROPDOWN -->
				{{--@include('layouts.backend.header.todo-dropdown')--}}
				<!-- END TODO DROPDOWN -->
				<!-- BEGIN CONTROL PANEL DROPDOWN -->
				{{--@include('layouts.backend.header.control-panel-dropdown')--}}
				<!-- END CONTROL PANEL DROPDOWN -->
				<!-- BEGIN USER LOGIN DROPDOWN -->
				@include('layouts.backend.header.user-login-dropdown')
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!--<li class="dropdown dropdown-quick-sidebar-toggler"><a
					href="javascript:;" class="dropdown-toggle"> <i class="icon-logout"></i>
				</a></li> -->
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>