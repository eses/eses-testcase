<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<div class="page-sidebar navbar-collapse collapse">
<!-- BEGIN SIDEBAR MENU -->
<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">

<li class="{{ Request::is('backend') ? 'start active' : '' }}">
    <a href="{{url('backend/dashboard')}}">
        <i class="icon-home"></i>
        <span class="title">{{Lang::get('messages.dashboard')}}</span>
        <span class="selected"></span>
    </a>
</li>

    <li class="{{ Request::is('backend/cms*') ? 'start active' : '' }} {{Request::is('backend/cms')?'start active':''}}">
        <a href="javascript:;">
            <i class="fa fa-book"></i>
            <span class="title">{{Lang::get('messages.cms')}}</span>
            <span class="arrow"></span>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li class="tooltips {{Request::is('backend/cms/categories')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="Categories">
                <a href="{{url('backend/cms/categories')}}">
                    <i class="fa fa-bars"></i>
                    <span class="title">{{Lang::get('messages.categories')}}</span>
                </a>
            </li>
            <li class="tooltips {{Request::is('backend/cms/news')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="News">
                <a href="{{url('backend/cms/news')}}">
                    <i class="fa fa-book"></i>
                    <span class="title">{{Lang::get('messages.news')}}</span>
                </a>
            </li>
            <li class="tooltips {{Request::is('backend/cms/media')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="Multi upload media">
                <a href="{{url('backend/cms/media')}}">
                    <i class="fa fa-cloud-upload"></i>
                    <span class="title">{{Lang::get('messages.media')}}</span>
                </a>
            </li>
            <li class="tooltips {{Request::is('backend/cms/slide')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="News">
                <a href="{{url('backend/cms/slide')}}">
                    <i class="fa fa-sliders"></i>
                    <span class="title">{{Lang::get('messages.slide')}}</span>
                </a>
            </li>
            <li class="tooltips {{Request::is('backend/cms/slide')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="Static page">
                <a href="{{url('backend/cms/staticpage')}}">
                    <i class="fa fa-thumb-tack"></i>
                    <span class="title">{{Lang::get('messages.static_page')}}</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ Request::is('backend/setting*') ? 'start active' : '' }} {{Request::is('backend/setting')?'start active':''}}">
        <a href="javascript:;">
            <i class="fa fa-wrench"></i>
            <span class="title">{{Lang::get('messages.app_setting')}}</span>
            <span class="arrow"></span>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li class="tooltips {{Request::is('backend/setting/siteinfo')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="Menu">
                <a href="{{url('backend/setting/siteinfo')}}">
                        <i class="fa fa-info"></i>
                    <span class="title">{{Lang::get('messages.site_info')}} </span>
                </a>
            </li>
            <li class="tooltips {{Request::is('backend/setting/emailtemplate')?'active':''}}" data-container="body" data-placement="right" data-html="true"
                data-original-title="Menu">
                <a href="{{url('backend/setting/emailtemplate')}}">
                    <i class="fa fa-envelope"></i>
                    <span class="title">{{Lang::get('messages.email_template')}} </span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ Request::is('backend/management_site_template*') ? 'start active' : '' }} {{Request::is('backend/frontend_theme')?'start active':''}}">
        <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">{{Lang::get('messages.frontend_themes')}} </span>
            <span class="arrow"></span>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li class="tooltips {{Request::is('backend/frontend_theme')?'active':''}}" data-container="body"
                data-placement="right" data-html="true"
                data-original-title="Configuration Theme">
                <a href="{{url('backend/frontend_theme')}}">
                    <span class="title">{{Lang::get('messages.config_theme')}}  </span>
                </a>
            </li>
            @if(Session::get('username')==Session::get('acct'))
            <li class="{{ Request::is('backend/management_site_template/create') ? 'open active' : '' }} {{ Request::is('backend/management_site_template') ? 'open active' : '' }}">
                <a href="javascript:;">
                    <i class="icon-settings"></i> {{Lang::get('messages.template')}}  <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="{{Request::is('backend/management_site_template/create')?'active':''}}">
                        <a href="{{url('backend/management_site_template/create')}}"><i class="fa fa-plus"></i> Create
                            template </a>
                    </li>
                    <li class="{{Request::is('backend/management_site_template')?'active':''}}">
                        <a href="{{url('backend/management_site_template')}}"><i class="fa fa-bars"></i> List template
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ Request::is('backend/management_site_template_management') || Request::is('backend/management_site_template_master_page*') || Request::is('backend/management_site_template_group_css*') || Request::is('backend/management_site_template_class_css*') || Request::is('backend/management_site_template_constructor_block*') || Request::is('backend/management_site_template_constructor*') || Request::is('backend/management_site_template_config*') ? 'active' : '' }}">

                <a href="{{url('backend/management_site_template_management')}}">
                    <i class="icon-settings"></i> Management </span>
                </a>

            </li>
            @endif
        </ul>
    </li>

    <!-- START USER MANAGEMENT -->
    <li>
        <a href="javascript:;">
            <i class="fa fa-users"></i>
            <span class="title">{{Lang::get('messages.user_management')}}</span>
            <span class="arrow"></span>
            <span class="selected"></span>
        </a>

        <ul class="sub-menu">
            <li class="" data-container="body"
                data-placement="right" data-html="true"
                data-original-title="User Account">
                <a href="{{url('backend/users')}}">
                    <i class="fa fa-table"></i>
                    <span class="title">{{Lang::get('messages.user_account')}}</span>
                </a>
            </li>
            @if(Session::get('username')==Session::get('acct'))
            <li class="" data-container="body"
                data-placement="right" data-html="true"
                data-original-title="User Account">
            </li>
            @endif
        </ul>
    </li>
</ul>
<!-- END SIDEBAR MENU -->
</div>
</div>
<!-- END SIDEBAR -->