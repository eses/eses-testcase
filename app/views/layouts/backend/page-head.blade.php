<title>Eses | Admin Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="{{asset('assets/backend/assets/global/plugins/font-googleapis/font-googleapis.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
@yield('head')
<!-- BEGIN THEME STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{asset('assets/backend/assets/global/css/components.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/themes/light2.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/bootstrap-toastr/toastr.min.css')}}"/>

<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/bootstrap-select/bootstrap-select.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/jquery-multi-select/css/multi-select.css')}}"/>

<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/angular/loading-bar/loading-bar.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/angular-block-ui/angular-block-ui.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/angular-toastr/angular-toastr.min.css')}}"/>

<link href="{{asset('assets/backend/assets/global/css/components.css" rel="stylesheet')}}" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>