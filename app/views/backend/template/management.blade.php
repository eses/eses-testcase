@extends('layouts.admins.main')

@section('head')
    <title>Admin - Template menu</title>
    {{HTML::script('assets/admin/js/template-management.js')}}
@stop

@section('breadcrumb')
<div class="row">
    <div class="container-fluid line-bottom">
        <h4>
            <a href="{{ url('/') }}"><button type="button" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-home"></span></button></a>
            <a href="{{ url('/template') }}"><button type="button" class="btn btn-xs btn-info">Create Template</button></a> /
            <a href="{{ url('/template/management') }}"><button type="button" class="btn btn-xs btn-success">Template management</button></a>
        </h4>
    </div>
</div>
@stop

@section('content')
<div class="row">
    <div class="container-fluid theme-showcase padding-zero">
        <div class="col-md-4 padding-zero">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Create Template</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{url('admins/template/management')}}" id="create-template-form" >
                        <div class="row row-height">
                            <div class="col-md-4">Name</div>
                            <div class="col-md-8">
                                <input type="text" id="name" class="form-control col-md-12" name="name" />
                            </div>
                        </div>
                        <div class="row row-height">
                            <div class="col-md-4">Sort</div>
                            <div class="col-md-8">
                                <input type="text" class="form-control col-md-12" id="sort" name="sort" value="0" />
                            </div>
                        </div>
                        <div class="col-md-12 row-height">
                            <button type="submit" class="btn btn-primary col-md-12" name="create" value="create">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop