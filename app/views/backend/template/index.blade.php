@extends('layouts.admins.main')

@section('head')
<title>Admin - Template menu</title>
@stop

@section('breadcrumb')
<div class="row">
	<div class="container-fluid line-bottom">
		<h4>
			<a href="{{ url('/') }}">
				<button type="button" class="btn btn-xs btn-success">
					<span class="glyphicon glyphicon-home"></span>
				</button>
			</a>
		</h4>
	</div>
</div>
@stop

@secrion('content')

@stop
