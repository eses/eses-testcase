@extends('layouts.admins.main')

@section('head')
	<title>Admin - Template menu management</title>
    {{HTML::script('assets/admin/js/template-color.js')}}
@stop

@section('breadcrumb')
<div class="row">
  <div class="container-fluid line-bottom">
    <h4>
      <a href="{{ url('/') }}"><button type="button" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-home"></span></button></a> / 
      <a href="{{ url('/template') }}"><button type="button" class="btn btn-xs btn-info">Create Template</button></a> /
      <a href="{{ url('/template/color') }}"><button type="button" class="btn btn-xs btn-success">Color style management</button></a>
    </h4>
  </div>
</div>
@stop

@section('content')
<div class="row">
    <div class="container-fluid theme-showcase padding-zero">
        <div class="col-md-4 padding-zero">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Create Color</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{url('admins/template/color')}}" id="create-color-form" >
                        <div class="row row-height">
                            <div class="col-md-4">Name</div>
                            <div class="col-md-8">
                                <input type="text" id="name" class="form-control col-md-12" name="name" />
                            </div>
                        </div>
                        <div class="row row-height">
                            <div class="col-md-4">Class css</div>
                            <div class="col-md-8">
                                <input type="text" class="form-control col-md-12" id="class_css" name="class_css" />
                            </div>
                        </div>
                        <div class="row row-height">
                            <div class="col-md-4">File name css</div>
                            <div class="col-md-8">
                                <input type="text" class="form-control col-md-12" id="file_name" name="file_name" />
                            </div>
                        </div>
                        <div class="row row-height">
                            <div class="col-md-4">Menu color</div>
                            <div class="col-md-8">
                                <select class="form-control col-md-12" name="menu_color">
                                    @foreach($menus as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row row-height">
                            <div class="col-md-4">Template</div>
                            <div class="col-md-8">
                                <select class="form-control col-md-12" name="template">
                                    @foreach($templates as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row row-height">
                            <div class="col-md-4">Sort</div>
                            <div class="col-md-8">
                                <input type="text" class="form-control col-md-12" id="sort" name="sort" value="0" />
                            </div>
                        </div>
                        <div class="col-md-12 row-height">
                            <button type="submit" class="btn btn-primary col-md-12" name="create" value="create">Create Color</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop