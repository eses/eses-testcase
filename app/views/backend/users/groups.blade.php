<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<div class='row'>
	<div class='col-md-12'>
		<legend>
			<h4>Groups Manager</h4>
		</legend>
		<div class='form-group'>
			<a class="btn btn-sm bg-green-meadow" data-toggle="modal" href="#responsive"><i class='fa fa-plus'></i> Create</a>
			<a class="btn btn-sm btn-danger" data-toggle="modal" href="#responsive"><i class='fa fa-trash-o'></i> Remove</a>
			<a class="btn btn-sm btn-default" href="#"><i class='fa fa-download'></i> Download</a>
			<a class="btn btn-sm btn-default" href="#"><i class='fa fa-cog'></i> Configuration</a>
		</div>
		<div class='form-group'>
			<table class="table table-striped table-hover" id='groups'>
				<thead>
				<tr>
					<th>No</th>
					<th><input type='checkbox' class='form-control'></th>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Level</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>1</th>
					<th><input type='checkbox' class='form-control'></th>
					<th>1</th>
					<td>Superadmin</td>
					<td>Root Superadmin , should be as top level group</td>
					<td>1</td>
					<td><span class='label bg-green-meadow'>Active</span></td>
					<td>
						<a href="#" class="btn btn-sm bg-green-meadow"><i class='fa fa-search'></i></a>
						<a href="#" class="btn btn-sm btn-danger"><i class='fa fa-trash-o'></i></a>
					</td>
				</tr>
				<tr>
					<th>2</th>
					<th><input type='checkbox' class='form-control'></th>
					<th>2</th>
					<td>Administrator</td>
					<td>Administrator level, level No 2</td>
					<td>2</td>
					<td><span class='label label-warning'>Inactive</span></td>
					<td>
						<a href="#" class="btn btn-sm bg-green-meadow"><i class='fa fa-search'></i></a>
						<a href="#" class="btn btn-sm btn-danger"><i class='fa fa-trash-o'></i></a>
					</td>
				</tr>
				<tr>
					<th>3</th>
					<th><input type='checkbox' class='form-control'></th>
					<th>3</th>
					<td>Users</td>
					<td>Users as registered / member</td>
					<td>3</td>
					<td><span class='label bg-green-meadow'>Active</span></td>
					<td>
						<a href="#" class="btn btn-sm bg-green-meadow"><i class='fa fa-search'></i></a>
						<a href="#" class="btn btn-sm btn-danger"><i class='fa fa-trash-o'></i></a>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add new language</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-md-12">
						<div class='form-group'>
							<div class='row'>
								<label class='col-md-4'>Languange Name</label>
								<div class='col-md-8'>
									<input class="form-control" type="text" id='languageName' name='languageName'>
								</div>
							</div>
						</div>
						<div class='form-group'>
							<div class='row'>
								<label class='col-md-4'>Folder Name</label>
								<div class='col-md-8'>
									<input class="form-control" type="text" id='folderName' name='folderName'>
								</div>
							</div>
						</div>
						<div class='form-group'>
							<div class='row'>
								<label class='col-md-4'>Author</label>
								<div class='col-md-8'>
									<input class="form-control" type="text" id='author' name='author'>
								</div>
							</div>
						</div>
						<div class='form-group'>
							<div class='row'>
								<input type='button' class='btn bg-green-meadow' value='Add language'>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/users/groups-init-datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type='text/javascript'>
	$(document).ready(function(){
		DataTableGroups.init();
	});
</script>