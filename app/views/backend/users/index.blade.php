@extends('layouts.backend.main')

<!-- Section head page -->
@section('head')
<title>Admin - Setting</title>

<link id="style_color" href="{{asset('assets/backend/assets/admin/layout/css/themes/default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
<link
	href="{{asset('assets/backend/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}"
	rel="stylesheet" type="text/css" />
<link
	href="{{asset('assets/backend/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/admin/pages/scripts/users/css/style.css')}}"/>

<link href="{{asset('assets/backend/assets/global/css/components.css')}}" rel="stylesheet" type="text/css"/>
<script>
var lang='{{$lang}}';
</script>

@stop

@section('content')
<div ng-app="UserManagement" ng-controller="UserController">
    <ng-view></ng-view>
</div>
@stop
<!-- Section core plugin -->
@section('core-plugin')
<script type="text/javascript" src="http://crypto-js.googlecode.com/svn/tags/3.0.2/build/rollups/md5.js"></script>

<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>



<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/bootbox/bootbox.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/form-samples.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/users/service/UserService.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/users/controller/UserController.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/scripts/directive/eses-unique.js')}}"></script>

<script type="text/javascript">
    jQuery(document).ready(function() {

    });
</script>
@stop
