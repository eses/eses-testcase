@extends('layouts.backend.main')

<!-- Section head page -->
@section('head')
<title>Admin - Setting</title>

<link href="{{asset('assets/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/css/components.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/backend/assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{asset('assets/backend/assets/admin/layout/css/themes/default.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/admin/pages/scripts/users/css/style.css')}}"/>

<link href="{{asset('assets/backend/assets/global/css/components.css')}}" rel="stylesheet" type="text/css"/>

<link rel="shortcut icon" href="favicon.ico"/>

@stop

@section('content')
<script>
    var username = '{{$username}}';
</script>
<div class="clearfix">
    <input type="hidden" id="hide-avatar-value" value="{{asset(Session::get('avatar'))}}">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<div ng-app="AdminProfile" ng-controller="AdminProfileController">
    <ng-view></ng-view>
</div>
</div>
@stop
<!-- Section core plugin -->
@section('core-plugin')

<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/backend/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>


<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/admin_profile/admin-profile-service.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/admin_profile/admin-profile-controller.js')}}"></script>

@stop
