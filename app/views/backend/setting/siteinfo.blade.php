@extends('layouts.backend.main')

<!-- Section head page -->
@section('head')
	<title>Admin - Email template setting</title>
@stop

<!-- Section breadcrumb page -->
@section('breadcrumb')
	<h3 class="page-title">
		Email template <small>Email template setting</small>
	</h3>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="fa fa-home font-green-meadow"></i>
			<a href="{{url('backend')}}">{{Lang::get('messages.dashboard')}}</a>
			<i class="fa fa-angle-right font-green-meadow"></i>
		</li>
		<li>
			<a href="{{url('backend/setting')}}">{{Lang::get('messages.setting')}}</a>
			<i class="fa fa-angle-right font-green-meadow"></i>
		</li>
		<li>
			<a href="{{url('backend/setting/emailtemplate')}}">{{Lang::get('messages.email_template')}}</a>
		</li>
	</ul>
@stop

<!-- Section content page -->
@section('content')
	<div ng-app="appSettingSiteInfo">
		<ng-view></ng-view>
	</div>
@stop

<!-- Section core plugin -->
@section('core-plugin')

	<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/backend/setting/siteinfo/appSettingSiteInfo.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/backend/setting/siteinfo/settingSiteInfoController.js')}}"></script>
@stop