@extends('layouts.backend.main')

<!-- Section head page -->
@section('head')
<title>Admin - Setting</title>
<link
	href="{{asset('assets/backend/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}"
	rel="stylesheet" type="text/css" />
<link
	href="{{asset('assets/backend/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}"
	rel="stylesheet" type="text/css" />
@stop

<!-- Section breadcrumb page -->
@section('breadcrumb')
<h3 class="page-title">
	Page setting <small>management setting configuration</small>
</h3>
<ul class="page-breadcrumb breadcrumb">
	<li>
		<i class="fa fa-home font-green-meadow"></i>
		<a href="{{url('backend')}}">{{Lang::get('messages.dashboard')}}</a>
		<i class="fa fa-angle-right font-green-meadow"></i>
	</li>
	<li>
		<a href="{{url('backend/setting')}}">{{Lang::get('messages.frontend_themes')}}</a>
	</li>
</ul>
@stop

<!-- Section content page -->
@section('content')
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab_info" data-toggle="tab" class="font-green-meadow"> Site Info</a>
		</li>
		<li>
			<a href="#tab_template" data-toggle="tab" class="font-green-meadow"> Email template</a>
		</li>
		<li>
			<a href="#tab_login" data-toggle="tab" class="font-green-meadow"> Login & Security</a>
		</li>
		<li>
			<a href="#tab_translation" data-toggle="tab" class="font-green-meadow"> Translation</a>
		</li>
		<li>
			<a href="#tab_cache" data-toggle="tab" class="font-green-meadow"> Clear Cache & Logs</a>
		</li>
		<li>
			<a href="#tab_core" data-toggle="tab" class="font-green-meadow"> Core</a>
		</li>
		<li>
			<a href="#tab_pagination" data-toggle="tab" class="font-green-meadow"> Pagination</a>
		</li>
		<li>
			<a href="#tab_reminders" data-toggle="tab" class="font-green-meadow"> Reminders</a>
		</li>
		<li>
			<a href="#tab_validation" data-toggle="tab" class="font-green-meadow"> Validation</a>
		</li>
	</ul>
	<div class="tab-content" >
		<div class="tab-pane active" id="tab_info">
		</div>
		<div class="tab-pane " id="tab_template">
			@include('backend.setting.email-template')
		</div>
		<div class="tab-pane " id="tab_login">
			@include('backend.setting.login-security')
		</div>
		<div class="tab-pane " id="tab_translation">
			@include('backend.setting.translation')
		</div>
		<div class="tab-pane " id="tab_cache">
			@include('backend.setting.cache')
		</div>
		<div class="tab-pane active" id="tab_core">
			@include('backend.setting.language-core')
		</div>
		<div class="tab-pane" id="tab_pagination">
			@include('backend.setting.language-pagination')
		</div>
		<div class="tab-pane" id="tab_reminders">
			@include('backend.setting.language-reminders')
		</div>
		<div class="tab-pane" id="tab_validation">
			@include('backend.setting.language-validation')
		</div>
	</div>
</div>
@stop

<!-- Section core plugin -->
@section('core-plugin')
<script
	src="{{asset('assets/backend/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"
	type="text/javascript"></script>
<script
	src="{{asset('assets/backend/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')}}"
	type="text/javascript"></script>
@stop
