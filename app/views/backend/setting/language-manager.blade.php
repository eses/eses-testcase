<h4>Language Manager</h4>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab_core" data-toggle="tab" class="font-green-meadow"> Site Info</a>
		</li>
		<li>
			<a href="#tab_pagination" data-toggle="tab" class="font-green-meadow"> Email template</a>
		</li>
		<li>
			<a href="#tab_reminders" data-toggle="tab" class="font-green-meadow"> Login & Security</a>
		</li>
		<li>
			<a href="#tab_valadation" data-toggle="tab" class="font-green-meadow"> Translation</a>
		</li>
	</ul>
	<div class="tab-content" >
		<div class="tab-pane active" id="tab_core">
			@include('backend.setting.siteinfo')
		</div>
		<div class="tab-pane " id="tab_pagination">
			@include('backend.setting.email-template')
		</div>
		<div class="tab-pane " id="tab_reminders">
			@include('backend.setting.login-security')
		</div>
		<div class="tab-pane " id="tab_validation">
			@include('backend.setting.translation')
		</div>
	</div>
</div>