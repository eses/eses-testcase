<div class='row'>
	<div class='col-md-12'>
		<legend>
			<h4>Language Manager</h4>
		</legend>
		<div class='form-group'>
			<a class="btn default" data-toggle="modal" href="#responsive">Add new language</a>
		</div>
		<div class='form-group'>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Folder</th>
						<th>Author</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>English</td>
						<td>en</td>
						<td>Hoan Tran</td>
						<td></td>
					</tr>
					<tr>
						<td>Việt Nam</td>
						<td>vi</td>
						<td>Hoan Tran</td>
						<td>
							<a href="#" class="btn btn-sm btn-primary"> Manage </a>
							<a href="#" class="btn btn-sm btn-danger"> Delete </a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add new language</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-md-12">
						<div class='form-group'>
							<div class='row'>
								<label class='col-md-4'>Languange Name</label>
								<div class='col-md-8'>
									<input class="form-control" type="text" id='languageName' name='languageName'>
								</div>
							</div>
						</div>
						<div class='form-group'>
							<div class='row'>
								<label class='col-md-4'>Folder Name</label>
								<div class='col-md-8'>
									<input class="form-control" type="text" id='folderName' name='folderName'>
								</div>
							</div>
						</div>
						<div class='form-group'>
							<div class='row'>
								<label class='col-md-4'>Author</label>
								<div class='col-md-8'>
									<input class="form-control" type="text" id='author' name='author'>
								</div>
							</div>
						</div>
						<div class='form-group'>
							<div class='row'>
								<input type='button' class='btn bg-green-meadow' value='Add language'>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>