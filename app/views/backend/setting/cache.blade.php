<div class='tab-content'>
	<legend>
		<h4>Session Cache Template</h4>
	</legend>
	<div class='row'>
		<div class='col-md-12'>
			<p>Error log <span class='label label-danger'>1.358 KB</span></p>
			<p>Template Cache</p>
		</div>		
		<div class='col-md-12'>
			<input type='button' id='saveChange' class='btn bg-green-meadow' value='Clear cache & log'> <input
				type='button' id='cancel' class='btn btn-default' value='Cancel'>
		</div>
	</div>
</div>