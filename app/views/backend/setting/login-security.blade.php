<div class='row'>
	<div class='col-md-6'>
		<legend>
			<h4>Social media</h4>
		</legend>
		<div class='form-group'>
			<div class='row'>
				<label class='col-md-4'><i class='fa fa-facebook font-blue'></i> Login Facebook</label>
				<div class='col-md-8'>
					<div class='form-group'>
						<input type='checkbox' class='form-control' id='facebookEnable' name='facebookEnable'> Enable
					</div>
					<div class='form-group'>
						<label>APP ID:</label> <input class='form-control' id='facebookAppId' name='facebookAppId'>
					</div>
					<div class='form-group'>
						<label>SECRET NUMBER:</label> <input class='form-control' id='facebookSecetNumber'
							name='facebookSecetNumber'>
					</div>
				</div>
			</div>
			<div class='row'>
				<label class='col-md-4'><i class='fa fa-google font-red'></i> Login Google</label>
				<div class='col-md-8'>
					<div class='form-group'>
						<input type='checkbox' class='form-control' id='googleEnable' name='googleEnable'> Enable
					</div>
					<div class='form-group'>
						<label>APP ID:</label> <input class='form-control' id='googleAppId' name='googleAppId'>
					</div>
					<div class='form-group'>
						<label>SECRET NUMBER:</label> <input class='form-control' id='googleSecetNumber'
							name='googleSecetNumber'>
					</div>
				</div>
			</div>
			<div class='row'>
				<label class='col-md-4'><i class='fa fa-twitter font-blue-madison'></i> Login Twitter</label>
				<div class='col-md-8'>
					<div class='form-group'>
						<input type='checkbox' class='form-control' id='twiterEnable' name='twitterEnable'> Enable
					</div>
					<div class='form-group'>
						<label>APP ID:</label> <input class='form-control' id='twitterAppId' name='twitterAppId'>
					</div>
					<div class='form-group'>
						<label>SECRET NUMBER:</label> <input class='form-control' id='twitterSecetNumber'
							name='twitterSecetNumber'>
					</div>
				</div>
			</div>
		</div>
		<input type='button' id='saveChangeEmail' class='btn bg-green-meadow' value='Save changes'>
	</div>
	<div class='col-md-6'>
		<legend>
			<h4>Registration Setting</h4>
		</legend>
		<div class='form-group'>
			<div class='row'>
				<label class='col-md-4'>Default Group Registration</label>
				<div class='col-md-8'>
					<select class='form-control' id='regGroupDefault' name='regGroupDefault'>
						<option value='user'>User</option>
						<option value='supperadmin'>Supper Administrator</option>
						<option value='admin'>Administator</option>						
					</select>
				</div>
			</div>
			<div class='row'>
				<label class='col-md-4'>Registration</label>
				<div class='col-md-8'>
					<div class='form-group'>
						<input type='radio' id='automatic' name='automaticActivation' class='form-control'>
						Automatic activation
					</div>
					<div class='form-group'>
						<input type='radio' id='manual' name='manualActivation' class='form-control'>
						Manual activation
					</div>
					<div class='form-group'>
						<input type='radio' id='sendEmail' name='sendEmailActivation' class='form-control'>
						Email with activation link
					</div>
				</div>
			</div>
			<div class='row'>
				<label class='col-md-4'>Allow Registration</label>
				<div class='col-md-8'>
					<input type='checkbox' id='regAllow' name='regAllow' class='form-control'>
					Enable
				</div>
			</div>
			<div class='row'>
				<label class='col-md-4'>Allow Frontend</label>
				<div class='col-md-8'>
					<input type='checkbox' id='frontendAllow' name='frontendAllow' class='form-control'>
					Enable
				</div>
			</div>
			<div class='row'>
				<label class='col-md-4'>Recaptcha</label>
				<div class='col-md-8'>
					<div class='form-group'>
						<input type='checkbox' id='recaptcha' name='recaptcha' class='form-control'>
						Enable
					</div>
					<div class='form-group'>
						<label>Public Key:</label>
						<input type='text' id='publicKey' name='publicKey' class='form-control'>
					</div>
					<div class='form-group'>
						<label>Private Key:</label>
						<input type='text' id='privateLKey' name='privateLKey' class='form-control'>
					</div>
				</div>
			</div>
		</div>
		<input type='button' id='saveChangeForgotPassword' class='btn bg-green-meadow'
			value='Save changes'>
	</div>
</div>