<div class='row'>
	<div class='col-md-12'>
		<div class='form-group'>
			<div class='row'>
				<div class='col-md-4'>
					<legend>
						<h4>Pharse</h4>
					</legend>
				</div>
				<div class='col-md-8'>
					<legend>
						<h4>Translation</h4>
					</legend>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<div class='row'>
				<label class='col-md-4'>password</label>
				<div class='col-md-8'>
					<input class='form-control' id='password' name='password'>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<div class='row'>
				<label class='col-md-4'>user</label>
				<div class='col-md-8'>
					<input class='form-control' id='user' name='user'>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<div class='row'>
				<label class='col-md-4'>token</label>
				<div class='col-md-8'>
					<input class='form-control' id='token' name='token'>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<div class='row'>
				<label class='col-md-4'>sent</label>
				<div class='col-md-8'>
					<input class='form-control' id='sent' name='sent'>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<input type='input' class='btn bg-green-meadow' id='saveTranslation' name='saveTranslation'
				value='Save Translation'>
		</div>
	</div>
</div>