@extends('layouts.backend.main')

<!-- Section head page -->
@section('head')
	<title>Admin - Site info setting</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/codemirror/lib/codemirror.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/backend/assets/global/plugins/codemirror/theme/eclipse.css')}}">
@stop

<!-- Section breadcrumb page -->
@section('breadcrumb')
	<h3 class="page-title">
        {{Lang::get('messages.site_information')}} <small>{{Lang::get('messages.management_site_information_setting')}}</small>
	</h3>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="fa fa-home font-green-meadow"></i>
			<a href="{{url('backend')}}">{{Lang::get('messages.dashboard')}}</a>
			<i class="fa fa-angle-right font-green-meadow"></i>
		</li>
		<li>
			<a href="{{url('backend/setting')}}">{{Lang::get('messages.setting')}}</a>
			<i class="fa fa-angle-right font-green-meadow"></i>
		</li>
		<li>
			<a href="{{url('backend/setting/siteinfo')}}">{{Lang::get('messages.site_info')}}</a>
		</li>
	</ul>
@stop

<!-- Section content page -->
@section('content')
	<div ng-app="appSettingEmailTemplate">
		<ng-view></ng-view>
	</div>
@stop

<!-- Section core plugin -->
@section('core-plugin')
	<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/codemirror/lib/codemirror.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/backend/setting/emailtemplate/appSettingEmailTemplate.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/backend/assets/admin/pages/scripts/backend/setting/emailtemplate/settingEmailTemplateController.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/backend/assets/global/plugins/angular-ui/ui-codemirror.js')}}"></script>
@stop