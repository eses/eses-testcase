<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// ===============================================
// BACKEND SECTION ===============================
// ===============================================

//Login backend
Route::get("/admin/login", "BackendLoginController@showLogin");
Route::post("/admin/login", "BackendLoginController@doLogin");
Route::get("/admin/logout", "BackendLoginController@doLogout");

// Backend
Route::group(array('before'=>'AuthAdmin','prefix' => 'backend'), function () {
    //testcase management
    Route::get('testlink', 'TestCaseManagementController@showIndex');
    Route::get('testlink/getall', 'TestCaseManagementController@getTestLinks');
    Route::post('testlink/add', 'TestCaseManagementController@addTestLink');
    Route::post('testlink/update', 'TestCaseManagementController@updateTestLink');
    Route::post('testlink/testcase/add', 'TestCaseManagementController@addTestCase');
    Route::post('testlink/testcase/update', 'TestCaseManagementController@updateTestCase');
    Route::post('testlink/get','TestCaseManagementController@getTestLink');
    Route::post('testlink/testcase/get', 'TestCaseManagementController@getTestCase');
    Route::get('testlink/testcase/getall', 'TestCaseManagementController@getTestCases');
    Route::get('admin/profile', 'AdminProfileController@showIndex');
    Route::post("/api/admin/profile/getUser",  "AdminProfileController@getUserProfile");
    Route::post("/api/admin/profile/updateUser", "AdminProfileController@updateUserProfile");
    Route::post("/api/admin/profile/updatePassword", "AdminProfileController@updatePassword");
    Route::post("/api/admin/profile/updateAvatar", "AdminProfileController@updateAvatar");
    //user management
    Route::get('/users', "BackendUserManagementController@showIndex");

    //for user manaement
    Route::get("/api/users",  "BackendUserManagementController@getUsers");
    Route::get("/api/users/roles", "BackendUserManagementController@getUserRoles");
    Route::get("/api/users/roles/datatable", "BackendUserManagementController@getUserRoleDataTable");
    Route::put("/api/users/upsert", "BackendUserManagementController@upsertUser");
    Route::post("/api/users/add", "BackendUserManagementController@addUser");
    Route::post("/api/users/verify", "BackendUserManagementController@verifyUserInfo");
    Route::post("/api/users/getUserById", "BackendUserManagementController@getUserInfo");

	//Dashboard
    Route::get('/dashboard', 'BackendHomeController@showHome');
    

});