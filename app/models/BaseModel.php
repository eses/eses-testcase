<?php

class BaseModel extends Eloquent {

	// Set default $timestamps 
	public $timestamps = true;

}