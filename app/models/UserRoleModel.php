<?php
/**
 * Created by PhpStorm.
 * User: Nadare
 * Date: 1/16/15
 * Time: 6:39 PM
 */

class UserRoleModel extends BaseModel{
    public $table="user_role";

    public static function getRoles($start, $length){
        $userData = UserRoleModel::skip($start)->take($length)->get()->toArray();
        $paginationData = Paginator::make($userData, UserRoleModel::count(), $length)->toArray();
        return $paginationData;
    }
} 