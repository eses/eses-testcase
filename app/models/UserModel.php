<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
class UserModel extends BaseModel  implements UserInterface, RemindableInterface {

	protected  $table = "user";
    protected $hidden = ["password"];

    const USER_INFO = "user_info";

    const ID = "id";
    const USER_ID = "username";
    const USER_PASSWORD = "password";
    const USER_EMAIL = "email";
    const USER_ADDRESS = "address";
    const USER_PHONE = "phone";
    const USER_STATUS = "status";
    const USER_ROLE = "role";
    const USER_GENDER = "gender";
    const USER_FIRST_NAME = "first_name";
    const USER_LAST_NAME = "last_name";
    const USER_STATUS_ACTIVE = 1;
    const USER_STATUS_DEACTIVE = 0;
    const USER_ADMIN_ROLE = 1;
    const USER_USER_ROLE = 2;

    const IS_LOGIN_FB = "IsLoginFB";
    const IS_LOGIN_GG = "IsLoginGG";

    const IS_LOGIN = 'IsLogin';
    const IS_EDIT = "IsEdit";

    //message
    const ERROR_ALREADY_USERNAME = "Your username is already in database";
    const ERROR_ALREADY_EMAIL = "Your email is already in database";
    //for authentication
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return "remember_token";
    }

    public function getReminderEmail()
    {
        return $this->email;
    }
}
