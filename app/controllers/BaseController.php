<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
            $this->layout = View::make($this->layout);
		}

        // Get theme default
        $theme = array();

        // Get default template
        $template = SiteTemplate::whereRaw('status = ? and `default` = ?', array(Status::ACTIVE, IsDefault::YES))->first();

        // Get default master page
        $masterPage = SiteTemplateMasterPage::whereRaw('template_id = ? and `default` = ? and status = ?',
            array($template->id, IsDefault::YES, Status::ACTIVE))->first();

        // Get class global
        $arrClass = DB::table('site_template_class_group as g')
            ->join('site_template_class_css as c', 'g.id', '=', 'c.group_id')
            ->whereRaw('g.template_id = ? and g.status = ? and c.status = ? and g.`default` = ? and c.`default` = ?',
                array($template->id, Status::ACTIVE, Status::ACTIVE, IsDefault::YES, IsDefault::YES))
            ->select('g.group_key', 'c.class_value')
            ->get();
        $arrClassGlobal = array();
        foreach ($arrClass as $value) {
            $arrClassGlobal[$value->group_key] = $value->class_value;
        }

        // Get default constructor
        $arrConstructor = DB::table('site_template_config as cf')
            ->join('site_template_constructor_block as bl', 'cf.constructor_block', '=', 'bl.id')
            ->join('site_template_constructor as c', 'bl.id', '=', 'c.constructor_block_id')
            ->whereRaw('cf.master_page = ? and cf.status = ? and cf.`default` = ? and bl.status = ? and c.`default` = ? and c.status = ?',
                array($masterPage->id, Status::ACTIVE, IsDefault::YES, Status::ACTIVE, IsDefault::YES, Status::ACTIVE))
            ->select('bl.block', 'c.file')
            ->get();
        $arrDetailConstructor = array();
        foreach ($arrConstructor as $value) {
            $arrDetailConstructor[$value->block] = $value->file;
        }
        //var_dump($arrClassGlobal); die();

        $menu = NewsCategoriesModel::where('categories_status','=','1')->orderBy('categories_sort','asc')->get();
//        $slide = ConfigSlideDetailModel::where('slide_id','1')->where('status','1')
//            ->get(array(
//                'slide_detail_image as image',
//                'slide_detail_text as text',
//                'slide_detail_class as class',
//                'slide_detail_video as video'
//            ));

        $theme['template'] = $template->template_code;
        $theme['class-global'] = $arrClassGlobal;
        $theme['master-page'] = $masterPage->master_page;
        $theme['constructor'] = $arrDetailConstructor;
        $theme['mainMenu'] = $menu;
        //$theme['slide'] = $slide;
        
        View::share('theme', $theme);
	}
}
