<?php

class BackendLoginController extends BaseBackendController {

    /**
     * Show page admin login
     * POST: backend/login
     * @return \Illuminate\View\View
     */
    public function showLogin()
    {
        return View::make('backend.login.index')->with('captcha', false);
    }
    public function doLogin(){

        if(!is_null(Input::has('g-recaptcha-response')) && (int)Session::get('LoginFail') > 1) {
            Log::info("login with catpcha");
            $rules = array(
                'g-recaptcha-response' => 'required|recaptcha:' . Input::get('g-recaptcha-response')
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails())
            {
                Log::info("Failed Captcha");
                return $this->eventLoginFailed(Lang::get('messages.failed_to_login'));
            }
        }
        Log::info("login without catpcha");
        $username = Str::lower(Input::get(UserModel::USER_ID));
        $password = Input::get(UserModel::USER_PASSWORD);
        Log::info("user:".$username);
        Log::info("pass:".$password);
       // if (Auth::attempt(array("username"=>$username, "password"=>$password))) {

            $userInfo = User::getUserInfo($username);
           // if($userInfo->status != 1){
             //   return $this->eventLoginFailed(Lang::get('messages.account_is_deactive'));
            //}
            Log::info("login success");
            Log::info($userInfo);
            if($userInfo->role == UserModel::USER_ADMIN_ROLE){
                Session::put(UserModel::USER_ID, $username);
                Session::put(UserModel::USER_INFO, $userInfo);
                Session::put(UserModel::IS_LOGIN, true);
                Session::put(UserModel::IS_EDIT, true);
                Session::put('acct', \AppSettingEnum::ESES_Account);
                Session::forget('LoginFail');
                //redirect to edit profile admin, em cần cái link dẫn đến nơi update profile của admin
                if(!isset($userInfo->avatar) || is_null($userInfo)){
                    Session::put('avatar', 'assets/backend/assets/admin/pages/img/no-avatar.png');
                }else{
                    Session::put('avatar', $userInfo->avatar);
                }
                Log::info('user avatar'.$userInfo);
                return Redirect::to('/backend/dashboard');
            }else{
                return $this->eventLoginFailed(Lang::get('messsages.account_is_invalid'));

            }

      //  } else {
        //    return $this->eventLoginFailed(Lang::get('messages.failed_to_login'));
        //}
    }

    public function eventLoginFailed($error){
        Log::error("login failed");
        if (Session::has('LoginFail')) {
            $pre_session = (int)Session::get('LoginFail') + 1;
            Session::put('LoginFail', $pre_session);
            if ($pre_session > 2) {
                return View::make('backend.login.index')->with('captcha', true)->withErrors([$error]);
            }
        } else {
            Session::put('LoginFail', 1);
        }
        return View::make('backend.login.index')->with('captcha', false)->withErrors([$error]);
    }
    public function doLogout(){
        Event::fire('user.logout');
        return View::make('backend.login.index')->with('captcha', false);
    }
}