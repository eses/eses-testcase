<?php
/**
 * Created by PhpStorm.
 * User: Nadare
 * Date: 1/10/15
 * Time: 4:28 PM
 */

class BackendUserManagementController extends BaseController {

    public $userModelDAO;
    const LENGTH = "length";
    const START = "start";

    public function showIndex(){

        return View::make('backend.users.index')
            ->with('lang',json_encode(Lang::get('messages')));

    }
    public function showCreatePage(){
        return View::make('backend.users.index')->with('lang',json_encode(Lang::get('messages')));
    }

    public function getUsers(){
        $length = Input::get(self::LENGTH);
        $start = Input::get(self::START);
        $keyword = Input::get('search')['value'];
        Log::info('keyword='.$keyword);
        $data = User::getUserBy($keyword,$start, $length);
        $dataPaginate = array(
            'draw' => Input::get('draw'),
            'recordsTotal' => $data['total'],
            'recordsFiltered'=> $data['total'],
            'data' => $data['data']
        );
        Log::info($dataPaginate);
        return Response::json($dataPaginate);
    }

    public function getUserInfo(){
        $id = Input::get('id');
        $data = UserModel::find($id);
        Log::info($data);
        return Response::json($data);
    }

    public function getUserRoles(){
        $roles = UserRoleModel::all();

        return Response::json($roles);
    }

    public function getUserRoleDataTable(){
        $length = 5;//Input::get(self::LENGTH);
        $start = 0;//Input::get(self::START);
        $data = UserRoleModel::getRoles($start,$length);
        $dataPaginate = array(
            'draw' => Input::get('draw'),
            'recordsTotal' => $data['total'],
            'recordsFiltered'=> $data['total'],
            'data' => $data['data']
        );
        return Response::json($dataPaginate);
    }

    public function upsertUser(){
        Log::info("Update Users");
        $this->userModelDAO = new UserModel();
        $userData = new UserModel();
        $userData->username = Input::get(UserModel::USER_ID);
        if(array_key_exists('password',Input::all())){
            Log::info("change password");
            $userData->password = Hash::make(Input::get(UserModel::USER_PASSWORD));
        }
        $userData->email = Input::get(UserModel::USER_EMAIL);
        $userData->address = Input::get(UserModel::USER_ADDRESS);
        $userData->phone = Input::get(UserModel::USER_PHONE);
        $userData->role = Input::get(UserModel::USER_ROLE);
        $userData->status = Input::get(UserModel::USER_STATUS);
        $userData->gender = Input::get(UserModel::USER_GENDER);
        $userData->first_name = Input::get(UserModel::USER_FIRST_NAME);
        $userData->last_name = Input::get(UserModel::USER_LAST_NAME);
        $userData->status = Input::get(UserModel::USER_STATUS);
        $userData->role = Input::get(UserModel::USER_ROLE);
        $result = User::updateUserProfile($userData);
        $error = 0;
        if($result != true){
            Log::info('failed,rs='.$result);
            $error = 1;
        }
        return Response::json(array('error' => $error), 200);
    }
    public function addUser(){

        Log::info("Add User");
        try{
            $result  = User::adminAddUser(Input::all());
            Log::info("End Adding User");
            Log::info($result);
            if(is_bool($result)){
                $data = array("error" => 0, "message" => Lang::get('messages.adding_user_successfully'));
                return Response::json($data);
            }else{
                $data = array("error"=>1, "message" => $result);
                return Response::json($data);
            }
        }catch(Exception $ex){
            Log::info("Failed To Create User" . $ex->getMessage());
            $data = array("error" => 1, "message" => $ex->getMessage());
            return Response::json($data);
        }
    }

    public function verifyUserInfo(){
        $value = Input::get('value');
        $name = Input::get('name');
        Log::info(Input::all());

        $result = User::isAlreadyInfo($name, $value);
        if($result){
            Log::info('true');
        }else{
            Log::info('false');
        }
        return Response::json(!$result);
    }


} 