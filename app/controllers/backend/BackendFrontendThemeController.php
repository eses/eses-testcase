<?php

class BackendFrontendThemeController extends BaseBackendController
{

    /**
     * Frontend theme config
     * GET: backend/frontend_theme
     * @return mixed
     */
    public function showFrontendTheme()
    {
        $templates = SiteTemplate::where('status', Status::ACTIVE)->get();

        // Get default master page
        $templateId = SiteTemplate::whereRaw('status = ? and `default` = ?', array(Status::ACTIVE, IsDefault::YES))->first()->id;
        $arrMasterPage = SiteTemplateMasterPage::whereRaw('template_id = ? and status = ?', array($templateId, Status::ACTIVE))->get();
        $arrClass = DB::table('site_template_class_group as g')
            ->join('site_template_class_css as c', 'g.id', '=', 'c.group_id')
            ->whereRaw('g.template_id = ? and g.status = ? and c.status = ?', array($templateId, Status::ACTIVE, Status::ACTIVE))
            ->select('g.id as group_id', 'c.id as class_id', 'g.template_id', 'g.group_key', 'g.group_name', 'g.default as default_group', 'c.class_value', 'c.name', 'c.default as default_class')
            ->get();
        $arrGroup = array();
        $arrDetail = array();
        foreach ($arrClass as $value) {
            $arrGroup[$value->group_key] = $value->group_name;
            $arrDetail[$value->group_key][] = array($value->name, $value->class_value, $value->default_class);
        }

        // Get default constructor
        $masterPage = SiteTemplateMasterPage::whereRaw('template_id = ? and status = ? and `default` = ?',
            array($templateId, Status::ACTIVE, IsDefault::YES))->first()->id;
        $arrConstructor = DB::table('site_template_config as cf')
            ->join('site_template_constructor_block as bl', 'cf.constructor_block', '=', 'bl.id')
            ->join('site_template_constructor as c', 'bl.id', '=', 'c.constructor_block_id')
            ->whereRaw('cf.master_page = ? and cf.status = ? and bl.status = ? and c.status = ?',
                array($masterPage, Status::ACTIVE, Status::ACTIVE, Status::ACTIVE))
            ->select('bl.block', 'bl.name as block_name', 'c.id', 'c.file', 'c.name as file_name', 'c.default')
            ->get();
        $arrGroupConstructor = array();
        $arrDetailConstructor = array();
        foreach ($arrConstructor as $value) {
            $arrGroupConstructor[$value->block] = $value->block_name;
            $arrDetailConstructor[$value->block][] = array($value->file_name, $value->id, $value->default);
        }

        return View::make('backend.frontend_theme.index')
            ->with('templates', $templates)
            ->with('arrMasterPage', $arrMasterPage)
            ->with('arrGroup', $arrGroup)
            ->with('arrDetail', $arrDetail)
            ->with('arrGroupConstructor', $arrGroupConstructor)
            ->with('arrDetailConstructor', $arrDetailConstructor)
            ->with('lang',json_encode(Lang::get('messages')));
    }

    /**
     * Config frontend theme
     * POST: backend/frontend_theme
     *
     * @return mixed
     */
    public function configFrontendTheme()
    {
        $templates = SiteTemplate::where('status', Status::ACTIVE)->get();

        //dd(Input::all());
        $msg = array('success'=> Lang::get('messages.save_complete'));
        DB::beginTransaction();
        try{
            $inputAll = Input::all();
            $templateId = Input::get('template');
            $masterPage = Input::get('master_page');
            // Update template default
            $templateOld = SiteTemplate::whereRaw('status = ? and `default` = ?', array(Status::ACTIVE, IsDefault::YES))->first();
            $templateOld->default = IsDefault::NO;
            $templateOld->save();
            $templateNew = SiteTemplate::find($templateId);
            $templateNew->default = IsDefault::YES;
            $templateNew->save();

            // Update master page default
            $masterPageOld = SiteTemplateMasterPage::whereRaw('template_id = ? and status = ? and `default` = ?',
                array($templateOld->id, Status::ACTIVE, IsDefault::YES))->first();
            $masterPageOld->default = IsDefault::NO;
            $masterPageOld->save();
            $masterPageNew = SiteTemplateMasterPage::whereRaw('template_id = ? and master_page = ?',
                array($templateNew->id, $masterPage))->first();
            $masterPageNew->default = IsDefault::YES;
            $masterPageNew->save();

            // Update class css
            $groupClass = SiteTemplateClassGroup::whereRaw('template_id = ? and status = ? and `default` = ?',
                array($templateOld->id, Status::ACTIVE, IsDefault::YES))->get();
            foreach($groupClass as $group){
                $classOld = SiteTemplateClassCss::whereRaw('group_id = ? and `default` = ?', array($group->id, IsDefault::YES))->first();
                $classOld->default = IsDefault::NO;
                $classOld->save();
                foreach($inputAll as $key => $value){
                    if('class#'.$group->group_key == $key){
                        $classNew = SiteTemplateClassCss::whereRaw('group_id = ? and class_value = ?', array($group->id, $value))->first();
                        $classNew->default = IsDefault::YES;
                        $classNew->save();
                        break;
                    }
                }
            }

            //Update constructor
            $count = 0;
            foreach($inputAll as $key => $value){
                if(strlen($key) > strlen('constructor#')  && substr($key,0,strlen('constructor#')) == 'constructor#'){
                    $constructor = SiteTemplateConstructor::find($value);
                    $constructor->default = IsDefault::YES;
                    $constructor->save();
                    $blocks = SiteTemplateConstructor::where('constructor_block_id','=',$constructor->constructor_block_id)->get();
                    foreach($blocks as $b){
                        if($b->id == $constructor->id){
                            continue;
                        }
                        $b->default = IsDefault::NO;
                        $b->save();
                    }
                }
            }
            //dd($count);
        } catch (Exception $e){
            DB::rollback();
            $msg = array('error'=> 'Config fail.'.$e->getMessage());
        }
        DB::commit();

        // Get default master page
        $templateId = SiteTemplate::whereRaw('status = ? and `default` = ?', array(Status::ACTIVE, IsDefault::YES))->first()->id;
        $arrMasterPage = SiteTemplateMasterPage::whereRaw('template_id = ? and status = ?', array($templateId, Status::ACTIVE))->get();
        $arrClass = DB::table('site_template_class_group as g')
            ->join('site_template_class_css as c', 'g.id', '=', 'c.group_id')
            ->whereRaw('g.template_id = ? and g.status = ? and c.status = ?', array($templateId, Status::ACTIVE, Status::ACTIVE))
            ->select('g.id as group_id', 'c.id as class_id', 'g.template_id', 'g.group_key', 'g.group_name', 'g.default as default_group', 'c.class_value', 'c.name', 'c.default as default_class')
            ->get();
        $arrGroup = array();
        $arrDetail = array();
        foreach ($arrClass as $value) {
            $arrGroup[$value->group_key] = $value->group_name;
            $arrDetail[$value->group_key][] = array($value->name, $value->class_value, $value->default_class);
        }

        // Get default constructor
        $masterPage = SiteTemplateMasterPage::whereRaw('template_id = ? and status = ? and `default` = ?',
            array($templateId, Status::ACTIVE, IsDefault::YES))->first()->id;
        $arrConstructor = DB::table('site_template_config as cf')
            ->join('site_template_constructor_block as bl', 'cf.constructor_block', '=', 'bl.id')
            ->join('site_template_constructor as c', 'bl.id', '=', 'c.constructor_block_id')
            ->whereRaw('cf.master_page = ? and cf.status = ? and bl.status = ? and c.status = ?',
                array($masterPage, Status::ACTIVE, Status::ACTIVE, Status::ACTIVE))
            ->select('bl.block', 'bl.name as block_name', 'c.id', 'c.file', 'c.name as file_name', 'c.default')
            ->get();
        $arrGroupConstructor = array();
        $arrDetailConstructor = array();
        foreach ($arrConstructor as $value) {
            $arrGroupConstructor[$value->block] = $value->block_name;
            $arrDetailConstructor[$value->block][] = array($value->file_name, $value->id, $value->default);
        }

        return View::make('backend.frontend_theme.index')
            ->with('templates', $templates)
            ->with('arrMasterPage', $arrMasterPage)
            ->with('arrGroup', $arrGroup)
            ->with('arrDetail', $arrDetail)
            ->with('arrGroupConstructor', $arrGroupConstructor)
            ->with('arrDetailConstructor', $arrDetailConstructor)
            ->with('messages', $msg)
            ->with('lang',json_encode(Lang::get('messages')));
    }

    /**
     * Controller ajaxChooseFrontendTheme
     * Choose frontend master page config
     * @return mixed
     */
    public function ajaxChooseFrontendTheme()
    {
        $templateId = Input::get('template');
        if (is_null($templateId)) {
            return Response::json(array('error' => Lang::get('messages.template_id_is_null')), 403);
        }
        $arrMasterPage = SiteTemplateMasterPage::whereRaw('site_template_id = ? and status = ?', array($templateId, 1))->get();
        $arrClass = SiteTemplateClass::whereRaw('site_template_id = ? and status = ?', array($templateId, 1))->get();
        $arrGroup = array();
        $arrDetail = array();
        foreach ($arrClass as $value) {
            if (is_null($value->class)) {
                $arrGroup[$value->class_key] = $value->name;
            } else {
                $arrDetail[$value->class_key][] = array($value->name, $value->class, $value->default);
            }
        }
        $html = View::make('backend.frontend_theme.ajax_choose_theme')
            ->with('arrMasterPage', $arrMasterPage)
            ->with('arrGroup', $arrGroup)
            ->with('arrDetail', $arrDetail)
            ->render();
        return Response::json($html);
    }

    /**
     * Controller ajaxChooseMasterPage
     * Choose mater page config
     * @return mixed
     */
    public function ajaxChooseMasterPage()
    {
        $templateId = Input::get('template');
        $masterPage = Input::get('master_page');
        if (is_null($templateId) || is_null($masterPage)) {
            return Response::json(array('error' => Lang::get('messages.template_id_or_master_page_is_null')), 403);
        }
        $arrConstructor = SiteTemplateConstructor::whereRaw('site_template_id = ? and master_page = ? and status = ?',
            array($templateId, $masterPage, 1))->get();
        $arrGroup = array();
        $arrDetail = array();
        foreach ($arrConstructor as $value) {
            $arrGroup[$value->include_folder] = $value->name;
            $arrDetail[$value->include_folder][] = array($value->name, $value->include_file, $value->default);
        }
        $html = View::make('backend.frontend_theme.ajax_choose_master_page')
            ->with('arrGroup', $arrGroup)
            ->with('arrDetail', $arrDetail)
            ->render();
        return Response::json($html);
    }

    public function showConfig()
    {
        return View::make('backend.frontend_theme.config');
    }

}