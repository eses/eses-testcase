<?php

class BackendUsersController extends BaseBackendController {
	
	/**
	 * Show index users page
	 * GET: /backend/users
	 * @return \Illuminate\View\View
	 */
	public function showIndex(){
		return View::make('backend.users.index')->with('lang',json_encode(Lang::get('messages')));
	}
}