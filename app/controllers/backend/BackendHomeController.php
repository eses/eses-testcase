<?php 

class BackendHomeController extends BaseBackendController {

	// Show home page
	public function showHome()
	{
		return View::make('backend.home.index')->with('lang',json_encode(Lang::get('messages')));
	}
}