<?php

class AdminProfileController extends BaseBackendController {

    /**
     * Show page admin login
     * POST: backend/login
     * @return \Illuminate\View\View
     */
    public function showIndex()
    {
        $username = Session::get(UserModel::USER_ID);
        Log::info('session avatar='.Session::get('avatar'));

        return View::make('backend.admin_profile.index')->with('username', $username)   ->with('lang',json_encode(Lang::get('messages')));
    }
    public function getUserProfile(){
        Log::info('get user profile, username='.Session::get(UserModel::USER_ID));
        $data = array('data' =>User::getUserProfile(Session::get(UserModel::USER_ID)),
            UserModel::IS_EDIT => Session::get(UserModel::IS_EDIT));

        return Response::json($data);
    }
    public function updateUserProfile()
    {
        $username = Session::get(UserModel::USER_ID);
        $firstname = Input::get(UserModel::USER_FIRST_NAME);
        $lastname = Input::get(UserModel::USER_LAST_NAME);
        $address = Input::get('address');
        $phone = Input::get("phone");
        $email = Input::get('email');
        $gender = Input::get("gender");
        $user = array(
            'username'=>$username,
            'first_name'=>$firstname,
            'last_name'=>$lastname,
            'gender'=>$gender,
            'phone'=>$phone,
            'address'=>$address,
            'email'=>$email
        );
        $result = User::updateUserProfile($user);
        $error = 0;
        $message = Lang::get('messages.update_user_profile_successfully');
        Log::info('rs='.$result);
        if($result != true){
            Log::info('failed,rs='.$result);
            $error = 1;
            $message = $result;
        }
        return Response::json(array('error' => $error,'data' => $message), 200);
    }
    public  function uploadBase64Image($username , $src)
    {
        try{
            $pathUpload = 'upload/'.md5($username);
            if(!file_exists($pathUpload)){
                mkdir($pathUpload);
            }
            Log::info($pathUpload);
            $type='';
            list($type, $src) = explode(';', $src);
            Log::info('type='.$type);

            list(, $data) = explode(',', $src);
            $data = base64_decode($data);

            $image= $pathUpload.'/avatar.png';
            $file = fopen($image,'w');
            fwrite($file, $data);
            fclose($file);

            DB::beginTransaction();

            UserModel::where(UserModel::USER_ID, '=',Str::lower($username))->update(['avatar'=>$image]);

            DB::commit();
            Session::put('avatar', $image);
            return true;
        }catch(\OAuth\Common\Exception\Exception $ex){
            DB::rollBack();
            Log::error('Upload avatar, error='.$ex->getMessage());
            return false;
        }


    }

    public function updateAvatar()
    {
        Log::info('update avatar');
        Log::info(Input::all('avatar'));
        $this->uploadBase64Image(Session::get(UserModel::USER_ID),Input::get('avatar'));

        $result = true;
        return Response::json(array('data' => $result), 200);
    }

    public function updatePassword()
    {
        Log::info('update password');
        Log::info(Input::all());
        $user = new UserModel();
        $user->username = Session::get(UserModel::USER_ID);
        $user->password = Input::get('password');
        $user->newPassword = Input::get('newPassword');
        $user->retypePassword = Input::get('retypePassword');
        if(strcmp($user->newPassword, $user->retypePassword) == 0){

            $result = User::changePassword($user);
            if($result){
                return Response::json(array('message' => Lang::get('messages.your_password_is_updated_successfully'),
                    'result' => true), 200);
            }else{
                return Response::json(array('message' => Lang::get('messages.account_information_is_incorrect'),
                    'result' => false), 200);
            }
        }else{
            return Response::json(array('message' => Lang::get('messages.confirm_password_does_not_match'),
                'result' => false), 200);
        }
    }
}