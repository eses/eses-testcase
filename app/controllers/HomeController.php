<?php

class HomeController extends BaseController{

    /**
     * Get home page with top news
     * GET: /
     * @return $this
     */
    public function showHome(){
        $template = SiteTemplateConstructor::whereRaw('status = 1 and `default` = 1 and constructor_block_id = 6')->first();
        return View::make('frontend.home.'.$template->file);
    }

    /**
     * Get news in categories
     * @param $alias
     * @return $this
     */
    public function getNewsInCategories($alias){

        $data = News::getNewsByCategoriesAliasWithPaging($alias,20);
        $categories = Categories::getCategoriesByAlias($alias);
        if(count($data)==1){
          return Redirect::action('HomeController@getNewsDetail', $data[0]->link);
        }
        return View::make('frontend.home.news')
            ->with('newses',$data)
            ->with('categories',$categories);
    }

    /**
     * Get news detail by link
     * @param $link
     * @return $this
     */
    public function getNewsDetail($link){
        $link = urlencode($link);
        $data = News::getSingleNewsByLink($link);
        $id = "-1";
        $status = "0";
        if($data != null){
            $id = $data->id;
            $status = $data->status;
        }
        $categories = News::getCategoriesByLink($link);
        $newses = array();
        if(isset($categories)){
            $newses = News::getNewsByCategoriesAlias($categories->categories_alias,6);
            $id = $newses[0]->id;
            $status = $newses[0]->status;
        } else {
           // dd(" not in categories");
            $newses = News::getNewsNotInCategories(6);
            $id = $newses[0]->id;
            $status = $newses[0]->status;
        }

        return View::make('frontend.home.detail')
            ->with('news',$data)
            ->with('categories',$categories)
            ->with('newses',$newses)
            ->with('idNews',$id)
            ->with('status', $status);
    }
}