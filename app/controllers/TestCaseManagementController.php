<?php

class TestCaseManagementController extends BaseBackendController {

    /**
     * Show page admin login
     * POST: backend/login
     * @return \Illuminate\View\View
     */
    public function showIndex()
    {
        $username = Session::get(UserModel::USER_ID);
        return View::make('backend.testcase.index')
            ->with('username', $username)
            ->with('lang',json_encode(Lang::get('messages')));
    }
    public function getTestLinks(){
        $length = Input::get('length');
        $start = Input::get('start');
        $keyword = Input::get('search')['value'];
        Log::info('keyword='.$keyword);
        $data = TestCase::getTestLinkBy($keyword,$start, $length);
        $dataPaginate = array(
            'draw' => Input::get('draw'),
            'recordsTotal' => $data['total'],
            'recordsFiltered'=> $data['total'],
            'data' => $data['data']
        );
        return Response::json($dataPaginate);
    }

    public function getTestCases(){
        $testlink_id = Input::get('id');
        $length = Input::get('length');
        $start = Input::get('start');
        $data = TestCase::getTestCaseBy($testlink_id, $start, $length);
        $dataPaginate = array(
            'draw' => Input::get('draw'),
            'recordsTotal' => $data['total'],
            'recordsFiltered'=> $data['total'],
            'data' => $data['data'],
            'idTestLink' => $testlink_id
        );
        return Response::json($dataPaginate);
    }



    //get TestLink by ID
    public function getTestLink(){
        Log::info("Get TestLink,id=".Input::get("id"));
        $rs = TestCase::getTestLink(Input::get("id"));
        return Response::json($rs);
    }

    //add TestLink
    public function addTestLink(){
        Log::info("Add TestLink".Input::get("name"));
        $data = array("error" => 0, "message" => "Add TestLink Successful!");
        try{
            $rs = TestCase::insertTestLink(Input::all());
            if($rs != true){
                $data = array("error" => 1, "message" => "System has error!Please contact Administrator!");
            }
        }catch(Exception $ex){
            $data = array("error" => 1, "message" => $ex->getMessage());
        }
        return Response::json($data);
    }

    //update TestLink
    public function updateTestLink(){
        Log::info("Update TestLink, id=".Input::get("id"));
        $data = array("error" => 0, "message" => "Update TestLink Successful!");
        try{
            $rs = TestCase::updateTestLink(Input::all());
        }catch(Exception $ex){
            $data = array("error" => 1, "message" => $ex->getMessage());
        }
        return Response::json($data);
    }

    //get TestCase by ID
    public function getTestCase(){
        $rs = TestCase::getTestCase(Input::get("id"));
        return Response::json($rs);
    }

    //get TestCase by TestLink
    public function getTestCaseByTestLink(){
        Log::info("Get TestCase,id=".Input::get("id"));
        $rs = TestCase::getTestCaseByTestLink(Input::get("id"));
        return Response::json($rs);
    }

    //add Testcase
    public function addTestCase(){
        Log::info("Add TestCase".Input::get("name"));
        $data = array("error" => 0, "message" => "Add TestCase Successful!");
        try{
            $rs = TestCase::insertTestCase(Input::all());
            if($rs != true){
                $data = array("error" => 1, "message" => "System has error!Please contact Administrator!");
            }
        }catch(Exception $ex){
            $data = array("error" => 1, "message" => $ex->getMessage());
        }
        return Response::json($data);
    }

    //update testcase
    public function updateTestCase(){
        Log::info("Update TestCase,");
        Log::info("Update TestCase, id=".Input::get("id"));
        $data = array("error" => 0, "message" => "Update TestCase Successful!");
        try{
            $rs = TestCase::updateTestCase(Input::all());
        }catch(Exception $ex){
            $data = array("error" => 1, "message" => $ex->getMessage());
        }
        return Response::json($data);
    }

}