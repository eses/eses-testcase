<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',
    app_path().'/commons',
    app_path().'/events',
    app_path().'/listeners'

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

App::missing(function($exception)
{
	$path = Request::path();
	// Init extension image capture
	$arrExt = array('.jpg','.jpeg','.png','.gif','.bmp','.tiff','.svg');

	// Resolve path image
	$arrPath = explode('/upload/media/', $path);
	if(isset($arrPath[1])){
		
		$filename = $arrPath[1];

		// Append the filename to the path where our images are located
        $pathImg = public_path() . '/upload/media/' . $filename;

        try {
        	// Initialize an instance of Symfony's File class.
	        // This is a dependency of Laravel so it is readily available.
	        $file = new Symfony\Component\HttpFoundation\File\File($pathImg);

	        // Make a new response out of the contents of the file
	        // Set the response status code to 200 OK
	        $response = Response::make(
	            File::get($pathImg), 
	            200
	        );

	        // Modify our output's header.
	        // Set the content type to the mime of the file.
	        // In the case of a .jpeg this would be image/jpeg
	        $response->header(
	            'Content-type',
	            $file->getMimeType()
	        );
        } catch (Exception $ex){

        	// Resolve image not found resource
        	foreach ($arrExt as $key => $value) {
				$value = strtolower($value);
				if (strpos($path, $value) !== false) {
				    return Redirect::to('/upload/image_null.jpg');
				}
			}
			
        }
        
        // We return our image here.
        return $response;
	}

	// Resolve image not found resource
	foreach ($arrExt as $key => $value) {
		$value = strtolower($value);
		if (strpos($path, $value) !== false) {
		    return Redirect::to('/upload/image_null.jpg');
		}
	}
	
    return Response::view('errors.missing', array(), 404);
});

require app_path().'/filters.php';
